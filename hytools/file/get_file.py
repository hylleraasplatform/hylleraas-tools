from functools import singledispatch
from pathlib import Path
from typing import Any

from .file import File


@singledispatch
def get_file(arg: Any, **kwargs) -> File:
    """Convert the argument to a file."""
    try:
        file = get_file(arg.asdict(), **kwargs)
    except AttributeError:
        try:
            file = get_file(arg.__dict__, **kwargs)
        except AttributeError:
            raise TypeError(f'Cannot convert {type(arg)} to File.')
    return file


@get_file.register(str)
@get_file.register(Path)
def _(arg: str, **kwargs) -> File:
    """Return a file with the given name."""
    try:
        p = Path(arg)
    except (OSError, TypeError):
        raise TypeError(f'Cannot convert {arg} to File.')

    content = (p.read_text() if kwargs.get('include_content', False)
               and p.is_file() and p.exists() else None)
    path = str(p) if p.is_absolute() else None
    folder = str(p.parent) if p.is_absolute() else None
    return File(name=p.name, content=content, path=path, folder=folder)


@get_file.register(dict)
def _(arg: dict, **kwargs) -> File:
    """Return a file with the given dictionary."""
    keys = [field.name for field in File.__dataclass_fields__.values()]
    return File(**{key: arg.get(key) for key in keys})
