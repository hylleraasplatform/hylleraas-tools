from functools import wraps
from pathlib import Path
from string import Template
from typing import Any, Optional

from .get_file import get_file


def list_exec(func):
    """Decorate to execute a function on a list of arguments."""
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        """Execute function on list."""
        if isinstance(args[0], list):
            return [func(self, arg, *args[1:], **kwargs) for arg in args[0]]
        else:
            return func(self, *args, **kwargs)
    return wrapper


class FileManager:
    """Class to manage files."""

    @classmethod
    @list_exec
    def read_file_local(cls, file: Any, **kwargs) -> str:
        """Read file locally."""
        _file = get_file(file)
        p = Path(getattr(_file, 'path', None))
        if not p:
            raise ValueError('The path of the file must be provided.')
        if not p.exists():
            raise FileNotFoundError(f'The file {p} does not exist.')
        return p.read_text()

    @classmethod
    @list_exec
    def write_file_local(cls,
                         file: Any,
                         overwrite: Optional[bool] = True,
                         variables: Optional[dict] = {},
                         **kwargs) -> str:
        """Write file locally."""
        _file = get_file(file)
        p = Path(getattr(_file, 'path', None))
        if not p:
            raise ValueError('The path of the file must be provided.')
        if p.exists() and not overwrite:
            return str(p)
        p.parent.mkdir(parents=True, exist_ok=True)
        content = (Template(getattr(_file, 'content', ''))
                   .safe_substitute(**variables)
                   if variables else getattr(_file, 'content', ''))
        if content:
            p.write_text(content)
        return str(p)
