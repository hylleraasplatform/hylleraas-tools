from dataclasses import dataclass
from hashlib import sha256
from pathlib import Path
from typing import Optional


@dataclass
class File:
    """Simple class to represent a file."""

    name: Optional[str] = None
    content: Optional[str] = None
    path: Optional[str] = None
    host: Optional[str] = None
    folder: Optional[str] = None

    def __post_init__(self):
        """Initialize the file object with the given parameters."""
        if not self.name and self.path:
            self.path = Path(self.path)
            if '~' in str(self.path):
                self.path = self.path.expanduser()
            # self.path = self.path.resolve()

            self.name = Path(self.path).name
            self.folder = str(Path(self.path).parent)
        self.folder = self.folder
        self.host = self.host
        if self.folder:
            self.path = self.path or str(Path(self.folder) / self.name)

    def hash(self) -> str:
        """Return the hash of the file."""
        return sha256(str(self).encode()).hexdigest()
