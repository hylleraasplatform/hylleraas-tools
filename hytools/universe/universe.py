from typing import Optional

from hytools.units import Units


class Universe:
    """Universe class."""

    def __init__(self, units: Optional[Units] = None):
        """Initialize the universe."""
        self._units = units or Units()

    @property
    def units(self) -> Units:
        """Return the units."""
        return self._units

    @units.setter
    def units(self, units: Units):
        """Set the units."""
        self._units = units
