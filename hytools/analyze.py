# type: ignore
from typing import List, Optional, Tuple, Union

import numpy as np


def gen_spectrum(*args, **kwargs):
    """Generate convoluted spectrum (x,y).

    Wrapper for Analyze.gen_spectrum()

    Parameters
    ----------
    xval : float
        abscissa
    yval : float
        ordinates
    fwhm : float
        full with at half maximum
    points : int, optional
        number of points (x,y) generated from each pair (xval,yval), defaults to 1000
    method : str, optional
        convolution method ("gaussian" or "lorentzian"), defaults to "gaussian"
    span : float, optional
        parameter for setting peak width as multiples of sigma

    Returns
    -------
    Tuple[list, list]
        tuple (x,y) for plotting

    """
    return Analyze.gen_spectrum(*args, **kwargs)


class Analyze:
    """Hylleraas analyze class."""

    @staticmethod
    def compute_gaussian(x: float, x0: float, sigma: float) -> float:
        """Compute value of Normal distribution.

        Parameters
        ----------
        x : float
            abscissa
        x0 : float
            mean
        sigma : float
            standard deviation

        Returns
        -------
        float
            value of the distribution at x

        """
        val = np.exp(-((x - x0)**2) / (2.0 * sigma**2)) / \
            (sigma * np.sqrt(2 * np.pi))
        return val

    @staticmethod
    def compute_lorentzian(x: float, x0: float, gamma: float) -> float:
        """Compute value of Cauchy-Lorentz distribution.

        Parameters
        ----------
        x : float
            abscissa
        x0 : float
            mean
        gamma : float
            half-width at half-maximum

        Returns
        -------
        float
            value of the distribution at x

        """
        # val = 1.0/(np.pi*gamma*(1.0+((x-x0)/gamma)**2))
        val = gamma**2 / ((x - x0)**2 + gamma**2)
        val *= 1.0 / (np.pi * gamma)
        return val

    @staticmethod
    def gen_spectrum(
        xval: Union[float, List[float], np.ndarray],
        yval: Union[float, List[float], np.ndarray],
        fwhm: float,
        points: Optional[int] = 100,
        method: Optional[str] = 'gaussian',
        span: Optional[float] = 10.0,
    ) -> Tuple[Union[List[float], np.ndarray], Union[List[float], np.ndarray]]:
        """Generate convoluted spectrum (x,y).

        Parameters
        ----------
        xval : float
            abscissa
        yval : float
            ordinates
        fwhm : float
            full with at half maximum
        points : int, optional
            number of points (x,y) generated from each pair (xval,yval), defaults to 1000
        method : str, optional
            convolution method ("gaussian" or "lorentzian"), defaults to "gaussian"
        span : float, optional
            parameter for setting peak width as multiples of sigma

        Returns
        -------
        Tuple[list, list]
            tuple (x,y) for plotting

        """
        if float(fwhm) < 0:
            raise Exception('FWHM < 0 does not make much sense')

        if int(points) < 2:
            raise Exception('number of interpolation points too small')

        if method == 'gaussian':
            sigma = fwhm / (2.0 * np.sqrt(2.0 * np.log(2.0)))
        elif method == 'lorentzian':
            sigma = fwhm / 2.0
        else:
            raise NotImplementedError(
                f'method {method} not implemented, use gaussian or lorentzian')

        xstep = (span * sigma) / float(points - 1)

        xlist = []
        xlist.extend(xval)
        for x in xval:
            xmin = x - span / 2.0 * sigma
            for i in range(0, points):
                xadd = xmin + float(i) * xstep
                xlist.append(xadd)
        xlist.sort()

        ylist = []
        for x in xlist:
            y = 0.0
            for x0, y0 in zip(xval, yval):
                if method == 'gaussian':
                    y += y0 * __class__.compute_gaussian(x, x0, sigma)
                elif method == 'lorentzian':
                    y += y0 * __class__.compute_lorentzian(x, x0, sigma)
            ylist.append(y)

        return xlist, ylist
