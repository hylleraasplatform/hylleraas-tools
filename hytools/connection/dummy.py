from .abc import Connection


class ConnectionDummy(Connection):
    """Dummy connection for testing."""

    def __init__(self, *args, **kwargs):
        """Initialize the connection."""
        super().__init__(*args, **kwargs)
        self._is_open = False

    def teardown(self) -> dict:
        """Teardown the connection."""
        self.close()
        return {'connection_type': 'dummy'}

    def __hash__(self) -> int:
        """Return the hash of the connection."""
        return hash(self._is_open)

    def __eq__(self, other):
        """Return True if the connections are equal."""
        return self._is_open == other._is_open

    def open(self):
        """Open the connection."""
        self._is_open = True

    def close(self):
        """Close the connection."""
        self._is_open = False

    def execute(self, *args, **kwargs):
        """Execute a statement."""
        return None

    def rollback(self):
        """Rollback the connection."""
        pass

    def __enter__(self):
        """Enter the context."""
        self.open()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        """Exit the context."""
        self.close()

    def send(self, *args, **kwargs):
        """Send object."""
        pass

    def receive(self, *args, **kwargs):
        """Receive object."""
        pass

    @property
    def is_open(self) -> bool:
        """Return True if the connection is open."""
        return self._is_open
