
import sys
from copy import deepcopy
from itertools import groupby
from pathlib import Path
from typing import List, Optional

from hytools.file import File, get_file
from hytools.logger import get_logger

# uses -R option of rsync (>= 2.6.7) to create relative paths in dest


def send_multiple(src: Optional[list] = None,
                  dest: Optional[list] = None,
                  rsync_opt: Optional[str] = None,
                  port: Optional[int] = None,
                  user: Optional[str] = None,
                  host: Optional[str] = None,
                  **kwargs) -> str:
    """Send multiple sources to multiple destinations using rsync>=2.6.7."""
    return RsyncMultiple().send_multiple(src=src,
                                         dest=dest,
                                         rsync_opt=rsync_opt,
                                         port=port,
                                         user=user,
                                         host=host,
                                         **kwargs)


def receive_multiple(src: Optional[list] = None,
                     dest: Optional[list] = None,
                     rsync_opt: Optional[str] = None,
                     port: Optional[int] = None,
                     user: Optional[str] = None,
                     host: Optional[str] = None,
                     **kwargs) -> str:
    """Receive multiple sources to multiple destinations using rsync>=2.6.7."""
    return RsyncMultiple().receive_multiple(src=src,
                                            dest=dest,
                                            rsync_opt=rsync_opt,
                                            port=port,
                                            user=user,
                                            host=host,
                                            **kwargs)


class RsyncMultiple:
    """Class to send and receive using rsync>=2.6.7."""

    def send_multiple(self,
                      src: Optional[list] = None,
                      dest: Optional[list] = None,
                      rsync_opt: Optional[str] = None,
                      port: Optional[int] = None,
                      user: Optional[str] = None,
                      host: Optional[str] = None,
                      **kwargs) -> str:
        """Send multiple sources to multiple destinations using rsync>=2.6.7."""
        rsync_opt = rsync_opt or '-avuzRP'
        if 'R' not in rsync_opt and not kwargs.get('failback', False):
            raise ValueError('rsync_opt must contain the -R option' +
                             ' or the failback option must be set to True' +
                             ' to create relative paths in dest, got: ' +
                             f'{rsync_opt}')
        port = port or 22

        src = [] if src is None else src if isinstance(src, list) else [src]
        dest = [] if dest is None else dest if isinstance(dest, list) else [dest]
        if not any(src + dest):
            raise ValueError('src and dest are required')
        src = [get_file(s) for s in src]
        dest = [Path(d.path if isinstance(d, File) else d) for d in dest]

        main = self.get_main_cmd(rsync_opt, port, **kwargs)

        if kwargs.get('failback', False):
            if 'R' in rsync_opt:
                logger = get_logger(print_level='WARNING')
                logger.warning('-R option is not needed for failback')
            cmds = self.get_failback_send_cmd(src, dest, main, user, host)
            return '\n'.join(cmds)

        src = [get_file(s) for s in src]
        dest = [Path(d.path if isinstance(d, File) else d) for d in dest]
        cmds = [f'{main} {f}' for f in self.get_send_str(src, dest, user, host)]
        return '\n'.join(cmds)

    def receive_multiple(self, src: Optional[list] = None,
                         dest: Optional[list] = None,
                         rsync_opt: Optional[str] = None,
                         port: Optional[int] = None,
                         user: Optional[str] = None,
                         host: Optional[str] = None,
                         **kwargs) -> str:
        """Receive multiple sources to multiple destinations using rsync>=2.6.7."""
        rsync_opt = rsync_opt or '-avuzRP'
        if 'R' not in rsync_opt and not kwargs.get('failback', False):
            raise ValueError('rsync_opt must contain the -R option' +
                             ' or the failback option must be set to True' +
                             ' to create relative paths in dest, got: ' +
                             f'{rsync_opt}')
        port = port or 22

        src = [] if src is None else src if isinstance(src, list) else [src]
        dest = [] if dest is None else dest if isinstance(dest, list) else [dest]
        if not any(src + dest):
            raise ValueError('src and dest are required')
        src = [get_file(s) for s in src]
        dest = [Path(d.path if isinstance(d, File) else d) for d in dest]

        main = self.get_main_cmd(rsync_opt, port, **kwargs)

        if kwargs.get('failback', False):
            if 'R' in rsync_opt:
                logger = get_logger(print_level='WARNING')
                logger.warning('-R option is not needed for failback')
            cmds = self.get_failback_receive_cmd(src, dest, main, user, host)
            return '\n'.join(cmds)

        src = [get_file(s) for s in src]
        dest = [Path(d.path if isinstance(d, File) else d) for d in dest]
        cmds = [f'{main} {f}' for f in self.get_receive_str(src, dest, user, host)]
        return '\n'.join(cmds)

    def get_failback_send_cmd(self, src: list,
                              dest: list,
                              main: str,
                              user: Optional[str] = None,
                              host: Optional[str] = None) -> List[str]:
        """Get the failback command."""
        src_group, dest_group = self.group_sources([s.path for s in src], dest)
        cmds = []
        for _src, _dest in zip(src_group, dest_group):
            for s in _src:
                dest_str = self.get_dest_str(str(_dest[0]), user, host)
                cmd = f'{main} {s} {dest_str}'
                cmds.append(cmd)
        return cmds

    def get_failback_receive_cmd(self, src: list,
                                 dest: list,
                                 main: str,
                                 user: Optional[str] = None,
                                 host: Optional[str] = None) -> List[str]:
        """Get the failback command."""
        src_group, dest_group = self.group_sources([s.path for s in src], dest)
        cmds = []
        for _src, _dest in zip(src_group, dest_group):
            for s in _src:
                src_str = self.get_dest_str(str(s), user, host)
                cmd = f'{main} {src_str} {_dest[0]}'
                cmds.append(cmd)
        return cmds

    def get_main_cmd(self, rsync_opt: str,
                     port: int,
                     **kwargs) -> str:
        """Get the main rsync command."""
        socket_path = kwargs.get('socket_path', None)
        cmd = f"rsync {rsync_opt} -e 'ssh -p {port}" + \
            (f" -o ControlPath={socket_path}'" if socket_path else "'")
        return cmd

    def insert_common(self, src: list,
                      dest: list) -> tuple:
        """Insert common parts with destination into the sources."""
        common = []
        _src = [Path(s.path) for s in src]
        src_names = [s.name for s in _src]
        src_parents = [s.parent for s in _src]
        for i, (s, d) in enumerate(zip(src_parents, dest)):
            _common = []
            for p in reversed(s.parts):
                d_p = d.parts
                if p == d_p[-1]:
                    d = d.parent
                    s = s.parent
                    _common.append(p)
                else:
                    break
            dest[i] = d
            common.append(_common)
            src_parents[i] = s
        # combine common parts
        common_paths = [Path('/'.join(c[::-1])) for c in common]
        # insert common parts into src
        src = [f'{Path(s)}/./{Path(c)}/{n}'
               if f'{Path(c)}' != '.'
               else f'{Path(s)}/./{n}'
               for s, c, n in zip(src_parents, common_paths, src_names)]
        for i, (s, d) in enumerate(zip(src, dest)):
            if Path(s).name == Path(d).name:
                dest[i] = Path(d).parent
        return src, dest

    def group_sources(self, src: list,
                      dest: list) -> tuple:
        """Group the sources and destinations."""
        # sort src according to dest
        src_sorted = [s for _, s in sorted(zip(dest, src))]
        dest_sorted = sorted(dest)
        dest_group = []
        src_group = []
        for _, g in groupby(dest_sorted, key=(lambda x: x)):
            d = list(deepcopy(g))[0]
            dest_group.append(list(g))
            src_group.append([src_sorted[j]
                              for j, x in enumerate(dest_sorted) if x == d])
        return src_group, dest_group

    def get_dest_str(self, dest: str,
                     user: str,
                     host: str) -> str:
        """Get the destination string."""
        dest_str = f"{user + '@' if user else ''}" + \
            f"{host + ':' if host else ''}{dest}"
        if 'darwin' in sys.platform:
            dest_str = f"'{dest_str}'"
        return dest_str

    def get_send_str(self, src: list,
                     dest: list,
                     user: Optional[str] = None,
                     host: Optional[str] = None
                     ) -> List[str]:
        """Get the send string."""
        src_group, dest_group = self.group_sources(*self.insert_common(src, dest))
        cmds = []
        for _src, _dest in zip(src_group, dest_group):
            files_str = ' '.join([f'{s}' for s in _src])
            dest_str = self.get_dest_str(str(_dest[0]), user, host)
            cmds.append(f'{files_str} {dest_str}')
        return cmds

    def get_receive_str(self, src: list,
                        dest: list,
                        user: Optional[str] = None,
                        host: Optional[str] = None
                        ) -> List[str]:
        """Get the send string."""
        src_group, dest_group = self.group_sources(*self.insert_common(src, dest))
        cmds = []
        for _src, _dest in zip(src_group, dest_group):
            files_str = ' '.join([f'{s}' for s in _src])
            src_str = self.get_dest_str('', user, host)
            if src_str[-1] == "'":
                cmds.append(f"{src_str[:-1]}{files_str}' {_dest[0]}")
            else:
                cmds.append(f'{src_str}{files_str} {str(_dest[0])}')
        return cmds


# if __name__ == '__main__':

#     import os

#     # src = [File(path='/home/test0.txt'),
#     #        File(path='/home/work1/scratch/test1.txt'),
#     #        File(path='/home/work2/scratch/test2.txt'),
#     #        File(path='/home/test/test3.txt')]
#     # dest = ['/cluster/', '/cluster/work1/scratch/', '/cluster/work2/scratch/', '/work3/scratch/']
#     # # dest = ['/dest']*7
#     # cmd = send_multiple(src=src, dest=dest, user='user', host='host', port=22)
#     # expected = "rsync -avuzqRP -e 'ssh -p 22' /home/./work1/scratch/test1.txt /home/./work2/scratch/test2.txt /home/test0.txt 'user@host:/cluster'"
#     # print('IST:', cmd)
#     # cmd_f = send_multiple(src=src, dest=dest, user='user', host='host', port=22, failback=True)
#     # print('FAILBACK', cmd_f)
#     # print('SOLL:', expected)
#     # assert cmd == expected
#     # src = ['/cluster/work/job1/*', '/cluster/work/job2/*', '/cluster/work/job1/file0.txt']
#     # dest = ['/home/job1', '/home/job2', '/home/job1']
#     # cmd = receive_multiple(src=src,
#     #                        dest=dest,
#     #                        user='user', host='host', port=22, failback=False)
#     # print(cmd)

    # dest_root = '/cluster/work/users/tilmann'
    # src_root = '/Users/tilmann/Downloads/test'
    # src = [f'{src_root}/test0.txt',
    #        f'{src_root}/job1/test1.txt',
    #        f'{src_root}/job2/test2.txt',]
    # dest = [f'{dest_root}']*3
    # dest = [f'{dest_root}', f'{dest_root}/job1', f'{dest_root}/job2']
    # cmd = send_multiple(src=src,
    #                     dest=dest,
    #                     user ='tilmann', host='saga.sigma2.no')
    # print(cmd)
#     src = [f'{dest_root}/test0.txt', f'{dest_root}/job1/*', f'{dest_root}/job2']
#     # dest = [f'{src_root}/received/'] * 3
#     dest = [f'{src_root}/received/', f'{src_root}/received/job1',
#             f'{src_root}/received/job2']
#     cmd = receive_multiple(src=src,
#                            dest=dest,
#                            user=os.getlogin(),
#                            host='saga.sigma2.no')
#     print(cmd)
