from shlex import split
from subprocess import CompletedProcess, run
from typing import Optional, Union

from hytools.logger import Logger, get_logger

from .rsync_get import rsync_get
from .rsync_put import rsync_put


def rsync(logger: Optional[Logger] = None,
          download: Optional[bool] = False,
          dry_run: Optional[bool] = False, **kwargs
          ) -> Union[str, CompletedProcess]:
    """Wrap around ``rsync_put``/``rsync_get``."""
    logger = logger or get_logger(print_level='ERROR')

    if download:
        logger.debug('rsync download')
        cmd = rsync_get(**kwargs)
    else:
        logger.debug('rsync upload')
        cmd = rsync_put(**kwargs)

    logger.debug('rsync command: {}'.format(cmd))
    if dry_run:
        return cmd
    logger.debug('Executing rsync command')
    return run(split(cmd), capture_output=True, text=True, shell=False)
    # return args[0].local(cmd)
