from getpass import getuser
from subprocess import CompletedProcess
from typing import Optional, Union

from fabric import Connection as Connection1
from fabric.connection import Connection as Connection2
from invoke.exceptions import UnexpectedExit

from hytools.logger import Logger, get_logger

from ..abc import Connection as ConnectionBase

# from paramiko.ssh_exception import SSHException

connection_type = Union[dict, Connection1, Connection2]


class SSHFabric(ConnectionBase):
    """SSH connection."""

    def __init__(self,
                 host: Optional[str] = None,
                 user: Optional[str] = None,
                 port: Optional[int] = None,
                 logger: Optional[Logger] = None,
                 connect_kwargs: Optional[dict] = None,
                 connection: Optional[connection_type] = None,
                 **kwargs):
        """Initialize the connection."""
        self.connection_type = 'ssh_fabric'
        self.host = host
        self.user = user or getuser()
        self.port = port or 22
        self.logger = logger or get_logger()
        self.connect_kwargs = (connect_kwargs or
                               kwargs.pop('connect_kwargs', {}))
        self.connection = connection or self.setup(kwargs)

    def teardown(self) -> dict:
        """Teardown the connection."""
        self.close()
        return {'connection_type': self.connection_type,
                'host': self.host,
                'user': self.user,
                'port': self.port,
                'connect_kwargs': self.connect_kwargs}

    def __hash__(self) -> int:
        """Return the hash of the connection."""
        return hash(self.connection)

    def __eq__(self, other) -> bool:
        """Return True if the connections are equal."""
        return self.connection == other.connection

    def _clean_kwargs(self, kwargs):
        """Remove unwanted keys from kwargs."""
        allowed_keys = ['host', 'user', 'port', 'config', 'gateway',
                        'forward_agent', 'connect_timeout', 'connect_kwargs',
                        'inline_ssh_env']
        # remove all keys not in allowed_keys
        return {k: v for k, v in kwargs.items() if k in allowed_keys}

    def setup(self, setup_dict) -> Connection1:
        """Connect to remote server."""
        self.logger.debug(f'Setting up connection to {self.host}.')
        setup_dict = self._clean_kwargs(setup_dict)
        return Connection1(self.host,
                           user=self.user,
                           port=self.port,
                           connect_kwargs=self.connect_kwargs.copy(),
                           **setup_dict)

    def open(self):
        """Open the connection."""
        if self.is_open:
            self.logger.warning(f'Connection to {self.host} is already open.')
            return
        self.logger.debug(f'Opening connection {self.connection}')
        self.connection.open()

    def close(self):
        """Close the connection."""
        if not self.is_open:
            self.logger.warning(f'Connection to {self.host} is not open.')
            return
        self.logger.debug(f'Closing connection {self.connection}')
        self.connection.close()

    def execute(self, cmd_str: str, **kwargs) -> CompletedProcess:
        """Execute a statement."""
        if not self.is_open:
            self.logger.error(f'Connection to {self.host} is not open.')
            self.open()
        try:
            result = self.connection.run(cmd_str,  # type: ignore
                                         hide='stdout',
                                         **kwargs)
        except UnexpectedExit:
            # test -f might drop this error, therefore it is caught here
            self.logger.warning(f' Unexpected exit running {cmd_str}')
            return CompletedProcess(args=cmd_str,
                                    returncode=1,
                                    stdout='',
                                    stderr='')
        else:
            return CompletedProcess(args=result.command,
                                    returncode=result.exited,
                                    stdout=result.stdout,
                                    stderr=result.stderr)

    def send(self, *args, **kwargs):
        """Put a file on the remote server."""
        return self.connection.put(*args, **kwargs)  # type: ignore

    def receive(self, *args, **kwargs):
        """Get a file from the remote server."""
        return self.connection.get(*args, **kwargs)  # type: ignore

    def __enter__(self):
        """Enter the context."""
        self.open()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        """Exit the context."""
        self.close()

    def rollback(self):
        """Rollback the connection."""
        self.logger.error('Rollback not implemented.')

    @property
    def is_open(self) -> bool:
        """Return True if the connection is open."""
        return getattr(self.connection, 'is_connected', False)

    # @singledispatch
    # def connect(self, connection: connection_type, **kwargs
    #             ) -> Connection1:
    #     """Connect to remote server.

    #     Parameters
    #     ----------
    #     connection : dict or fabric.Connection, optional
    #         Connection (options).

    #     Returns
    #     -------
    #     fabric.Connection
    #         Connection object.

    #     """
    #     if connection is None:
    #         raise ValueError('connection must be a dict or
    # fabric.Connection ' +
    #                          f'not {type(connection)}')
    #     try:
    #         c_dict = connection.__dict__
    #     except AttributeError:
    #         return None
    #         # raise NotImplementedError('connect is not implemented for ' +
    #         #                           f'{type(connection)}')
    #     else:
    #         return self.connect(dict(c_dict), **kwargs)  # type: ignore

    # @connect.register(Connection1)
    # @connect.register(Connection2)
    # def _(self, connection: Union[Connection1,
    #                               Connection2], **kwargs) -> Connection1:
    #     """Connect to remote server.

    #     Parameters
    #     ----------
    #     connection : fabric.Connection
    #         Connection object.

    #     Returns
    #     -------
    #     fabric.Connection
    #         Connection object.

    #     """
    #     if kwargs.get('transfer', None) is not None:
    #         for k, v in kwargs['transfer'].items():
    #             setattr(connection, k, v)
    #     return connection

    # @connect.register(dict)
    # def _(self, connection: dict, **kwargs) -> Connection1:  # type: ignore
    #     """Connect to remote server.

    #     Parameters
    #     ----------
    #     connection : mapping
    #         Connection options.

    #     Returns
    #     -------
    #     fabric.Connection
    #         Connection object.

    #     Raises
    #     ------
    #     ValueError
    #         If host is not specified in options.

    #     """
    #     copt = connection.copy()
    #     connection_keys = ['host',
    #                        'user',
    #                        'port',
    #                        'config',
    #                        'gateway',
    #                        'forward_agent',
    #                        'connect_timeout',
    #                        'connect_kwargs',
    #                        'inline_ssh_env']

    #     transfer_keys = ['auto_connect', 'max_connect_attempts']
    #     transfer_values = [copt.pop(k, None) for k in transfer_keys]
    #     transfer = dict(zip(transfer_keys, transfer_values))
    #     copt = {k: copt.pop(k) for k in connection_keys if k in copt}
    #     if any(copt[k] == '' for k in ['host', 'user']):
    #         raise ValueError('host and user must be specified in ' +
    #                          'connection_options')
    #     # host = copt.pop('host', None)
    #     host = self.host
    #     copt['user'] = self.user
    #     # if host is None:
    #     #     raise ValueError('host must be specified in'
    #     #                      + 'connection_options')
    #     self.logger.debug(f'Connecting to {host} with {copt}')
    #     return self.connect(Connection1(host, **copt),
    #                         transfer=transfer,
    #                         **kwargs)
