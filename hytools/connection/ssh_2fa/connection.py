from ..ssh_multiplex import SSHMultiplexed


class SSH2FA(SSHMultiplexed):
    """Dummy connection for testing."""

    def __init__(self, *args, **kwargs):
        """Initialize the connection."""
        super().__init__(*args, **kwargs)
        self.connection_type = 'ssh_2fa'
