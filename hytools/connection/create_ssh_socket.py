import argparse

from hytools.connection.get_connection import get_connection
from hytools.logger import get_logger


def create_ssh_socket(args):
    """Create an ssh socket."""
    logger = get_logger(print_level='DEBUG'
                        if getattr(args, 'verbose', 'ERROR') else 'ERROR')

    connection = get_connection(
        connection_type='ssh_2fa',
        ssh_socket_path=args.ssh_socket_path,
        host=args.host,
        user=args.user,
        port=args.port,
        logger=logger,
        connect_kwargs=args.connect_kwargs,
        timeout=args.timeout
    )
    if args.force:
        connection.close()
    connection.open()

    if connection.is_open:
        timeout_str = (connection.timeout_to_str(connection.timeout)
                       if not isinstance(connection.timeout, bool)
                       else 'indefinite')
        msg = (f'Connection to {connection.host} established.\n'
               f'Socket location: {connection.ssh_socket_path}\n'
               f'Timeout: {timeout_str}')
    else:
        msg = f'Connection to {connection.host} failed.'

    print(msg)


def parse_args():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(prog='python create_ssh_tunnel.py')
    parser.add_argument('--ssh_socket_path', help='Location of socket path.')
    parser.add_argument('--host', required=True, help='Host to connect to.')
    parser.add_argument('--user', help='User to connect as.')
    parser.add_argument('--port', type=int, help='Port to connect to.')
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('-f', '--force', action='store_true',
                        help='Force terminating already open connection.')
    parser.add_argument('--connect_kwargs', help='Connection kwargs.')
    parser.add_argument('--timeout', help='Lifetime of the ssh socket.')
    return parser.parse_args()


def main():
    """Run function."""
    args = parse_args()
    create_ssh_socket(args)


if __name__ == '__main__':
    main()
