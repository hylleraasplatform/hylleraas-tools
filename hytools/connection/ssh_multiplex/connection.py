import hashlib
from datetime import timedelta
from getpass import getuser
from pathlib import Path
from subprocess import CompletedProcess, run
from typing import List, Optional, Union

from hytools.logger import Logger, get_logger
from hytools.time import get_timedelta

from ..abc import Connection as ConnectionBase
from ..rsync import rsync


class SSHMultiplexed(ConnectionBase):
    """Dummy connection for testing."""

    def __init__(self,
                 ssh_socket_path: Optional[Union[str, Path]] = None,
                 host: Optional[str] = None,
                 user: Optional[str] = None,
                 port: Optional[int] = None,
                 timeout: Optional[Union[bool, int, float, str,
                                   timedelta]] = None,
                 logger: Optional[Logger] = None,
                 connect_kwargs: Optional[Union[dict, str]] = None,
                 **kwargs):
        """Initialize the connection."""
        # super().__init__(*args, **kwargs)
        self.connection_type = 'ssh_multiplexed'
        self.host = host
        self.user = user or getuser()
        self.port = port or 22
        self.timeout = timeout or True

        self.ssh_socket_path = (ssh_socket_path or
                                kwargs.pop('ssh_ssh_socket_path', None) or
                                f'~/.ssh/{self.user}@{self.host}:{self.port}')
        self.logger = logger or get_logger(print_level='ERROR')
        self.reopen = kwargs.get('reopen', False)
        self.connect_kwargs: str = self._convert_connect_kwargs(
            connect_kwargs=connect_kwargs)

    def timeout_to_str(self, timeout: Union[bool, int, float, str,
                                            timedelta]) -> str:
        """Convert timeout to string."""
        if isinstance(timeout, bool):
            return 'yes' if timeout else 'no'
        return self.time_delta_to_ssh_format(get_timedelta(timeout))

    def time_delta_to_ssh_format(self, time: timedelta) -> str:
        """Convert timedelta to ssh format."""
        total_seconds = int(time.total_seconds())
        days, remainder = divmod(total_seconds, 86400)
        hours, remainder = divmod(remainder, 3600)
        minutes, seconds = divmod(remainder, 60)

        time_components = [
            f'{days}d' if days > 0 else '',
            f'{hours}h' if hours > 0 else '',
            f'{minutes}m' if minutes > 0 else '',
            f'{seconds}s' if seconds > 0 else ''
        ]
        return ''.join(time_components)

    def teardown(self) -> dict:
        """Teardown the connection."""
        if self.timeout and not isinstance(self.timeout, bool):
            self.logger.info('Closing connection.')
            self.close()
        else:
            self.logger.warning('Connection still open.')
        return {'connection_type': self.connection_type,
                'host': self.host,
                'user': self.user,
                'port': self.port,
                'ssh_socket_path': self.ssh_socket_path,
                'reopen': self.reopen,
                'timeout': self.timeout,
                'connect_kwargs': self.connect_kwargs}

    def __hash__(self) -> int:
        """Return the hash of the connection."""
        hash_str = f'{self.ssh_socket_path}{self.host}{self.user}'
        hash_str += f'{self.connect_kwargs}'
        return int(hashlib.sha256(hash_str.encode()).hexdigest(), 16)

    def __eq__(self, other):
        """Return True if the connections are equal."""
        return hash(self) == hash(other)

    def _convert_connect_kwargs(self,
                                connect_kwargs: Optional[Union[dict,
                                                               str]] = None
                                ) -> str:
        """Convert connect_kwargs to a string."""
        if isinstance(connect_kwargs, dict):
            return ' '.join(f'{k}={v}' for k, v in connect_kwargs.items()
                            if v is not None)
        return connect_kwargs or ''

    def open(self, args: Optional[List[str]] = None):
        """Open the connection."""
        if self.is_open:
            self.logger.debug(f'Connection to {self.host} is already open.')
            return

        default_args = ['-CX', '-fN',
                        '-o', 'ControlMaster=auto',
                        '-o', f'ControlPath={self.ssh_socket_path}',
                        '-o',
                        f'ControlPersist={self.timeout_to_str(self.timeout)}',
                        '-o', 'ServerAliveInterval=30']
        args = args or default_args
        if int(getattr(self.logger, 'level', 11)) <= 10:
            args += ['-o', 'LogLevel=DEBUG1']
        cmd = ['ssh']
        cmd += args
        cmd += [f'-p {self.port}']
        cmd += [f'{self.user}@{self.host}']

        self.logger.info(f'Opening connection to {self.host} with {cmd}')

        socket = run(cmd, capture_output=True, text=True, shell=False)

        if socket.stdout:
            self.logger.critical(f'ssh master socket open:\n{socket.stdout}')
        self.logger.debug(f'ssh master socket stderr:\n{socket.stderr}')

    def close(self, args: Optional[List[str]] = None):
        """Close the connection."""
        if not self.is_open:
            self.logger.warning(f'Connection to {self.host} is not open.')
            return
        default_args = ['-O', 'exit',
                        '-o', f'ControlPath={self.ssh_socket_path}']
        args = args or default_args
        cmd = ['ssh',
               f'-p {self.port}']
        cmd += args
        cmd += [f'{self.user}@{self.host}']
        self.logger.info(f'Closing connection to {self.host} with {cmd}')

        result = run(cmd, capture_output=True, text=True)

        self.logger.debug(f'ssh master socket close:\n{result.stderr}')

    def execute(self,
                cmd_str: str,
                args: Optional[List[str]] = None,
                **kwargs) -> Optional[CompletedProcess]:
        """Execute a statement."""
        if not self.is_open:
            if not self.reopen:
                self.logger.error(f'Connection to {self.host} is not open.')
                return None
            self.open()

        default_args = ['-o', f'ControlPath={self.ssh_socket_path}']
        args = args or default_args

        cmd = ['ssh']
        cmd += args
        if self.connect_kwargs:
            cmd += [self.connect_kwargs]
        cmd += [f'-p {self.port}']
        cmd += [f'{self.user}@{self.host}', cmd_str]
        self.logger.debug(f'Executing command: \n{" ".join(cmd)}\n')
        result = run(cmd, capture_output=True, text=True, shell=False)
        return result

    def rollback(self):
        """Rollback the connection."""
        self.logger.error('Rollback not implemented.')

    def __enter__(self):
        """Enter the context."""
        self.open()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        """Exit the context."""
        if self.timeout and not isinstance(self.timeout, bool):
            self.logger.info('Closing connection.')
            self.close()

    @property
    def is_open(self) -> bool:
        """Return True if the connection is open."""
        p = Path(self.ssh_socket_path).expanduser()
        return p.exists()

    def send(self, *args, **kwargs):
        """Send object via rsync+ssh."""
        return rsync(logger=self.logger, download=False, **kwargs)

    def receive(self, *args, **kwargs):
        """Receive object via rsync+ssh."""
        return rsync(logger=self.logger, download=True, **kwargs)
