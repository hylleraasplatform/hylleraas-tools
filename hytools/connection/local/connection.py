import os
from socket import gethostname

from hytools.logger import get_logger

from ..abc import Connection


class LocalConnection(Connection):
    """Local connection for testing."""

    def __init__(self, *args, **kwargs):
        """Initialize the connection."""
        super().__init__(*args, **kwargs)
        self.connection_type = 'local'
        self._is_open = False
        self.logger = kwargs.get('logger', get_logger(print_level='ERROR'))
        self.host = kwargs.get('host', 'localhost')
        self.port = kwargs.get('port', 22)
        self.user = kwargs.get('user', os.getlogin())
        if self.host not in [gethostname(), 'localhost']:
            self.logger.warning(f'Host {self.host} does not match ' +
                                f'socket.gethostname() ({gethostname()}).')

    def __hash__(self) -> int:
        """Return the hash of the connection."""
        return hash(self._is_open)

    def __eq__(self, other):
        """Return True if the connections are equal."""
        return self._is_open == other._is_open

    def open(self):
        """Open the connection."""
        self._is_open = True

    def close(self):
        """Close the connection."""
        self._is_open = False

    def execute(self, *args, **kwargs):
        """Execute a statement."""
        return None

    def rollback(self):
        """Rollback the connection."""
        pass

    def __enter__(self):
        """Enter the context."""
        self.open()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        """Exit the context."""
        self.close()

    def send(self, *args, **kwargs):
        """Send object."""
        pass

    def receive(self, *args, **kwargs):
        """Receive object."""
        pass

    def teardown(self) -> dict:
        """Teardown the connection."""
        self.close()
        return {'connection_type': self.connection_type,
                'host': self.host}

    @property
    def is_open(self) -> bool:
        """Return True if the connection is open."""
        return self._is_open
