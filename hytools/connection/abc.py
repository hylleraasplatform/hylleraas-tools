from abc import ABC, abstractmethod

from hytools.logger import Logger, get_logger


class Connection(ABC):
    """Abstract base class for a connection."""

    def __init__(self, *args, **kwargs):
        """Initialize the connection."""
        self.connection_type = kwargs.get('connection_type', 'local')

    @abstractmethod
    def __hash__(self):
        """Return the hash of the connection."""
        pass

    @abstractmethod
    def __eq__(self, other):
        """Return True if the connections are equal."""
        pass

    @abstractmethod
    def open(self):
        """Open the connection."""
        pass

    @abstractmethod
    def close(self):
        """Close the connection."""
        pass

    @abstractmethod
    def execute(self, *args, **kwargs):
        """Execute a statement."""
        pass

    @abstractmethod
    def rollback(self):
        """Rollback the connection."""
        pass

    @abstractmethod
    def __enter__(self):
        """Enter the context."""
        pass

    @abstractmethod
    def __exit__(self, exc_type, exc_value, traceback):
        """Exit the context."""
        pass

    @property
    @abstractmethod
    def is_open(self) -> bool:
        """Return True if the connection is open."""
        pass

    def send(self, *args, **kwargs):
        """Send object via connection."""
        pass

    def receive(self, *args, **kwargs):
        """Receive object via connection."""
        pass


def connector(func):
    """Decorate a function for runningwith a connection."""
    def with_connection(*args, connection=None, logger=None, **kwargs):
        """Run a function with a connection."""
        logger: Logger = logger or get_logger()
        conn: Connection = connection or Connection()
        conn.open()
        try:
            rv = func(conn, *args, **kwargs)
        except Exception:
            conn.rollback()
            logger.error(f'Error in {func.__name__}')
            raise
        # else:
        #     conn.commit()
        finally:
            conn.close()
        return rv
    return with_connection
