# from .ssh_multiplex import SSHMultiplexed
from hytools.logger import get_logger

from .abc import Connection
from .local import LocalConnection
from .ssh_2fa import SSH2FA
from .ssh_fabric import SSHFabric
from .ssh_multiplex import SSHMultiplexed

CONNECTION_MAP = {'local': LocalConnection,
                  'ssh_fabric': SSHFabric,
                  'ssh_2fa': SSH2FA,
                  'ssh_multiplexed': SSHMultiplexed}


def get_connection(**kwargs) -> Connection:
    """Get a connection object.

    Parameters
    ----------
    **kwargs
        Connection options

    Returns
    -------
    fabric.Connection
        Connection object.

    """
    logger = kwargs.pop('logger', get_logger(print_level='ERROR'))
    connection = kwargs.pop('connection_type', 'ssh_fabric')
    logger.debug(f'Connection type: {connection}')
    return CONNECTION_MAP[connection](logger=logger, **kwargs)

    # if kwargs.pop('two_factor', False):
    #     return SSH2FA(**kwargs)
    # kwargs.pop('ssh_socket_path', None)

    # return SSHFabric(**kwargs)
