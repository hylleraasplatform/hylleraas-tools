from typing import Any, Optional

from .convert_molecule import ConvertMolecule


def convert(input_object: Any, input_type: str, output_type: str, options: Optional[dict] = None) -> Any:
    """Convert objects.

    Parameters
    ----------
    input_object : :obj:`Any`
            object to be converted
    input_type : str
            origin type
    output_type : str
            destination type
    options : dict, optional
            options

    Returns
    -------
    :obj:`Any`
        depending on input, e.g. str for xyz -> pdb

    """
    if hasattr(input_object, 'atoms') and hasattr(input_object, 'coordinates'):
        # if isinstance(input_object, Molecule):
        converter = ConvertMolecule(input_object)
        if input_type == 'xyz':
            if output_type == 'pdb':
                return converter.xyz2pdb(options)
            elif output_type == 'zmat':
                return converter.xyz2zmat()
            elif output_type.lower() == 'smiles':
                return converter.xyz2smiles(options)
            elif output_type.lower() == 'daltonproject':
                return converter.daltonproject()
            elif output_type.lower() == 'mol':
                return converter.xyz2mol()
            elif output_type.lower() == 'xyz':
                return converter.xyz2xyz()
        elif input_type == 'zmat':
            raise NotImplementedError(
                "Correct usage : convert(list,'zmat', 'xyz'")

    if isinstance(input_object, list):
        if input_type == 'zmat':
            if output_type == 'xyz':
                return ConvertMolecule.zmat2xyz(input_object)
    if isinstance(input_object, str):

        if input_type == 'smiles':
            try:
                from rdkit import Chem
                from rdkit.Chem import AllChem  # noqa: F401
            except Exception:
                raise ImportError('could not find package rdkit')

            mol = Chem.MolFromSmiles(input_object)
            mol = Chem.AddHs(mol)
            Chem.AllChem.EmbedMolecule(mol)
            Chem.AllChem.MMFFOptimizeMolecule(mol)
            lines = Chem.AllChem.MolToXYZBlock(mol).split('\n')
            num_atoms = int(lines[0])
            atoms = []
            coordinates = []
            for j in range(2, 2 + num_atoms):
                atom, x, y, z = lines[j].split()
                atoms.append(atom)
                coordinates.append([float(x), float(y), float(z)])

            return atoms, coordinates


if __name__ == '__main__':
    smiles = 'CN=C=O'
    atoms, coords = convert(smiles, 'smiles', 'xyz')
    print(atoms, coords)
    import hylleraas as hsp
    mymol = hsp.Molecule({'atoms': atoms, 'coordinates': coords})
    mol = convert(mymol, 'xyz', 'mol')
    print(mol)
