from .memory import get_memory

__all__ = ['get_memory']
