from functools import singledispatch
from typing import Optional, Union


def get_memory_scale(memory, base=1024, max_number=1000):
    """Return the memory scale."""
    units = ['', 'K', 'M', 'G', 'T', 'P']
    power = 0
    while memory >= max_number and power < len(units) - 1:
        memory /= base
        power += 1
    unit = units[power]
    if base == 1024 and unit:
        unit += 'i'
    return (base**power, unit)


@singledispatch
def get_memory(memory: Union[int, float, str, dict],
               base: Optional[int] = 1024) -> Optional[str]:
    """Convert memory to bytes."""
    if memory is None:
        return '0 B'
    raise TypeError(f'Cannot convert {type(memory)} to memory.')


@get_memory.register(float)
@get_memory.register(int)
def _(memory: Union[float, int],
      base: Optional[int] = 1024) -> str:
    """Return the memory."""
    scale = get_memory_scale(float(memory), base=base)
    return f'{float(memory)/scale[0]:.2f} {scale[1]}B'


@get_memory.register(str)
def _(memory: str, base: Optional[int] = 1024) -> str:
    """Return the memory."""
    if not memory:
        return '0 B'

    try:
        memory_ = float(memory)  # Try to convert to int
    except ValueError:
        memory_str = memory.replace(' ', '').replace('B', '').replace('i', '')
        unit = memory_str[-1].upper() if memory_str[-1].isalpha() else ''
        memory_ = float(memory_str[:-1])
        if unit in 'KMGT':
            power = 'KMGT'.index(unit) + 1
            base = 1024 if 'i' in memory else 1000
            memory_ *= base**power
        # else:
        #     raise ValueError(f'Invalid memory unit: {unit}')
    scale = get_memory_scale(memory_, base=base)
    return f'{memory_/scale[0]:.2f} {scale[1]}B'
