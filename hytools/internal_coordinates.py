import warnings
from dataclasses import dataclass
from typing import Any, Optional

import numpy as np
from qcelemental import PhysicalConstantsContext, covalentradii

constants = PhysicalConstantsContext('CODATA2018')


@dataclass
class Molecule:
    """Molecule class for ensuring compatibility."""

    atoms: list
    coordinates: np.array
    properties: dict


class InternalCoordinates:
    """Main class for manipulating internal coordinates."""

    def __init__(self):
        pass

    def convert_molecule(self, mol):
        """Convert molecule."""
        if hasattr(mol, '_coordinates'):
            coord = mol._coordinates
            atoms = mol._atoms
            prop = mol._properties

        return Molecule(atoms=atoms, coordinates=coord, properties=prop)

    def initiate(self, molecule: Any, constraints: Optional[dict] = None):
        """Initiate internal coordinates."""
        constraints = {} if constraints is None else constraints

        self.molecule = self.convert_molecule(molecule)
        self.atoms = self.molecule.atoms
        self.coords = self.molecule.coordinates
        self.n = len(self.atoms)
        self.tmat = self.get_topology_matrix()
        # self.bonds = self.get_bonds(self.tmat, constraints.get('bonds', {}))

        self.stretch = self.get_bond_definitions(self.tmat)
        # print('strech definitions', self.stretch)
        # next step : contract
        # for contraction compute part of b-matrix
        b1 = self.get_grad_stretch(self.stretch)

        b1 = self.get_bmat_bond(self.stretch)
        print('new bmat', b1)

        g = b1.T @ b1
        w, v = np.linalg.eigh(g)
        print('eigenvalues bond', w)
        # print('eigenvectors', v )
        # print('atoms', self.atoms)
        self.bend = self.get_angle_definitions(self.tmat)
        b2 = self.get_bmat_angle(self.bend)
        g = b2.T @ b2

        w, v = np.linalg.eigh(g)
        print('eigenvalues  angle', w)
        # print('eigenvectors', v )
        print('atoms', self.atoms)

        # vec = [ v[:,i]  for i in range(len(w)) if w[i] > 0.002]
        # print('bond contraction:', len(B1), len(vec), self.n*3-6)
        # bonds = self.get_bonds(self.stretch)
        # # print(bonds)
        # # self.contract(self.bonds)
        # # then : make sure the constraints are there
        # self.bend=self.get_angle_definitions(self.tmat)
        # B2 = self.get_grad_bend(self.bend)
        # print(self.bend)
        # # print(B2)
        # G =B2@B2.T
        # print('gmat', G)
        # w,v= np.linalg.eigh(G)
        # print('angles', w)
        # vec = [ v[:,i]  for i in range(len(w)) if w[i] > 0.002]
        # print('angle contraction:', len(B2), len(vec), self.n*3-6)
        print(np.shape(b1), np.shape(b2))
        b3 = np.append(b1, b2, axis=1)
        print(np.shape(b3))
        # print('B1', B1)
        # print('B2', B2)
        # print('B3', B3)
        g = b3.T @ b3
        w, v = np.linalg.eigh(g)
        print('Gtot eigenveluea', w)
        # u,s,vh = np.linalg.svd(G)
        # print(G, s)
        vec = [v[:, i] for i in range(len(w)) if w[i] > 0.002]
        print(
            'angle+bond contraction:', np.shape(b3), len(vec), self.n * 3 - 6
        )

        self.tors = self.get_torsion_definitions(self.tmat)
        print('created torsions', self.tors, len(self.tors))
        b4 = self.get_bmat_tors(self.tors)
        g = b4.T @ b4
        print(np.shape(b4))
        w, v = np.linalg.eigh(g)
        print('Gtors eigenveluea', w)

        b5 = np.append(b3, b4, axis=1)
        g = b5.T @ b5
        w, v = np.linalg.eigh(g)
        print('Gtot eigenvalues', w)
        vec = [v[:, i] for i in range(len(w)) if w[i] > 0.002]
        print(
            'angle+bond+tors contraction:',
            np.shape(b3),
            len(vec),
            self.n * 3 - 6,
        )

    def get_bmat_tors(self, torsions):
        """Get torsion part of wilson B matrix."""
        bmat = np.zeros((3 * self.n, len(torsions)))

        i = 0
        for tors in torsions:
            ii, jj, kk, ll = tors

            e12 = self.coords[jj] - self.coords[ii]
            e23 = self.coords[kk] - self.coords[jj]
            e43 = self.coords[kk] - self.coords[ll]

            r12 = np.linalg.norm(e12)
            e12 = e12 / r12
            r23 = np.linalg.norm(e23)
            e23 = e23 / r23
            r43 = np.linalg.norm(e43)
            e43 = e43 / r43
            e34 = -1.0 * e43
            e32 = -1.0 * e23

            phi2 = np.arccos(e12 @ e23)
            phi3 = np.arccos(e23 @ e34)

            v1 = np.cross(e12, e23) / np.sin(phi2)
            v2 = np.cross(e23, e34) / np.sin(phi3)
            prod = min(v1 @ v2, 1.0)

            tau = np.arccos(prod)

            print('dihedral angkes', tau * 180.0 / np.pi)
            st1 = (
                -1.0 * np.cross(e12, e23) / (r12 * np.sin(phi2) * np.sin(phi2))
            )
            st2 = (
                (r23 - r12 * np.cos(phi2))
                / (r23 * r12 * np.sin(phi2))
                * np.cross(e12, e23)
                / np.sin(phi2)
            )
            st2 -= (
                np.cos(phi3)
                / (r23 * np.sin(phi3))
                * np.cross(e43, e32)
                / (np.sin(phi3))
            )

            # 1->4
            # 4->1
            # 2->3
            # 3->2
            st3 = (
                (r23 - r43 * np.cos(phi2))
                / (r23 * r43 * np.sin(phi2))
                * np.cross(e43, e32)
                / np.sin(phi2)
            )
            st3 -= (
                np.cos(phi3)
                / (r23 * np.sin(phi3))
                * np.cross(e12, e23)
                / (np.sin(phi3))
            )
            st4 = (
                -1.0 * np.cross(e43, e32) / (r43 * np.sin(phi2) * np.sin(phi2))
            )

            norm = np.linalg.norm(np.array([st1, st2, st3, st4]))

            bmat[ii * 3: ii * 3 + 3, i] = st1 / norm
            bmat[jj * 3: jj * 3 + 3, i] = st2 / norm
            bmat[kk * 3: kk * 3 + 3, i] = st3 / norm
            bmat[ll * 3: ll * 3 + 3, i] = st4 / norm

            i += 1

        return bmat

    def get_torsion_definitions(self, tmat):
        """Get torsion definitions."""
        tors = []
        if self.n < 4:
            return tors
        bonds = []
        for i in range(self.n):
            terminal = np.sum(tmat[i, :])
            if terminal == 1:
                continue
            for j in range(self.n):
                if tmat[i, j] == 0:
                    continue
                bonds.append([i, j])

        # now we need to match 3 bonds to get

        for i1, j1 in bonds:
            bond1 = [i1, j1]
            iadded = 0
            for i2, j2 in bonds:
                if iadded > 0:
                    continue
                # if not any([i2 in [i1,j1], j2 in [i1,j1]]):
                #     continue
                # print('found', i1,j1,i2,j2)
                if not any([j1 in [i2, j2], i1 in [i2, j2]]):
                    continue

                res = [*set([i1, j1, i2, j2])]
                if len(res) < 3:
                    continue

                # if i2== bond1[0]:
                #     triple = [j2, *bond1]
                # elif i2 == bond1[-1]:
                #     triple = [*bond1, j2]
                # elif j2 == bond1[0]:
                #     triple = [i2, *bond1]
                # elif j2 == bond1[-1]:
                #     triple = [*bond1, i2]

                if i2 == bond1[-1]:
                    triple = [*bond1, j2]
                else:
                    continue

                for i3, j3 in bonds:
                    if iadded > 0:
                        continue
                    if not any(
                        [triple[0] in [i3, j3], triple[-1] in [i3, j3]]
                    ):
                        continue
                    res = [*set([*triple, i3, j3])]
                    if len(res) < 4:
                        continue

                    # if i3 == triple[0]:
                    #     quadruple = [j3, *triple]
                    # elif i3 == triple[-1]:
                    #     quadruple = [*triple,  j3]
                    # elif j3 == triple[0]:
                    #     quadruple = [i3, *triple]
                    # elif j3 == triple[-1]:
                    #     quadruple = [*triple, i3]
                    # else:
                    #     continue
                    if i3 == triple[-1]:
                        quadruple = [*triple, j3]

                    else:
                        continue

                    iadded = 1
                    tors.append(quadruple)

        print('torsions', tors)
        return tors

        # if len(triple)<3:
        #     continue
        # # print('triple', triple)
        # for i3,j3 in bonds:
        #     if not triple[-1] in [i3,j3]:
        #         continue
        #     print('found', triple, i3,j3)
        # for i3,j3 in bonds:
        #     if not any([i3 in [i2,j2], j3 in [i2,j2]]):
        #         continue
        #     res = [*set([i1,j1,i2,j2,i3,j3])]
        #     if len(res)!= 4:
        #         continue
        #     tors.

        #     print('BINGEO', res, len(res))

        # for i,j in bonds:
        #     unused = [ k for k in range(self.n) if not any([k==i, k==j])]
        #     center = (self.coords[i] + self.coords[j])/2.0
        #     dist =[ np.linalg.norm(center-self.coords[k])  for k in unused ]
        #     # print(unused, dist)
        #     mindist1_i = np.argmin(dist)
        #     idx1 = unused[mindist1_i]
        #     dist[mindist1_i] = 1e6
        #     mindist2_i = np.argmin(dist)
        #     idx2 = unused[mindist2_i]
        #     # print(idx1,idx2)
        #     tors.append([i,j,idx1,idx2])

        # return tors
        # # lets cleanup
        # for i in range(len(tors)):
        #     i1,i2,i3,i4 = tors[i]
        #     if sum(tors[i]) == 0 :
        #         continue
        #     # print(tors[i])
        #     for j in range(i+1, len(tors)):
        #         t = tors[j]
        #         if all([i1 in t, i2 in t, i3 in t, i4 in t]):
        #             tors[j] = [0,0,0,0]
        #             # print('duoplicate fgound', j,t)

        # # print(tors)
        # return [ tors[i] for i in range(len(tors)) if sum(tors[i])>0]

        # mindist2_i = np.argmin(dist)
        # idx2 = unused[mindist2_i]
        # mindist2 = dist.pop(mindist2_i)
        # print(idx1,idx2)

        # print(i,j,unused, dist)
        # tors.append(i,j,unused[0], unused)

    def get_bmat_angle(self, bends):
        """Get angle part for wilson B matrix."""
        bmat = np.zeros((3 * self.n, len(bends)))

        for i, bend in enumerate(bends):
            jj, ii, kk = bend

            e31 = self.coords[ii] - self.coords[jj]
            e32 = self.coords[ii] - self.coords[kk]

            r31 = np.linalg.norm(e31)
            r32 = np.linalg.norm(e32)
            e31 = e31 / r31
            e32 = e32 / r32
            phi = np.arccos(e31 @ e32)
            st1 = (np.cos(phi) * e31 - e32) / (r31 * np.sin(phi))
            st2 = (np.cos(phi) * e32 - e31) / (r32 * np.sin(phi))
            st3 = -st1 - st2

            norm = np.linalg.norm(np.array([st1, st2, st3]))

            bmat[ii * 3: ii * 3 + 3, i] = st3 / norm
            bmat[jj * 3: jj * 3 + 3, i] = st2 / norm
            bmat[kk * 3: kk * 3 + 3, i] = st1 / norm

        return bmat

    def get_bmat_bond(self, stretch):
        """Get bond part of wilson B matrix."""
        bmat = np.zeros((3 * self.n, len(stretch)))

        for i, stre in enumerate(stretch):
            idx1, idx2 = stre
            coord1 = self.coords[idx1]
            coord2 = self.coords[idx2]
            e12 = coord1 - coord2
            r12 = np.linalg.norm(e12)
            e12 = e12 / r12
            # 1 < 2

            norm = np.linalg.norm(np.array([e12, -e12]))

            bmat[3 * idx1: 3 * idx1 + 3, i] = -e12 / norm
            bmat[3 * idx2: 3 * idx2 + 3, i] = e12 / norm

        return bmat

        # for i in range(self.n):
        #     istart = i*3+1
        #     if
        #     ix = i*3+1
        #     iy = i*3+2
        #     iz = i*3+3
        #     print(i,ix,iy,iz)

    # nq = len(bonds)
    #       grads = np.array((self.n, nq))

    def get_grad_bend(self, angles):
        """Get gradients for bond bendings."""
        grads = []
        for i, j, k in angles:
            # 3->i
            # 1->j
            # 2->k

            e31 = self.coords[i] - self.coords[j]
            e32 = self.coords[i] - self.coords[k]
            r31 = np.linalg.norm(e31)
            r32 = np.linalg.norm(e32)
            e31 = e31 / r31
            e32 = e32 / r32
            phi = np.arccos(e31 @ e32)
            st1 = (np.cos(phi) * e31 - e32) / (r31 * np.sin(phi))
            st2 = (np.cos(phi) * e32 - e31) / (r32 * np.sin(phi))
            st3 = -st1 - st2
            grads.append(st1)
            grads.append(st2)
            grads.append(st3)
        return np.array(grads)

    def get_angle_definitions(self, tmat):
        """Get angle definitions."""
        angles = []
        for i in range(self.n):
            terminal = np.sum(tmat[i, :])
            if terminal == 1:
                continue
            connections = [j for j in range(self.n) if tmat[i, j] == 1]
            is_planar = False
            if len(connections) > 2:
                planarity = 0.0
                vecs = []
                for j in range(len(connections)):
                    vec = self.coords[connections[j]] - self.coords[i]
                    norm = np.linalg.norm(vec)
                    vecs.append(vec / norm)
                for j in range(2, len(connections)):
                    ivec = vecs[j]
                    jvec = vecs[j - 1]
                    kvec = vecs[j - 2]
                    planarity += np.abs(ivec @ (np.cross(jvec, kvec)))
                if planarity < 0.5:
                    is_planar = True
                    print(f'Fragment {i} is planar, {planarity}')
            for j in range(len(connections)):
                jj = connections[j]
                for k in range(j + 1, len(connections)):
                    kk = connections[k]
                    angles.append([jj, i, kk])
            if is_planar:
                print('fragmen is planar, adding all other angels')
                for j in range(self.n):
                    if i == j:
                        continue
                    for k in range(self.n):
                        if i == k:
                            continue
                        if j == k:
                            continue
                        angles.append([j, i, k])

        #         not_connected = [ j for j in range(n) if j not in connections]
        #         for j in range(n_connections):
        #             for k in range(len(not_connected)):
        #                 idx1 = connections[j]
        #                 idx2 = not_connected[k]
        #                 ifound=False
        #                 for constraint in constraints:
        #                     if all([i in constraint[0:3], idx1 in constraint[0:3], idx2 in constraint[0:3]]):
        #                         ifound=True
        #                         theta = constraint[3]
        #                 if ifound:
        #                     angles.append([*constraint[0:3], theta])
        #                     continue
        #                 u = coord[idx1]-coord[i]
        #                 norm = np.linalg.norm(u)
        #                 if norm <thresh:
        #                     print('WARNING,u ')
        #                     continue
        #                 u = u/norm
        #                 v = coord[idx2]-coord[i]
        #                 norm=np.linalg.norm(v)
        #                 if norm <thresh:
        #                     print('WARNING,v ')
        #                     continue
        #                 v=v/norm

        #                 theta = np.arccos(u@v)*180.0/np.pi
        #                 print('ANGLES ADDED', [idx1,i,idx2, theta])
        #                 angles.append([idx1,i,idx2, theta])

        # print('angles=', angles)

        return angles

    def get_grad_stretch(self, bonds):
        """Get gradients for bond stretches."""
        grads = []
        for bond in bonds:
            coord1, coord2 = self.coords[bond[0:2]]
            fac = 1.0
            if bond[0] < bond[1]:
                fac = -1.0
            grad = fac * (coord1 - coord2)
            norm = np.linalg.norm(grad)
            grad = grad / norm
            grads.append(grad)

        return np.array(grads)

    def get_bonds(self, bonds):
        """Get bonds."""
        dist = []
        for bond in bonds:
            coord1, coord2 = self.coords[bond[0:2]]
            dist.append(np.linalg.norm(coord1 - coord2))
        return dist

    def get_bond_definitions(self, tmat):
        """Get bond definitions."""
        bonds = []
        for i in range(self.n):
            terminal = np.sum(tmat[i, :])
            if terminal == 1:
                continue
            for j in range(i + 1, self.n):
                if tmat[i, j] == 0:
                    continue
                bonds.append([i, j])
                # bonds.append([j,i])

        # for constraint in bond_constraints:
        #     idx1 = constraint[0]
        #     idx2 = constraint[1]
        #     if [idx1,idx2] in bonds:
        #         continue
        #     if [idx2,idx1] in bonds:
        #         idx = bonds.index([idx2,idx1])
        #         bonds[idx] = [idx1,idx2]
        #         continue
        #     bonds.append([idx1,idx2])

        return bonds

    def get_topology_matrix(self, unit: Optional[str] = None):
        """Get Topology."""
        fac1 = 1.05  # taken from doi: 10.1063/1.479510 (turbomole)
        unit = 'bohr' if unit is None else unit
        tmat = np.zeros((self.n, self.n))
        for i in range(self.n):
            atom1 = self.atoms[i]
            cov_rad1 = covalentradii.get(atom1, units=unit)
            coord1 = self.coords[i]
            for j in range(i + 1, self.n):
                atom2 = self.atoms[j]
                cov_rad2 = covalentradii.get(atom2, units=unit)
                coord2 = self.coords[j]
                dist = np.linalg.norm(coord1 - coord2)
                if dist < fac1 * (cov_rad1 + cov_rad2):
                    tmat[i, j] = 1
                    tmat[j, i] = 1

        # unit bohr, taken from doi: 10.1063/1.479510 (turbomole)
        min_dist = 6.5
        fac2 = 1.2  # unit bohr, taken from doi: 10.1063/1.479510 (turbomole)
        for i in range(self.n):
            subunits = self.get_subunits(tmat)
            if len(subunits) <= 1:
                break
            subunits_min = self.get_min_subunit_dist(subunits)
            for sunit in subunits_min:
                if sunit[2] < min_dist:
                    tmat[sunit[0], sunit[1]] = 1
                    tmat[sunit[1], sunit[0]] = 1
            min_dist *= fac2

        subunits = self.get_subunits(tmat)
        if len(subunits) > 1:
            warnings.warn('Topolgy matrix not fully connected')

        return tmat

    def get_subunits(self, tmat):
        """Get subunits."""
        subunits = []
        for i in range(len(tmat)):
            ifound = 0
            for unit in subunits:
                if i in unit:
                    ifound += 1
            if ifound == 0:
                subunits.append([i])
            for j in range(i + 1, len(tmat)):
                if np.abs(tmat[i][j]) > 1e-16:
                    for unit in subunits:
                        if i in unit:
                            if j not in unit:
                                unit.append(j)

        for i in range(len(tmat)):
            doubles = []
            for j, unit in enumerate(subunits):
                if i in unit:
                    doubles.append(j)
            if len(doubles) <= 1:
                continue
            for i in range(1, len(doubles)):
                subunits = self.merge_sublists(
                    subunits, doubles[i - 1], doubles[i]
                )

        return subunits

    def get_min_subunit_dist(self, subunits: list) -> list:
        """Get minmal distance between subunits."""
        subunits_min = []
        for i in range(len(subunits)):
            unit1 = subunits[i]
            n1 = len(unit1)
            for j in range(i + 1, len(subunits)):
                unit2 = subunits[j]
                n2 = len(unit2)
                mindist = np.linalg.norm(
                    self.coords[unit1[0]] - self.coords[unit2[0]]
                )
                idx = [unit1[0], unit2[0], mindist]
                for atom1 in range(n1):
                    for atom2 in range(n2):
                        dist = np.linalg.norm(
                            self.coords[unit1[atom1]]
                            - self.coords[unit2[atom2]]
                        )
                        if dist < mindist:
                            idx = [unit1[atom1], unit2[atom2], dist]
                subunits_min.append(idx)

        return subunits_min

    # n_subunits = n
    # min_dist = unit_min
    # i=0
    # for i in range(n):
    #     subunits = ConvertMolecule.get_subunits_topology(tmat)
    #     if len(subunits) <= 1:
    #         break
    #     subunits_min = ConvertMolecule.get_min_subunit_dist(coord, subunits)

    #     for unit in subunits_min:
    #         if unit[2] < min_dist:
    #             tmat[unit[0], unit[1]] = 1
    #             tmat[unit[1], unit[0]] = 1

    #     i+=1
    #     min_dist *= fac2

    @classmethod
    def merge_sublists(cls, biglist: list, idx1: int, idx2: int) -> list:
        """Merge two lists."""
        first = biglist[idx1]
        second = biglist.pop(idx2)
        combined = first + list(set(second) - set(first))
        biglist[idx1] = sorted(combined)
        return biglist

        # fac1 = 1.05  # taken from doi: 10.1063/1.479510 (turbomole)
        # fac2 = 1.2  # taken from doi: 10.1063/1.479510 (turbomole)
        # unit_min = 6.5 # unit bohr, taken from doi: 10.1063/1.479510 (turbomole)
        # tmat = np.zeros((n,n))

        # for i in range(n):
        #     atom1 = atoms[i]
        #     cov_rad1= covalentradii.get(atom1, units='bohr')
        #     for j in range(i+1, n):
        #         atom2 = atoms[j]
        #         cov_rad2 = covalentradii.get(atom2, units='bohr')
        #         dist = np.linalg.norm(coord[i]-coord[j])
        #         if (dist  < fac1*(cov_rad1+cov_rad2)):
        #             tmat[i,j] = 1
        #             tmat[j,i] = 1

        # # return tmat

        # n_subunits = n
        # min_dist = unit_min
        # i=0
        # for i in range(n):
        #     subunits = ConvertMolecule.get_subunits_topology(tmat)
        #     if len(subunits) <= 1:
        #         break
        #     subunits_min = ConvertMolecule.get_min_subunit_dist(coord, subunits)

        #     for unit in subunits_min:
        #         if unit[2] < min_dist:
        #             tmat[unit[0], unit[1]] = 1
        #             tmat[unit[1], unit[0]] = 1

        #     i+=1
        #     min_dist *= fac2

        # return tmat


# if __name__ == '__main__':

#     class MMOL:
#         """Test class."""

#         def __init__(self, mol_hsp):

#             self.xyzs = [mol_hsp.coordinates]
#             self.elem = mol_hsp.atoms

#     import hylleraas as hsp
#     mymol = hsp.Molecule('C -0.061684 .67379 0\n C -0.061684 -0.726210 0\nF 1.174443 1.331050 0\n'
#                          + 'H -0.927709 1.173790 0 \n' + 'H -0.927709 -1.226210 0\nH 0.804342 -1.226210 0')
#     mymol2 = mymol2 = hsp.Molecule('''
# O  -17.3102   -0.3814   -0.0567
# C   -5.2228   -1.2332    0.2439
# C   -6.1762   -0.2801    0.5498
# C   -5.9295    1.0541    0.2854
# C   -4.7292    1.4353   -0.2844
# C   -3.7754    0.4824   -0.5895
# C   -4.0226   -0.8520   -0.3264
# C   10.7023   -1.6870   -0.1998
# C   10.0865   -0.3159   -0.0910
# O   10.6984    0.6529   -0.4723
# C    8.7070   -0.1537    0.4936
# C    8.3218    1.3271    0.4887
# H  -17.5629    0.5520   -0.0555
# H  -16.4308   -0.5503   -0.4217
# H   -5.4137   -2.2751    0.4546
# H   -7.1140   -0.5780    0.9949
# H   -6.6747    1.7986    0.5237
# H   -4.5367    2.4777   -0.4916
# H   -2.8378    0.7802   -1.0353
# H   -3.2773   -1.5964   -0.5644
# H   11.6945   -1.6055   -0.6435
# H   10.7836   -2.1286    0.7934
# H   10.0741   -2.3186   -0.8280
# H    7.9906   -0.7174   -0.1039
# H    8.7000   -0.5275    1.5175
# H    8.3288    1.7009   -0.5352
# H    7.3241    1.4444    0.9115
# H    9.0383    1.8908    1.0862''')
#     mymol3 = hsp.Molecule('O 0 0 0 \n H 0 0.7 1 \n H 0 0 -1.')
#     # test= InternalCoordinates()
#     # test.initiate(mymol)
#     bohr2angstrom = 0.529177210903
#     import geometric

#     geometric_molecule = geometric.molecule.Molecule()
#     geometric_molecule.elem = mymol3.atoms
#     geometric_molecule.xyzs = [np.array(mymol3.coordinates) * bohr2angstrom]

#     myint = geometric.optimize.DelocalizedInternalCoordinates(geometric_molecule)
#     xyz = geometric_molecule.xyzs[0]
#     print(myint)
#     print(dir(myint))
#     print(myint.Prims)
#     G = myint.Prims.GMatrix(xyz)
#     # print(G)
#     w, v = np.linalg.eigh(G)
#     print(w)
#     vec = [v[:, i] for i in range(len(w)) if w[i] > 0.002]
#     print('numfwe', len(G), len(vec), 3 * len(geometric_molecule.elem) - 6)
#     dlc = myint.build_dlc(xyz)
#     print(np.shape(myint.Vecs))
#     test = myint.applyConstraints(xyz.ravel())

#     print(test)
#     print('original', xyz.ravel())
#     print('original distace', np.linalg.norm(mymol.coordinates[0] - mymol.coordinates[1]))
#     o1 = mymol3.coordinates[0]
#     h1 = mymol3.coordinates[1]
#     h2 = mymol3.coordinates[2]
#     u = h1 - o1
#     v = h2 - o1
#     angle = np.arccos(u @ v / (np.linalg.norm(u) * np.linalg.norm(v))) * 180.0 / np.pi
#     print('origina angles, dist', angle, np.linalg.norm(u) * bohr2angstrom, np.linalg.norm(v) * bohr2angstrom)
#     dist_to_be = 1.2
#     print('distance to be', dist_to_be / bohr2angstrom)
#     # val = np.pi/180.0*150.
#     # ADD CONSTREINATS AND RE_COMPUTE PRIMITIVES
#     val = 156.0
#     # constraints = geometric.optimize.ParseConstraints(geometric_molecule, f'$set\ndistance 1 2 {dist_to_be}')
#     constraints = geometric.optimize.ParseConstraints(geometric_molecule, f'$set\n angle 2 1 3 {val}')
#     print(constraints, type(constraints))
#     # myint2 = geometric.optimize.DelocalizedInternalCoordinates(geometric_molecule,
#     # constraints=constraints[0], cvals=constraints[1])
#     # print('dqwd', myint2)
#     print('has cons', myint.haveConstraints())
#     aa = myint.addConstraint(constraints[0], constraints[1], xyz)
#     myint.Prims = geometric.optimize.PrimitiveInternalCoordinates(geometric_molecule,
#                                                                   connect=False,
#                                                                   addcart=False,
#                                                                   constraints=constraints[0],
#                                                                   cvals=constraints[1])
#     dlc = myint.build_dlc(xyz)

#     ic_definitions = myint.Prims.Internals
#     print('ic def', ic_definitions)
#     ic = myint.Prims.calculate(xyz)
#     print('values of internal coordiantes', ic)

#     transcoord = myint.applyConstraints(xyz.ravel()).reshape(-1, 3)
#     print(transcoord)
#     print('tras distace', np.linalg.norm(transcoord[0] - transcoord[1]))
#     o1 = transcoord[0]
#     h1 = transcoord[1]
#     h2 = transcoord[2]
#     u = h1 - o1
#     v = h2 - o1
#     angle = np.arccos(u @ v / (np.linalg.norm(u) * np.linalg.norm(v))) * 180.0 / np.pi
#     print('new angles, dist', angle, np.linalg.norm(u), np.linalg.norm(v))

#     # print(dir(myint.Prims),myint.Prims.Internals, myint.Prims.cVals)
#     # # test= myint.Prims.makePrimitives(geometric_molecule, connect=False, addcart=False)
#     # test = myint.Prims.calculate(xyz)
#     # print(test)
#     print(dir(myint))
