import csv
from typing import Any, Optional

import numpy as np
from qcelemental import periodictable

from .convert import convert


def view_molecule_browser(molecule: Any, orbital: Optional[str] = None,
                          resolution: Optional[int] = None):
    """Show hylleraas molecule in browser using javascript.

    Parameters
    ----------
    molecule : :obj:`Any`
        Hylleraas molecule instance
    orbital : str, optional
        filename of orbital file (see below)
    resolution : int, optional
        1d resolution, defaults to 10

    Note
    ----
    * The script will start a local webserver.


    """
    if isinstance(molecule, str):
        if molecule == 'demo':
            filename = './examples/H2O_4.pdb'
            orbital = './examples/cubified15.txt'
            resolution = 15
    else:
        filename = 'hylleraasmolecule.pdb'
        try:
            pdb_string = convert(molecule, 'xyz', 'pdb')
        except Exception:
            raise Exception('could not convert molecule')

        with open(filename, 'w') as text_file:
            text_file.write(pdb_string)

    if orbital is not None and molecule != 'demo':
        data, dims, coord_min, coord_max, atoms, coordinates = \
            PrepareOrbital.read_cub(orbital, return_molecule=True)
        data_cubed = PrepareOrbital.interpolate_field(data, dims,
                                                      [coord_min, coord_max],
                                                      [resolution, resolution,
                                                       resolution], [coord_min,
                                                                     coord_max])
        filename = 'hylleraascub.txt'
        print('coordmin', coord_min, coord_max)
        with open(filename, 'w') as myfile:
            wr = csv.writer(myfile, delimiter=',')
            wr.writerow(data_cubed)

    resolution = 10 if resolution is None else resolution

    VisualizeJavaScript.gen_index_html()
    VisualizeJavaScript.gen_javascript_libs()
    VisualizeJavaScript.gen_javascript_main(filename, orbital, resolution)
    VisualizeJavaScript.gen_startserver()

    # exec(open('startserver.py').read())
    print('\n to view object, run \nexec(open("startserver.py").read())\n'
          ' in your python console, open a browser (with javascript enabled)'
          ' and go to localhost:8080 \n')


class PrepareOrbital:
    """Class to prepare orbitals for visualization."""

    @classmethod
    def read_cub(cls, filename: str, return_molecule=False):
        """Read field f(x,y,z) from Gaussian-style (ascii)-cube file.

        Parameters
        ----------
        filename : str
            filename
        return_molecule: bool, optional
            if to return atoms and coordinates of the molecule.
            Defaults to False.

        """
        # if not os.path.isabs(filename):
        #     os.path.join(os.getcwd(), filename)
        with open(filename, 'r') as cube_file:
            line = cube_file.readline()
            line = cube_file.readline()
            split_line = cube_file.readline().split()
            num_atoms = int(split_line[0])
            origin = np.array(split_line[1:]).astype(np.float64)
            split_line = cube_file.readline().split()
            n1 = int(split_line[0])
            ax1 = np.array(split_line[1:]).astype(np.float64)
            split_line = cube_file.readline().split()
            n2 = int(split_line[0])
            ax2 = np.array(split_line[1:]).astype(np.float64)
            split_line = cube_file.readline().split()
            n3 = int(split_line[0])
            ax3 = np.array(split_line[1:]).astype(np.float64)
            n_tot = n1 * n2 * n3
            coordinates = []
            atoms = []
            for i in range(0, num_atoms):
                split_line = cube_file.readline().split()
                atoms.append(periodictable.to_E(split_line[0]))
                coordinates.append(np.array(split_line[2:]).astype(np.float64))
            val = np.zeros(n_tot)
            i = 0
            for line in cube_file:
                for vval in line.split():
                    val[i] = float(vval)
                    i += 1
            coord_min = origin
            coord_max = origin + n1 * ax1 + n2 * ax2 + n3 * ax3
            dims = [n1, n2, n3]
            if return_molecule:
                return val, dims, coord_min, coord_max, atoms, coordinates
            else:
                return val, dims, coord_min, coord_max

    @classmethod
    def interpolate_field(cls, data, npoints, bounds, npointsi, boundsi):
        """Compute an interpolated field f(xi,zi,yi) from input field f(x,y,z).

        Note
        ----
        Requires regular grids.

        Parameters
        ----------
        data : array
            input field values
        npoints : list
            number of points in x,y,z direction
        bounds : list
            bounds of the input grid (xmin, ymin, zmin, xmax, ymax, zmax)
        npointsi : list
            number of points in xi,yi,zi direction
        boundsi : list
            bounds of the output grid (ximin, yimin, zimin, ximax, yimax, zimax)

        """
        from scipy.interpolate import interpn

        data = np.array(data).ravel()
        bounds = np.array(bounds).ravel()
        boundsi = np.array(boundsi).ravel()

        x = np.linspace(bounds[0], bounds[3], npoints[0])
        y = np.linspace(bounds[1], bounds[4], npoints[1])
        z = np.linspace(bounds[2], bounds[5], npoints[2])

        xi = np.linspace(boundsi[0], boundsi[3], npointsi[0])
        yi = np.linspace(boundsi[1], boundsi[4], npointsi[1])
        zi = np.linspace(boundsi[2], boundsi[5], npointsi[2])

        mesh = np.array(np.meshgrid(xi, yi, zi))
        # points= np.rollaxis(mesh, 0, 4)
        points = np.moveaxis(mesh, 0, 3)
        points = points.reshape((mesh.size // 3, 3))

        data_new = interpn((x, y, z),
                           data.reshape(npoints),
                           points,
                           method='linear',
                           bounds_error=False,
                           fill_value=0.0)
        data_out = np.zeros(npointsi)
        ii = 0
        # note the order
        for j in range(0, npointsi[1]):
            for i in range(0, npointsi[0]):
                for k in range(0, npointsi[2]):
                    data_out[i][j][k] = data_new[ii]
                    ii += 1

        del points, data_new
        return data_out.ravel()


class VisualizeJavaScript:
    """JavaScript visualization class."""

    @classmethod
    def gen_index_html(cls):
        """Generate file index.html."""
        index_html: str = ''

        index_html += """<!DOCTYPE html>
<!--
Note:
-----
    * currently we put EVERY module we might need here
    * this makes it easier to separate CDN-based from local versions
    compared to imports
    * might need to be changed in the future
-->
<html>

<head>
    <title> Hylleraas Software Platform </title>
    <script src="https://unpkg.com/three@0.141.0/build/three.js"></script>
    <script src="https://unpkg.com/three@0.141.0/examples/js/loaders/PDBLoader.js"></script>
    <script src="https://unpkg.com/three@0.141.0/examples/js/objects/MarchingCubes.js"></script>
    <script src="https://unpkg.com/three@0.141.0/examples/js/controls/TrackballControls.js"></script>
    <script src="https://unpkg.com/three@0.141.0/examples/js/controls/OrbitControls.js"></script>
    <script src="https://unpkg.com/three@0.141.0/examples/js/utils/BufferGeometryUtils.js"></script>
    <script src="https://unpkg.com/dat.gui@0.7.9/build/dat.gui.js"></script>
<!--<script src = "https://unpkg.com/html2canvas@1.4.1/dist/html2canvas.js"></script>
<script src = "https://unpkg.com/2cs-canvas2image@0.0.2/canvas2image.js" > </script> -->
    <style>
        body {
            /* set margin to 0 and overflow to hidden, to go fullscreen */
            margin: 0;
            overflow: hidden;
        }
    </style>
</head>
<body>

<script type="module" src="main.js">
</script>
</body>
</html>"""
        with open('index.html', 'w') as text_file:
            text_file.write(index_html)

    @classmethod
    def gen_javascript_libs(cls):
        """Generate javascript files."""
        loader_js = """export const loadFile = (filename, delimiter) => {
    return new Promise((resolve) => {
        const loader = new THREE.FileLoader();
        loader.load(filename, (data) => {
            var array = data.trim().split(delimiter).map(Number);
            resolve(array);
        });
    });
};"""

        molecule_js = """export function create_molecule( material, filename){

    let loader = new THREE.PDBLoader();
    let atoms = new THREE.Object3D();
    let bonds = new THREE.Object3D();
    const offset = new THREE.Vector3();


    loader.load(filename, function( pdb ) {

    const geometryAtoms = pdb.geometryAtoms;
    const geometryBonds = pdb.geometryBonds;
    const json = pdb.json;

    // computing bounding box -> necessary for marchingcubes
    geometryAtoms.computeBoundingBox();
    geometryAtoms.boundingBox.getCenter( offset ).negate();

    // move molecule to center of bounding box
    geometryAtoms.translate(offset.x, offset.y, offset.z);
    geometryBonds.translate(offset.x, offset.y, offset.z);

    const positionAtoms = geometryAtoms.getAttribute('position');
    const colorAtoms = geometryAtoms.getAttribute('color');

    const position = new THREE.Vector3();
    const color = new THREE.Color();

    // to increase performance - reuse objects
    // var sphere = new THREE.SphereGeometry(.35);
    // var material_atom = material.clone();
    // material_atom.color.set(color);
    // var mesh = new THREE.Mesh(sphere, material);


    var imesh = new THREE.InstancedMesh(sphere, material, 10);
    //console.log(imesh);

    // var mesh = new THREE.Mesh(sphere, material_atom);
    // create atoms
    for ( let i = 0; i < positionAtoms.count; i ++ ) {

                position.fromBufferAttribute( positionAtoms, i );
                color.fromBufferAttribute( colorAtoms, i );
                const atomJSON = json.atoms[ i ];
                const element = atomJSON[ 4 ];
                var sphere = new THREE.SphereGeometry(0.35);
                var material = new THREE.MeshPhongMaterial({color:color});
                var mesh = new THREE.Mesh(sphere, material);
                mesh.position.copy(position);
                atoms.add(mesh);
                // maybe add the atom here already to the scene (?)
        };

    const positionBonds = geometryBonds.getAttribute('position');
    const start = new THREE.Vector3();
    const end = new THREE.Vector3();

    // create bonds
    for  ( let i = 0 ; i < positionBonds.count ; i+=2 ){

        start.fromBufferAttribute( positionBonds, i );
        end.fromBufferAttribute( positionBonds, i + 1 );
        // TODO - take the path out of the loop and rather set its properties
        // inside
        //
        var path = new THREE.LineCurve3(start, end);
        var tube = new THREE.TubeGeometry(path, 1, 0.08);
        var material_bond = material.clone();
        material_bond.color.set(0xcccccc);
        var mesh = new THREE.Mesh(tube, material_bond);
        bonds.add(mesh);
        };

    }

    );



    // console.log('moleculebox', bbox, geometryAtoms);

    atoms.name='atoms';
    bonds.name='bonds';

    return { atoms, bonds} ;


}"""

        orbital_js = """import { loadFile } from './loader.js';

export async function create_orbital(material, options){

    let orbital = new THREE.MarchingCubes( options.orbital.resolution, material, false, false );
    orbital.isolation = options.isovalue;
    orbital.orbarray = await loadFile(options.orbital_filename, ',');

    options.gui.isovalue_min = Math.min(...orbital.orbarray);
    options.gui.isovalue_max = Math.max(...orbital.orbarray);
    options.gui.isovalue_step = (options.gui.isovalue_max - options.gui.isovalue_min)/options.gui.isovalue_steps;
    options.gui.isovalue_start = options.gui.isovalue_min+ (options.gui.isovalue_max - options.gui.isovalue_min)/2.0;
    orbital.field = orbital.orbarray;

    // update_orbital(orbital,options.orbital);

    return orbital;
}

export function update_orbital(object,options){

    // object.reset();

    var res = object.size;

    const a_x = options.bbox_min[0]
    const a_y = options.bbox_min[1]
    const a_z = options.bbox_min[2]

    const b_x = options.bbox_max[0]
    const b_y = options.bbox_max[1]
    const b_z = options.bbox_max[2]

    // for analytical orbitals...
    //
    // for ( var k = 0 ; k < res ; k++ ) {
    //     for ( var j = 0 ; j < res ; j++ ) {
    //         for ( var i = 0 ; i < res ; i++ ) {

    //     // x value in bbox from grid point
    //     var x = (b_x-a_x)*(i-res/2)/res + (a_x+b_x)/2.0;
    //     var y = (b_y-a_y)*(j-res/2)/res + (a_y+b_y)/2.0;
    //     var z = (b_z-a_z)*(k-res/2)/res + (a_z+b_z)/2.0;

    //     // object.field[ i + j*res + k*res*res ] = quadValue(quadricData,x,y,z);
    //     // object.field[ i + j*res + k*res*res ] = object.orbarray[i + j*res + k*res*res]
    //     //object.field[ i + j*res + k*res*res ] = 10.0*x*Math.exp(-1.0/1.0*(x*x+y*y+z*z));
    //         }
    //     }
    // }

    // normal bounding box is -1,1 , scaling...
    object.scale.set(1.0*(b_x-a_x)/2.0, 1.0*(b_y-a_y)/2.0, 1.0*(b_z-a_z)/2.0);


    var c = new THREE.Vector3(options.origin[0], options.origin[1], options.origin[2]);
    object.geometry.translate( -c.x, -c.y, -c.z );
    object.position.set( c.x, c.y, c.z );
    // object.geometry.computeVertexNormals();

    object.update();


    // console.log(object.geometry);
    // object.geometry.computeVertexNormals();
    // THREE.BufferGeometryUtils.mergeVertices(object.geometry);
    // let geometry = object.geometry.clone();
    // geometry.deleteAttribute('normal');
    // geometry.deleteAttribute('uv');
    // object.geometry = THREE.BufferGeometryUtils.mergeVertices(geometry);
    // object.geometry.computeVertexNormals();
    // object.geometry = newgeo;

    // object.geometry.computeVertexNormals();

}"""

        with open('loader.js', 'w') as text_file:
            text_file.write(loader_js)
        with open('molecule.js', 'w') as text_file:
            text_file.write(molecule_js)
        with open('orbital.js', 'w') as text_file:
            text_file.write(orbital_js)

    @classmethod
    def gen_javascript_main(cls,
                            molecule_str: str,
                            orbital: Optional[str] = None,
                            resolution: Optional[int] = None):
        """Generate main javascript file."""
        main_js = ''

        control_js = """let options = {
    showMolecule:true,
    showOrbital:true,
    bgcolor:'#000000',
    control_damping_factor : 1.0,
    camera_position : [10,10,10],
    camera_scaling : 1.0,
    camera_origin: [0,0,0],
    gui : {
        showAtoms: true, showBonds : false, screenshot:true,
        showVRbutton : true,
        bgcolor: '#000000', isovalue_min : -1 , isovalue_max : 1 , isovalue_start : 0.0,   isovalue_steps : 10.0,
    },
    orbital:{
    filename : null,
    resolution : res_value,
    origin : [0,0,0],
    bbox_min : [-4., -4.944863, -5.889726],
    bbox_max : [4.20512,  4.203277, 5.149278],
    },
    molecule_filename : './examples/H2O_4.pdb',
    orbital_filename : './examples/cubified15.txt',
};
"""
        control_js = control_js.replace('./examples/H2O_4.pdb', molecule_str)
        if orbital is not None:
            control_js = control_js.replace(
                './examples/cubified30.txt', 'hylleraascub.txt')
        control_js = control_js.replace('res_value', str(resolution))

        main_js += control_js
        main_js += """
var material_molecule = new THREE.MeshPhongMaterial({
    color:0x0000ff,
    flatShading:false,
    shininess:50,
});

var material_alpha = new THREE.MeshPhongMaterial({
    color:0x2b38ff,
    transparent: true,
    opacity: 0.7,
    side: THREE.DoubleSide,
    flatShading:false,
});

var material_beta = new THREE.MeshPhongMaterial({
    color:0x17d9ff,
    transparent: true,
    opacity: 0.7,
    side: THREE.DoubleSide,
    flatShading:false,
});



import { create_molecule } from './molecule.js';
import { create_orbital, update_orbital } from './orbital.js';
import { loadFile } from './loader.js';
import { VRButton } from 'https://unpkg.com/three@0.141.0/examples/jsm/webxr/VRButton.js';



async function init (){

    // setup scene
    var scene = new THREE.Scene();
    scene.background = new THREE.Color(options.bgcolor);

    // setup renderer
    var renderer = new THREE.WebGLRenderer({
        alpha: false,
        antialias: false,
        powerPreference: 'high-performance',
    });
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);

    // setup camera
    var camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 1000);

    // setup controls
    var controls = new THREE.OrbitControls(camera, renderer.domElement);
    controls.addEventListener("change", () => renderer.render(scene, camera));
    // controls.enableDamping = true;
    // controls.damping  = true;
    // controls.dampingFactor = options.control_damping_factor;

    // setup lights
    var light1 = new THREE.DirectionalLight( 0xffffff );
    light1.position.set( 0., 0., 100 );
    scene.add( light1 );

    var light2 = new THREE.DirectionalLight( 0xffffff );
    light2.position.set( 0.,100,0 );
    scene.add( light2 );

    var light3 = new THREE.DirectionalLight(.8);
    light3.position.set( 100,0,0 );
    scene.add( light3 );

    var ambientLight1 = new THREE.AmbientLight( 0x080808 );
    scene.add( ambientLight1 );

    // setup molecule


    var molecule = create_molecule(material_molecule, options.molecule_filename);

    scene.add(molecule.atoms);
    scene.add(molecule.bonds);
"""
        if orbital is not None:
            main_js += """
    // setup orbitals

    var orbital_alpha = await create_orbital(material_alpha, options);
    scene.add(orbital_alpha);

    var orbital_beta = await create_orbital(material_beta, options);
    scene.add(orbital_beta);
"""
        main_js += """

    // add axis
    // const axesHelper = new THREE.AxesHelper( 100 );
    // scene.add( axesHelper );


    // setup gui/controler
    var gui = new dat.GUI();
    setup_gui(gui, options);

    update_camera(options.camera_position, options.camera_scaling, options.camera_origin);
    window.addEventListener( 'resize', onWindowResize, false );

    render();
    //animate();


    function setup_gui(object, options){

        console.log('gui options', options.gui)

        if ('bgcolor' in options.gui){
            object.addColor(options.gui,'bgcolor').name('background').onChange(
                val => {scene.background.set(val)});
        }

        // var folder = object.addFolder('molecule')
        if (options.gui.showAtoms){
            var checkbox = object.add(options.gui,'showAtoms').name('show atoms').listen();
            checkbox.onChange( function (newValue) {
                if (newValue) {scene.add(molecule.atoms)}
                else { removeEntity(molecule.atoms)}
            });
        }

        if (options.gui.showVRbutton){
            var checkbox = object.add(options.gui,'showVRbutton').name('enable VR').listen();
            checkbox.onChange( function (newValue) {
            document.body.appendChild( VRButton.createButton( renderer ) );
            renderer.xr.enables= true;
   });
        }



"""
        if orbital is not None:
            main_js += """

        var slider_alpha = object.add(options.gui,'isovalue_start',
            options.gui.isovalue_min,options.gui.isovalue_max,options.gui.isovalue_step).name('isovalue');
        slider_alpha.onChange(function(newValue){
            orbital_alpha.isolation = newValue;
            update_orbital(orbital_alpha, options.orbital);
            render();
        });

        var slider_beta = object.add(options.gui,'isovalue_start',
            options.gui.isovalue_min,options.gui.isovalue_max,options.gui.isovalue_step).name('isovalue');
        slider_beta.onChange(function(newValue){
            orbital_beta.isolation = newValue;
            update_orbital(orbital_beta, options.orbital);
            render();
        });
"""

        main_js += """


//         if (options.gui.screenshot){
//             var checkbox = object.add(options.gui,'screenshot').name('screenshot').listen();
//             checkbox.onChange( function (newValue) {
//                 if (newValue) {

// html2canvas(document.body).then(function(canvas) {
//     // document.body.appendChild(canvas);
//     return Canvas2Image.saveAsPNG(canvas);
// });

//                 }
//                 else {}
//             });
//         }



    }

    function onWindowResize() {
        update_camera(options.camera_position, options.camera_scaling, options.camera_origin);
        renderer.setSize( window.innerWidth, window.innerHeight );
    }


    function update_camera(position,factor, origin){
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        camera.position.x = position[0]*factor
        camera.position.y = position[1]*factor
        camera.position.z = position[2]*factor
        camera.lookAt(new THREE.Vector3(origin[0], origin[1], origin[2]));
        // for reading the zoom-level
        let length = camera.position.length();
        console.log('camera length = ', length)

    }

    function removeEntity(object) {
            var selectedObject = scene.getObjectByName(object.name);
            scene.remove( selectedObject );
            render();
    }


    function render () {

        renderer.clear();
        controls.update();
        requestAnimationFrame(render);
        renderer.render(scene,camera);
        // console.log("Number of Triangles :", renderer.info.render.triangles);
    }


    // function animate(){
    //     requestAnimationFrame(animate);
    //     render();
    // }

}


init();

"""
        with open('main.js', 'w') as text_file:
            text_file.write(main_js)

    @classmethod
    def gen_startserver(cls):
        """Generate startserver input."""
        python_str = """import http.server

HandlerClass = http.server.SimpleHTTPRequestHandler
HandlerClass.extensions_map['.js'] = 'text/javascript'
HandlerClass.extensions_map['.mjs'] = 'text/javascript'

import os
import time

hostName = "localhost"
serverPort = 8080

class MyServer(HandlerClass):


    def do_GET(self):
        if self.path == '/':
            self.path = '/index.html'
        try:
            split_path = os.path.splitext(self.path)
            request_extension = split_path[1]
            if request_extension != ".py":
                f = open(self.path[1:]).read()
                self.send_response(200)
                self.end_headers()
                self.wfile.write(bytes(f, 'utf-8'))
            else:
                f = "File not found"
                self.send_error(404,f)
        except:
            f = "File not found"
            self.send_error(404,f)

if __name__ == "__main__":
    webServer = http.server.test(HandlerClass, port=serverPort)
    print("Server started http://%s:%s" % (hostName, serverPort))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")
"""

        with open('startserver.py', 'w') as text_file:
            text_file.write(python_str)


if __name__ == '__main__':

    import hylleraas as hsp
    mymol = hsp.Molecule('H 0 0 0 \n O 1 1 0')
    view_molecule_browser(mymol)
