from datetime import timedelta
from functools import singledispatch
from typing import Union

from .slurm import slurmtime_to_timedelta


@singledispatch
def get_timedelta(time: Union[timedelta, str, int, float, dict, bool]
                  ) -> timedelta:
    """Convert time to timedelta."""
    if time is None:
        return timedelta(seconds=0)
    raise TypeError(f'Cannot convert {type(time)} to timedelta.')


@get_timedelta.register(timedelta)
def _(time: timedelta) -> timedelta:
    """Return the time."""
    return time


@get_timedelta.register(int)
@get_timedelta.register(str)
@get_timedelta.register(float)
def _(time: Union[str, int, float]) -> timedelta:
    """Return the time."""
    try:
        time_ = timedelta(seconds=float(time))
    except ValueError as e:
        try:
            time_ = slurmtime_to_timedelta(str(time))
        except ValueError:
            raise ValueError(f'Invalid time string: {time}') from e
    return time_


@get_timedelta.register(dict)
def _(time: dict) -> timedelta:
    """Return the time."""
    return timedelta(**time)


@get_timedelta.register(bool)
def _(time: bool) -> timedelta:
    """Return the time."""
    return timedelta(seconds=0)
