from datetime import datetime
from functools import singledispatch
from typing import Union


@singledispatch
def get_time(time: Union[datetime, str, int, float, dict, bool]) -> str:
    """Convert time to datetime.isoformat."""
    if time is None:
        return ''
    raise TypeError(f'Cannot convert {type(time)} to time strign.')


@get_time.register(datetime)
def _(time: datetime) -> str:
    """Return the time."""
    return time.isoformat()


@get_time.register(str)
def _(time: str) -> str:
    """Return the time."""
    try:
        time_ = datetime.fromisoformat(time)
    except ValueError as e:
        raise ValueError(f'Invalid time string: {time}') from e
    return time_.isoformat()


@get_time.register(int)
@get_time.register(float)
def _(time: Union[str, int, float]) -> str:
    """Return the time."""
    return datetime.fromtimestamp(float(time)).isoformat()


@get_time.register(dict)
def _(time: dict) -> str:
    """Return the time."""
    return datetime(**time).isoformat()
