# type: ignore
import os
import uuid
from pathlib import Path
from shutil import which
from string import Template
from typing import Any, Dict, List, Optional, Tuple, Union

import numpy as np
from qcelemental import PhysicalConstantsContext

from .convert import convert

# from .molecule import Molecule
# from .utils import in_jupyter

constants = PhysicalConstantsContext('CODATA2018')


def print_summary(summary: dict) -> None:
    """Print summary in a nicer way.

    Parameters
    ----------
    summary : dict
        dictionary to be printed to screen

    """
    import pprint

    pprint.pprint(summary)
    return


def view_molecule_old(*args, **kwargs):
    """Visualize molecule using different programs.

    Wrapper for Visualize.show_molecule()

    Parameters
    ----------
    molecule : :obj:`Any`
        hylleraas Molecule instance
    engine : str, optional
        program used for plotting, defaults to "nglview"
        (for jupyter notebooks, "nglview" is best, in a normal terminal, use "mogli")

    """
    return Visualize.show_molecule(*args, **kwargs)


def plot_molecule(*args, **kwargs):
    """Generate input file for rendering the molecule using blender.

    Wrapper for Visualize.render_molecule()

    Parameters
    ----------
    molecule : :obj:`Any`
        hylleraas Molecule instance
    camera_position : list
        array containing the location of the camera (default value is tuple((0, 0, 15)))
    light_position : list
        array containing the location(s) of the light(s) (default is one light positioned at list((2, -2, 10)))
    save_script : bool
         wether to write the script to file, defaults to True.
    filename: str
        root file name, defaults to "hylleraas_molecule"

    """
    return Visualize.render_molecule(*args, **kwargs)


def view_orbital(
    filename: str,
    filetype: Optional[str] = 'cub',
    molecule: Optional[Any] = None,
    isovalue: Optional[np.array] = None,
):
    """Visualize orbitals using plotly.

    Parameters
    ----------
    filename : str
        name of orbital file
    filetype : str, optional
        type of orbital file (currently, only "cub", "plt" and "xyz" are supported), defaults to "cub".
    molecule : :obj:`Any`, optional
        hylleraas Molecule class. The molecule will be plotted very rudimentary as black balls
    isovalue : float or list, optional
        (1d- or 2d-) array of isovalue(s) to be plotted. Per default, two surfaces with values +- 0.03 are plotted

    """
    try:
        import plotly.graph_objects as go
    except Exception:
        raise Exception('could not find package plotly')

    if not os.path.isabs(filename):
        if molecule is not None:
            os.path.join(molecule.settings.work_dir, filename)
        else:
            os.path.join(os.getcwd(), filename)

    if isovalue is not None:
        if isinstance(isovalue, float):
            isomin = isovalue * (1.0 - 0.001)
            isomax = isovalue * (1.0 + 0.001)
        else:
            if len(isovalue) == 1:
                isomin = isovalue[0] * (1.0 - 0.001)
                isomax = isovalue[0] * (1.0 + 0.001)
            elif len(isovalue) == 2:
                isomin = isovalue[0] * (1.0 - 0.001)
                isomax = isovalue[1] * (1.0 + 0.001)
            else:
                raise Exception(
                    'view_orbital requires max 2 isovalues as a List'
                )
    else:
        isomin = -0.03
        isomax = 0.03

    if filetype == 'cub':
        coord, val = Visualize.read_cube(filename)
    elif filetype == 'xyz':
        coord, val = Visualize.read_scalar_field(filename)
    elif filetype == 'plt':
        coord, val = Visualize.read_plt(filename)
    else:
        raise Exception(f'could not read surface from filetype {filetype}')

    opacity = 1.0
    layout = go.Layout(  # title="f{filename}",
        plot_bgcolor='#ffffff',
        paper_bgcolor='rgb(233,233,233)',  # set the background colour
    )
    fig = go.Figure(layout=layout)
    if molecule is not None:
        fig.add_trace(
            go.Scatter3d(
                x=molecule.coordinates[0],
                y=molecule.coordinates[1],
                z=molecule.coordinates[2],
                marker=dict(color='rgba(0,0,0,1)', size=5),
                mode='markers',
            )
        )
        opacity = 0.1
    fig.add_trace(
        go.Isosurface(
            x=np.array(coord).T[0],
            y=np.array(coord).T[1],
            z=np.array(coord).T[2],
            value=val,
            isomin=isomin,
            isomax=isomax,
            opacity=opacity,
            caps=dict(x_show=False, y_show=False),
        )
    )
    fig.update_xaxes(showgrid=False, zeroline=False)
    fig.update_yaxes(showgrid=False, zeroline=False)
    fig.update_traces(mode='markers', selector=dict(type='scatter3d'))
    return fig.show()


def plot_orbital(
    filename: str,
    filetype: Optional[str] = 'cub',
    molecule: Optional[Any] = None,
    view_mesh: Optional[bool] = False,
    render_settings: Optional[Dict] = None,
) -> None:
    """Generate input file for rendering an orbital.

    Wrapper for Visualize.render_surface()

    Parameters
    ----------
    filename : str
        name of orbital file
    filetype : str, optional
        type of orbital file (currenly, "cub", "plt" and "xyz" are supported), defaults to "cub".
    molecule : :obj:`Any`, optional
        hylleraas Molecule instance if that also should be plotted
    view_mesh : bool, optional
        view the meshes during creation. Useful for debugging and if no blender is available
    render_settings : dict, optional
        Dictionary with the rendering settings, will mostly be parsed directly to the other methods. Contains:
                 isovalue[None] : (1d or 2d-)array of isovalue(s) to be plotted (default +- 0.03).
                     If only one isovalue is given to plot_orbital(),
                     it will anyway plot two surfaces, defined by isovalue*(1+- 0.001)
                 isorange[None] : defines the range of isovalues from  which the isosurface is created.
                     Per default, the intervall is given by isovalue + -0.2 * abs(isovalue) to create a smooth surface
                 color[None] : (rgb)-array of surface color(s). Per default, the colors are interpolated between blue and red
                 camera_position[None] : array containing the location of the camera (default value is tuple((0, 0, 15)))
                 light_position[None] : array containing the location(s) of the light(s) (default is one light positioned at list((2, -2, 10)))
                 opacity[1.0] : opacity of the rendered surface
                 resolution_x[1000] : number of pixel in x direction
                 resolution_y[1000] : number of pixel in y direction
                 run_script[True] : wether or not to run the generated (blender) script. If False, the script gets saved to disk
                 engine['BLENDER_EEVEE'] : engine for redering
                 program['blender']: program used for rendering. Currently, only blender is supported

    """
    number_of_surfaces = 2
    if 'isovalue' in render_settings:
        isovalue = list(render_settings['isovalue'])
        if len(isovalue) == 1:
            isomin = float(isovalue[0]) * (1.0 - 0.001)
            isomax = float(isovalue[0]) * (1.0 + 0.001)
        else:
            if len(isovalue) == 1:
                isomin = isovalue[0] * (1.0 - 0.001)
                isomax = isovalue[0] * (1.0 + 0.001)
            elif len(isovalue) == 2:
                isomin = isovalue[0] * (1.0 - 0.001)
                isomax = isovalue[1] * (1.0 + 0.001)
            else:
                raise Exception(
                    'plot_orbital requires max 2 isovalues as a List'
                )
    else:
        isomin = -0.03
        isomax = 0.03

    if 'isorange' in render_settings:
        isorange = float(render_settings['isorange'])
    else:
        isorange = None

    if 'color' in render_settings:
        color = list(render_settings['color'])
    else:
        color = np.zeros((number_of_surfaces, 3))
        for i in range(0, number_of_surfaces):
            f = float(i) / float(number_of_surfaces - 1)
            color[i][0] = 1.0 - f
            color[i][1] = 0
            color[i][2] = f

    # the following settings are only used by render_surface and thus best set there
    # alternatively, one can give a full dictionary of settings to render_surface, e.g. set default values
    # and just take them as they are.
    #    if "camera_position" in render_settings:
    #        camera_position = tuple(render_settings["camera_position"])
    #    else :
    #        camera_psition = tuple((0,0,15))
    #
    #    if "light_position" in render_settings:
    #        light_position = list(render_settings["light_position"])
    #    else :
    #        light_position = list(camera_position)
    #
    #    if "resolution_x" in render_settings:
    #        resolution_x = int(render_settings["resolution_x"])
    #    else:
    #        resolution_x = 1000
    #
    #    if "resolution_y" in render_settings:
    #        resolution_y = int(render_settings["resolution_y"])
    #    else:
    #        resolution_y = 1000
    #
    #    if "opacity" in render_settings:
    #        opacity = float(render_settings["opacity"])
    #        if opacity > 1.0 or opacity < 0.0 :
    #            raise Exception('opacity must between 0 and 1')
    #
    #    if "run_script" in render_settings:
    #        run_script = render_settings["run_script"]
    #    else:
    #        run_script = True

    if not os.path.isabs(filename):
        if molecule is not None:
            work_dir = molecule.settings.work_dir
            os.path.join(work_dir, filename)
        else:
            work_dir = os.getcwd()
            os.path.join(work_dir, filename)

    plotfile = Path(filename).stem
    render_settings['filename'] = plotfile

    if filetype == 'cub':
        coord, val = Visualize.read_cube(filename)
    elif filetype == 'xyz':
        coord, val = Visualize.read_scalar_field(filename)
    elif filetype == 'plt':
        coord, val = Visualize.read_plt(filename)
    else:
        raise Exception(f'could not read surface from filetype {filetype}')

    filename_isomin = 'hylleraas_surface_isomin.ply'
    filename_isomax = 'hylleraas_surface_isomax.ply'
    os.path.join(work_dir, filename_isomin)
    os.path.join(work_dir, filename_isomax)
    coord_min = Visualize.filter_scalar_field(
        coord, val, isomin, isorange=isorange
    )
    coord_max = Visualize.filter_scalar_field(
        coord, val, isomax, isorange=isorange
    )

    print('generating first mesh...')
    Visualize.generate_mesh(filename_isomin, coord_min, view=view_mesh)
    print('generating second mesh...')
    Visualize.generate_mesh(filename_isomax, coord_max, view=view_mesh)

    Visualize.render_surface(
        [filename_isomin, filename_isomax],
        molecule=molecule,
        render_settings=render_settings,
    )


def view_surface(
    filename: str,
    filetype: Optional[str] = 'cub',
    isovalue: Optional[Union[List, float, np.array]] = None,
    number_of_surfaces: Optional[int] = 2,
):
    """Visualize surface using plotly.

    Parameters
    ----------
    filename : str
        filename
    filetype : str, optional
        type of orbital file (currently, only "cub", "plt" and "xyz" are supported), defaults to "cub".
    isovalue : float or list, optional
        (nd-)array of isovalue(s) to be plotted
    number_of_surfaces : int, optional
        number of surfaces to be plotted. Per default, plotly wants to plot two Isosurfaces
                defined by a minium and a maximal value. If only one isovalue is given to view_surface(),
                it will anyway plot two surfaces, defined by isovalue*(1+- 0.001) in order to trick plotly.
                defaults to 2.

    """
    if not os.path.isabs(filename):
        os.path.join(os.getcwd(), filename)

    try:
        import plotly.graph_objects as go
    except Exception:
        raise Exception('could not find package plotly')

    if isovalue is None:
        raise Exception('please provide list of isosurface value(s)')
    else:
        if isinstance(isovalue, float):
            isomin = isovalue * (1.0 - 0.001)
            isomax = isovalue * (1.0 + 0.001)
        else:
            if len(isovalue) == 1:
                isomin = isovalue[0] * (1.0 - 0.001)
                isomax = isovalue[0] * (1.0 + 0.001)
            elif len(isovalue) > 1:
                isomin = np.min(np.array(isovalue))
                isomax = np.max(np.array(isovalue))
            else:
                raise Exception(
                    'view_orbital requires max 2 isovalues as a List'
                )

    if filetype == 'cub':
        coord, val = Visualize.read_cube(filename)
    elif filetype == 'xyz':
        coord, val = Visualize.read_scalar_field(filename)
    elif filetype == 'plt':
        coord, val = Visualize.read_plt(filename)
    else:
        raise Exception(f'could not read surface from filetype {filetype}')

    fig = go.Figure()
    fig.add_trace(
        go.Isosurface(
            x=np.array(coord).T[0],
            y=np.array(coord).T[1],
            z=np.array(coord).T[2],
            value=val,
            isomin=isomin,
            isomax=isomax,
            opacity=1,
            caps=dict(x_show=False, y_show=False),
            surface_count=number_of_surfaces,
        )
    )
    return fig.show()


def plot_surface(
    filename: str,
    filetype: Optional[str] = 'cub',
    molecule: Optional[Any] = None,
    view_mesh: Optional[bool] = False,
    render_settings: Optional[Dict] = None,
):
    """Generate input file for rendering an orbital.

    Wrapper for Visualize.render_surface()

    Parameters
    ----------
    filename : str
        name of surface file
    filetype : str, optional
        type of orbital file (currenly, "cub", "plt" and "xyz" are supported), defaults to "cub".
    molecule : :obj:`Any`, optional
        hylleraas Molecule instance if that also should be plotted, defaults to None.
    view_mesh : bool, optional
        view the meshes during creation. Useful for debugging and if no blender is available, defaults to False.
    render_settings : dict, optional
        Dictionary with the rendering settings, will mostly be parsed directly to the other methods. Contains:
                 isovalue[None] : (1d or 2d-)array of isovalue(s) to be plotted (default +- 0.03).|
                     If only one isovalue is given to plot_orbital(),
                     it will anyway plot two surfaces, defined by isovalue*(1+- 0.001)|
                 isorange[None] : defines the range of isovalues from  which the isosurface is created.|
                     Per default, the intervall is given by isovalue + -0.2 * abs(isovalue) to create a smooth surface
                 color[None] : (rgb)-array of surface color(s). Per default, the colors are interpolated between blue and red
                 camera_position[None] : array containing the location of the camera (default value is tuple((0, 0, 15)))
                 light_position[None] : array containing the location(s) of the light(s) (default is one light positioned at list((2, -2, 10)))
                 opacity[1.0] : opacity of the rendered surface
                 resolution_x[1000] : number of pixel in x direction
                 resolution_y[1000] : number of pixel in y direction
                 run_script[True] : wether or not to run the generated (blender) script. If False, the script gets saved to disk
                 engine['BLENDER_EEVEE'] : engine for redering
                 program['blender']: program used for rendering. Currently, only blender is supported

    """
    if not os.path.isabs(filename):
        os.path.join(os.getcwd(), filename)
        work_dir = os.getcwd()
    else:
        work_dir = os.path.dirname(filename)

    plotfile = Path(filename).stem

    if 'isovalue' in render_settings['isovalue']:
        isovalue = render_settings['isovalue']
    else:
        isovalue = None

    if isovalue is None:
        raise Exception('please provide a list of isovalue(s)')
    if isinstance(isovalue, float):
        isovalue = np.ndarray(isovalue)

    number_of_isovalues = len(isovalue)

    if 'isorange' in render_settings:
        isorange = float(render_settings['isorange'])
    else:
        isorange = None

    if 'color' in render_settings['color']:
        color = list(render_settings['color'])
        if len(color) != number_of_isovalues:
            raise Exception(
                'number of color does not match number of isosurfaces'
            )

    if filetype == 'cub':
        coord, val = Visualize.read_cube(filename)
    elif filetype == 'xyz':
        coord, val = Visualize.read_scalar_field(filename)
    elif filetype == 'plt':
        coord, val = Visualize.read_plt(filename)
    else:
        raise Exception(f'could not read surface from filetype {filetype}')

    import string

    letters = list(string.ascii_lowercase)
    filename_plys = []
    for i in range(0, number_of_isovalues):
        filename_ply = f'hylleraas_surface_{letters[i]}.ply'
        os.path.join(work_dir, filename_ply)
        filename_plys.append(filename_ply)
        coord_filtered = Visualize.filter_scalar_field(
            coord, val, isovalue[i], isorange=isorange
        )
        print('creating surface mesh...')
        Visualize.generate_mesh(filename_ply, coord_filtered, view=view_mesh)
        del coord_filtered
        del filename_ply

    render_settings['filename'] = plotfile
    Visualize.render_surface(
        filename_plys, molecule=molecule, render_settings=render_settings
    )


class Visualize:
    """Hylleraas Visualization class."""

    @staticmethod
    def in_notebook():
        """Check environment.

        Returns
        -------
        bool
            true if the environment is a jupyter notebook

        """
        try:
            from IPython import get_ipython

            if 'IPKernelApp' not in get_ipython().config:  # pragma: no cover
                return False
        except ImportError:
            return False
        except AttributeError:
            return False
        return True

    @staticmethod
    def in_jupyter() -> bool:
        """Check environment.

        Returns
        -------
        bool
            true if the environment is a jupyter environment

        """
        try:
            from IPython import get_ipython

            ip = get_ipython()
            if ip is None:
                return False
            else:
                # for fixing F821
                _checkhtml: Any = None
                from IPython.core.interactiveshell import InteractiveShell

                format = InteractiveShell.instance().display_formatter.format
                if len(format(_checkhtml, include='text/html')[0]):
                    return True
                else:
                    return False
        except Exception:
            return False

    @staticmethod
    def show_molecule(
        molecule: Any,
        engine: Optional[str] = None,
        render: Optional[bool] = False,
    ) -> None:
        """Visualize molecule using different programs.

        Parameters
        ----------
        molecule : :obj:`Any`
            hylleraas Molecule instance
        engine : str, optional
            program used for plotting, defaults depend on environment
            (for jupyter notebooks, "nglview" is best, in a normal terminal, use "mogli")
        render : bool, optional
            if the molecule should be rendered as well

        """
        filename = str(uuid.uuid4().hex) + '.pdb'

        if Visualize.in_jupyter() or Visualize.in_notebook():
            try:
                import nglview

                # from ipywidgets import Box

            except ImportError:
                raise ImportError(
                    'could not import module nglview, maybe try installing ipywidgets==7.7.2 jupyterlab-widgets==1.1.1 widgetsnbextension==3.6.1'
                )

            pdb_string = convert(molecule, 'xyz', 'pdb', {'bond_thresh': 1.5})

            with open(filename, 'w') as text_file:
                text_file.write(pdb_string)
            # molecule.xyz2pdb(
            #     molecule.atoms,
            #     molecule.coordinates,
            #     filename='visualize.pdb',
            #     unit='bohr',
            # )

            # v = nglview.show_file(
            #    os.path.join(molecule.settings.scratch_dir, "visualize.pdb"))
            # box = Box([v])
            # box.layout.width = "600px"
            # box.layout.height = "600px"
            # box

            if render:
                v = nglview.show_file(
                    os.path.join(molecule.settings.scratch_dir, filename)
                )
                v.render_image()
            # return nglview.write_html("embed.html", box)
            # return nglview.show_file("visualize.pdb")
            # return v
            # fullname = os.path.join(molecule.settings.scratch_dir, "visualize.pdb")
            # print(fullname)
            # unfortunately, nglview seems to not work with absolute paths...

            return nglview.show_file(filename)

        else:
            try:
                import mogli
            except ImportError:
                raise ImportError('could not find module mogli')

            molecule.write_xyz(
                os.path.join(molecule.settings.scratch_dir, filename),
                molecule.atoms,
                molecule.coordinates,
            )
            mol = mogli.read(
                os.path.join(molecule.settings.scratch_dir, filename)
            )
            mogli.show(mol[0], bonds_param=1.25)

    @staticmethod
    def render_molecule(
        molecule: Any,
        render_settings: Optional[Dict] = None,
        engine: Optional[str] = 'blender',
    ):
        """Generate input file for rendering the molecule using blender.

        Parameters
        ----------
        molecule : :obj:`Any`
            hylleraas Molecule instance
        render_settings : dict, optional
            dictionary containing the render settings. Currently supported:
                    camera_position[None] : array containing the location of the camera (default value is tuple((0, 0, 15)))
                    light_position[None] : array containing the location(s) of the light(s) (default is one light positioned at list((2, -2, 10)))
                    resolution_x[1000] : number of pixels in x-direction
                    resolution_y[1000] : number of pixels in y-direction
                    filename["hylleraas_molecule"] : root file name
                    run_script[True] : wether or not {engine} should be called
        engine: str, optional
            program for rendering (currently only blender is supported)

        """
        if 'camera_position' in render_settings:
            camera_position = tuple(render_settings['camera_position'])
        else:
            camera_position = tuple((0, 0, 15))

        if 'light_position' in render_settings:
            light_position = list(render_settings['light_position'])
        else:
            light_position = list(camera_position)

        if 'resolution_x' in render_settings:
            resolution_x = int(render_settings['resolution_x'])
        else:
            resolution_x = 1000

        if 'resolution_y' in render_settings:
            resolution_y = int(render_settings['resolution_y'])
        else:
            resolution_y = 1000

        if 'filename' in render_settings:
            filename = str(render_settings['filename'])
        else:
            filename = 'hylleraas_molecule'

        if 'run_script' in render_settings:
            run_script = render_settings['run_script']
        else:
            run_script = True

        if engine != 'blender':
            raise Exception('Currently,  blender is supported for rendering')

        if not os.path.isabs(filename):
            if molecule is not None:
                os.path.join(molecule.settings.work_dir, filename)
            else:
                os.path.join(os.getcwd(), filename)

        # some of the files maybe need scratch instead of work dir?
        filename_pdb = filename + '.pdb'
        filename_bpy = filename + '.bpy'
        filename_png = filename + '.png'
        molecule.xyz2pdb(molecule.atoms, molecule.coordinates, filename_pdb)

        number_of_lights = len(np.array(light_position).shape)

        template = """\
import bpy
import addon_utils
addon_utils.enable("io_mesh_atomic")
# jupyter notebooks need CYCLES, else please use Eevee
bpy.context.scene.render.engine = 'CYCLES'

bpy.ops.object.delete()
bpy.ops.import_mesh.pdb(filepath="$filename", filter_glob="*.pdb", use_center=True, use_camera=False, use_light=False, ball='0', mesh_azimuth=32, mesh_zenith=32, scale_ballradius=0.66, scale_distances=1, atomradius='0', use_sticks=True, use_sticks_type='0', sticks_subdiv_view=2, sticks_subdiv_render=2, sticks_sectors=100, sticks_radius=0.15, sticks_unit_length=0.05, use_sticks_color=True, use_sticks_smooth=True, use_sticks_bonds=False, sticks_dist=1.1, use_sticks_one_object=True, use_sticks_one_object_nr=200, datafile="")
bpy.types.World.color=(0.0, 0.0, 0.0)
camera_data = bpy.data.cameras.new(name='Camera')
camera_object = bpy.data.objects.new('Camera', camera_data)
bpy.data.objects['Camera'].location = $camera_location
direction =  bpy.data.objects['Camera'].location * -1.0
rot_quat = direction.to_track_quat('-Z', 'Y')
bpy.data.objects['Camera'].rotation_euler = rot_quat.to_euler()
"""
        input_str = Template(template).substitute(
            filename=filename_pdb, camera_location=str(camera_position)
        )

        for i in range(0, number_of_lights):
            if i == 0:
                pos = light_position
            else:
                pos = np.array(light_position)[i]

            light_str = """\
light_data = bpy.data.lights.new(name='Light$nlight', type='SPOT')
light_data.energy = 5000
light_data.shadow_soft_size = 2
light_object$nlight = bpy.data.objects.new(name="Light$nlight", object_data=light_data)
bpy.context.collection.objects.link(light_object$nlight)
bpy.context.view_layer.objects.active = light_object$nlight
light_object$nlight.location = $lightpos
direction = -1* light_object$nlight.location
rot_quat = direction.to_track_quat('-Z', 'Y')
light_object$nlight.rotation_euler = rot_quat.to_euler()
dg = bpy.context.evaluated_depsgraph_get()
dg.update()
"""
            light_str = Template(light_str).substitute(
                nlight=i, lightpos=str(tuple(pos))
            )
            input_str += light_str

        render_str = """\
scene = bpy.context.scene
scene.render.film_transparent = True
scene.render.image_settings.color_mode = 'RGBA'
scene.render.image_settings.file_format='PNG'
scene.render.filepath='$filename'
scene.render.use_border = True
#scene.render.use_crop_to_border = True
scene.render.resolution_x = $res_x
scene.render.resolution_y = $res_y
bpy.ops.render.render(write_still=1)
"""
        input_str += Template(render_str).substitute(
            filename=filename_png, res_x=resolution_x, res_y=resolution_y
        )

        write_file = open(filename_bpy, 'w')
        write_file.write(input_str)
        write_file.close()

        if which('blender') is None or not run_script:
            print(
                f'\nblender-script {filename_bpy} created.',
                'For rendering the molecule, use \n',
                f'  blender --background --python {filename_bpy}\n'
                f'to obtain rendered picture in file {filename_png} ',
            )
        else:
            os.system(f'blender --background --python {filename_bpy} ')

    @staticmethod
    def read_scalar_field(
        filename: str,
    ) -> Tuple[Union[List, np.ndarray], Union[List, np.ndarray]]:
        """Read scalar field from file.

        Parameters
        ----------
        filename : str
            filename

        Returns
        -------
        Tuple(Union[List, np.ndarray],Union[List, np.ndarray])
            cartesian coordinates and isovalues

        """
        if not os.path.isabs(filename):
            os.path.join(os.getcwd(), filename)
        coord = []
        val = []
        with open(filename) as xyz_file:
            lines = list(filter(None, (line.rstrip() for line in xyz_file)))
            for i, line in enumerate(lines):
                if not line.strip().startswith('#'):
                    split_line = line.split()
                    x = float(split_line[0].replace('D', 'E'))
                    x *= constants.bohr2angstroms
                    y = float(split_line[1].replace('D', 'E'))
                    y *= constants.bohr2angstroms
                    z = float(split_line[2].replace('D', 'E'))
                    z *= constants.bohr2angstroms
                    c = float(split_line[3].replace('D', 'E'))
                    coord.append(np.array((x, y, z)))
                    val.append(c)

        return np.array(coord), np.array(val)

    @staticmethod
    def read_plt(
        filename: str,
    ) -> Tuple[Union[List, np.ndarray], Union[List, np.ndarray]]:
        """Read scalar field from .plt-file.

        Parameters
        ----------
        filename : str
            filename

        Returns
        -------
        Tuple(Union[List, np.ndarray],Union[List, np.ndarray])
            cartesian coordinates and isovalues

        """
        if not os.path.isabs(filename):
            os.path.join(os.getcwd(), filename)

        plt_file = open(filename, 'rb')
        ints = np.fromfile(plt_file, dtype=np.dtype('<i4'), count=5)
        if ints[0] != 3:
            raise Exception('no proper plt file!')
        nz = ints[2]
        ny = ints[3]
        nx = ints[4]
        limits = np.fromfile(plt_file, dtype=np.dtype('<f4'), count=6)
        zmin = limits[0]
        zmax = limits[1]
        ymin = limits[2]
        ymax = limits[3]
        xmin = limits[4]
        xmax = limits[5]
        c = np.fromfile(plt_file, dtype=np.dtype('<f4'), count=-1)
        plt_file.close()

        origin = np.array((xmin, ymin, zmin))
        n_tot = nx * ny * nz
        if n_tot != len(c):
            raise Exception('plt file dimension error')
        ax1 = (xmax - xmin) / float(nx - 1)
        ax2 = (ymax - ymin) / float(ny - 1)
        ax3 = (zmax - zmin) / float(nz - 1)
        coord = np.zeros((n_tot, int(3)))
        l1 = 0
        # check order !!
        for i in range(0, nz):
            for j in range(0, ny):
                for k in range(0, nx):
                    xyz = origin
                    coord[l1][0] = float(xyz[0]) + float(k) * ax1
                    coord[l1][1] = float(xyz[1]) + float(j) * ax2
                    coord[l1][2] = float(xyz[2]) + float(i) * ax3
                    l1 += 1

        return np.array(coord, dtype=np.float64), np.array(c, dtype=np.float64)

    @staticmethod
    def read_cube(
        filename: str,
    ) -> Tuple[Union[List, np.ndarray], Union[List, np.ndarray]]:
        """Read scalar field from Gaussian-style (ascii)-cube file.

        Parameters
        ----------
        filename : str
            filename

        Returns
        -------
        Tuple(Union[List, np.ndarray],Union[List, np.ndarray])
            cartesian coordinates and isovalues

        """
        if not os.path.isabs(filename):
            os.path.join(os.getcwd(), filename)
        with open(filename, 'r') as cube_file:
            line = cube_file.readline()
            line = cube_file.readline()
            split_line = cube_file.readline().split()
            num_atoms = int(split_line[0])
            origin = np.array(split_line[1:]).astype(np.float64)
            split_line = cube_file.readline().split()
            n1 = int(split_line[0])
            ax1 = np.array(split_line[1:]).astype(np.float64)
            split_line = cube_file.readline().split()
            n2 = int(split_line[0])
            ax2 = np.array(split_line[1:]).astype(np.float64)
            split_line = cube_file.readline().split()
            n3 = int(split_line[0])
            ax3 = np.array(split_line[1:]).astype(np.float64)
            n_tot = n1 * n2 * n3
            coords = []
            atoms = []
            for i in range(0, num_atoms):
                split_line = cube_file.readline().split()
                atoms.append(split_line[0])
                coords.append(np.array(split_line[1:]).astype(np.float64))
            val = np.zeros(n_tot)
            i = 0
            for line in cube_file:
                for vval in line.split():
                    val[i] = float(vval)
                    i += 1
            coord = np.zeros((n_tot, int(3)))
            l1 = 0
            for i in range(0, n1):
                for j in range(0, n2):
                    for k in range(0, n3):
                        xyz = origin
                        xyz += float(i) * ax1 + float(j) * ax2 + float(k) * ax3
                        coord[l1][0] = xyz[0] * constants.bohr2angstroms
                        coord[l1][1] = xyz[1] * constants.bohr2angstroms
                        coord[l1][2] = xyz[2] * constants.bohr2angstroms
                        l1 += 1
            return coord, val

    @staticmethod
    def filter_scalar_field(
        coord: Union[List, np.ndarray],
        val: Union[List, np.ndarray],
        isovalue: float,
        isorange: Optional[float] = None,
    ) -> np.array:
        """Filter scalar field.

        Parameters
        ----------
        coord : list
            list of cartesian coordinates
        val : list
            list of field values at the coordinates
        isovalue : float
            isovalue
        isorange : float, optional
            range to be looked in, defaults to None

        Returns
        -------
        :obj:`Numpy.ndarray`
            updated coordinates

        """
        if isorange is None:
            isorange = 0.2 * abs(isovalue)

        minval = isovalue - isorange
        maxval = isovalue + isorange
        if len(coord) != len(val):
            print(len(coord), len(val))
            raise Exception(' scalar field dimension discrepancy')
        rm_ind = []
        for i in range(0, len(val)):
            if val[i] < minval or val[i] > maxval:
                rm_ind.append(i)

        return np.delete(coord, rm_ind, axis=0)

    @staticmethod
    def generate_mesh(
        filename: str, coord: np.array, view: Optional[bool] = False
    ) -> None:
        """Generate mesh using open3d and save to disk as ply-file.

        Parameters
        ----------
        filename : str
            filename
        coord : np.array
            numpy array of coordinates
        view : bool, optional
            view the meshes during creation. Useful for debugging and if no blender is available

        Returns
        -------
                mesh stored in {filename}.ply

        Note
        ----
            this routine is experimental and might be modifed for optimal mesh creation.

        """
        try:
            import open3d as o3d
        except Exception:
            raise Exception('could not find pacakge open3d')

        import sys

        if not os.path.isabs(filename):
            os.path.join(os.getcwd(), filename)

        f = open(os.devnull, 'w')
        save_stdout = sys.stdout
        sys.stdout = f
        save_stderr = sys.stderr
        sys.stderr = f

        # generate pointcloud from coords
        pcd = o3d.geometry.PointCloud()
        pcd.points = o3d.utility.Vector3dVector(np.array(coord))
        pcd.estimate_normals()
        # pcd.orient_normals_consistent_tangent_plane(100)
        distances = pcd.compute_nearest_neighbor_distance()
        # remove outliers
        avg_dist = np.mean(distances)
        # pcd1, ind = pcd.remove_statistical_outlier(nb_neighbors=20,
        #                                           std_ratio=2.0)
        # pcd = pcd1.select_by_index(ind)
        pcd1, ind = pcd.remove_radius_outlier(nb_points=4, radius=avg_dist * 3)
        pcd = pcd1.select_by_index(ind)
        # mesh 1 :  ball pivoting
        radii = [
            avg_dist / 10,
            avg_dist / 5,
            avg_dist / 2,
            avg_dist,
            2 * avg_dist,
            3 * avg_dist,
            4 * avg_dist,
            5 * avg_dist,
        ]
        bpa_mesh = (
            o3d.geometry.TriangleMesh.create_from_point_cloud_ball_pivoting(
                pcd, o3d.utility.DoubleVector(radii)
            )
        )
        # mesh 2 : standard alpha shaping
        alpha = 0.5
        alpha_mesh = (
            o3d.geometry.TriangleMesh.create_from_point_cloud_alpha_shape(
                pcd, alpha
            )
        )
        # add both meshes and smoothen
        super_mesh = bpa_mesh + alpha_mesh
        alpha = 0.1
        alpha_mesh = (
            o3d.geometry.TriangleMesh.create_from_point_cloud_alpha_shape(
                pcd, alpha
            )
        )
        super_mesh += alpha_mesh
        super_mesh.filter_smooth_simple(number_of_iterations=2)
        mesh = super_mesh.subdivide_loop(number_of_iterations=2)
        mesh.compute_vertex_normals()
        # re-sample mesh
        super_pcd = mesh.sample_points_uniformly(number_of_points=10000)
        pcd = super_pcd.voxel_down_sample(voxel_size=0.05)

        # mesh 1 :  ball pivoting
        radii = [
            avg_dist / 10,
            avg_dist / 5,
            avg_dist / 2,
            avg_dist,
            2 * avg_dist,
            3 * avg_dist,
            4 * avg_dist,
            5 * avg_dist,
        ]
        bpa_mesh = (
            o3d.geometry.TriangleMesh.create_from_point_cloud_ball_pivoting(
                pcd, o3d.utility.DoubleVector(radii)
            )
        )
        # mesh 2 : standard alpha shaping
        alpha = 0.5
        alpha_mesh = (
            o3d.geometry.TriangleMesh.create_from_point_cloud_alpha_shape(
                pcd, alpha
            )
        )
        # add both meshes and smoothen
        super_mesh = bpa_mesh + alpha_mesh
        alpha = 0.1
        alpha_mesh = (
            o3d.geometry.TriangleMesh.create_from_point_cloud_alpha_shape(
                pcd, alpha
            )
        )
        super_mesh += alpha_mesh
        super_mesh.filter_smooth_simple(number_of_iterations=2)
        mesh = super_mesh.subdivide_loop(number_of_iterations=2)
        mesh.compute_vertex_normals()
        # final mesh by possion shaping
        # poisson_mesh, densities = o3d.geometry.TriangleMesh.create_from_point_cloud_poisson(pcd, depth=9)
        # mesh += poisson_mesh
        # alpha_mesh = o3d.geometry.TriangleMesh.create_from_point_cloud_alpha_shape(pcd, alpha)
        # vertices_to_remove = densities < np.quantile(densities, 0.01)
        # poisson_mesh.remove_vertices_by_mask(vertices_to_remove)
        # simplify
        # mesh = poisson_mesh.simplify_quadric_decimation(target_number_of_triangles=1000)
        # mesh = alpha_mesh.simplify_quadric_decimation(target_number_of_triangles=1000)
        # save mesh to disc
        o3d.io.write_triangle_mesh(filename, mesh)
        sys.stdout = save_stdout
        sys.stderr = save_stderr
        if view:
            o3d.visualization.draw_geometries([mesh, pcd])

    @staticmethod
    def render_surface(
        filenames: Union[List[str], str],
        render_settings: Optional[Dict] = None,
        molecule: Optional[Any] = None,
        program: Optional[str] = 'blender',
    ) -> None:
        """Generate input file for rendering a surface using blender.

        Parameter
        ---------
        filename : str
            name of .ply file(s) containing the surface mesh(es)
        render_settings : dict, optional
            Dictionary with render settings. Currently supported:
                    color[None] : (rgb) array of surface color(s). Per default, the colors are interpolated between blue and red
                    camera_position[None] : array containing the location of the camera (default value is tuple((0, 0, 15)))
                    light_position[None] : array containing the location(s) of the light(s) (default is one light positioned at list((2, -2, 10)))
                    opacity[1.0] : opacity of the rendered surface
                    resolution_x[1000] : number of pixel in x direction
                    resolution_y[1000] : number of pixel in y direction
                    filename["hylleraas_surface"] : root file name
                    run_script[True] : wether or not the engine could be called
                    engine['Eevee']
        molecule : :obj:`Any`, optional
            molecule instance to be plotted as well
        program : str, optional
            engine for rendering (currently, only blender is supported)
        """
        number_of_surfaces = len(filenames)

        if 'color' in render_settings:
            color = render_settings['color']
        else:
            color = np.zeros((number_of_surfaces, 3))
            for i in range(0, number_of_surfaces):
                f = float(i) / float(number_of_surfaces - 1)
                color[i][0] = 1.0 - f
                color[i][1] = 0
                color[i][2] = f

        if 'camera_position' in render_settings:
            camera_position = tuple(render_settings['camera_position'])
        else:
            camera_position = tuple((0, 0, 15))

        if 'light_position' in render_settings:
            light_position = list(render_settings['light_position'])
        else:
            light_position = list(camera_position)

        if 'opacity' in render_settings:
            opacity = float(render_settings['opacity'])
        else:
            opacity = 1.0
        if 'resolution_x' in render_settings:
            resolution_x = int(render_settings['resolution_x'])
        else:
            resolution_x = 1000

        if 'resolution_y' in render_settings:
            resolution_y = int(render_settings['resolution_y'])
        else:
            resolution_y = 1000

        if 'filename' in render_settings:
            filename = str(render_settings['filename'])
        else:
            filename = 'hylleraas_surface'

        if 'run_script' in render_settings:
            run_script = render_settings['run_script']
        else:
            run_script = True

        if 'engine' in render_settings:
            engine = str(render_settings['engine'])
        else:
            engine = 'BLENDER_EEVEE'

        if program != 'blender':
            raise Exception('Currently,  blender is supported for rendering')

        if molecule is not None:
            os.path.join(molecule.settings.work_dir, filename)
        else:
            os.path.join(os.getcwd(), filename)

        filename_png = filename + '.png'
        filename_bpy = filename + '.bpy'
        if molecule is not None:
            filename_pdb = filename + '.pdb'
            molecule.xyz2pdb(
                molecule.atoms, molecule.coordinates, filename_pdb
            )

        if camera_position is None:
            camera_position = tuple((0, 0, 10))
        else:
            camera_position = tuple(camera_position)

        if light_position is None:
            light_position = list(camera_position)

        number_of_lights = len(np.array(light_position).shape)

        template = """\
import bpy
import addon_utils
addon_utils.enable("io_mesh_atomic")
# jupyter notebooks need CYCLES, else please use Eevee
bpy.context.scene.render.engine = '$engine'
#bpy.context.scene.render.engine = 'CYCLES'

bpy.ops.object.delete()
name="MyMaterial"
overwrite=True
if(overwrite is True) and ( name in bpy.data.materials ):
    blenderMat = bpy.data.materials[name]
else:
    blenderMat = bpy.data.materials.new(name)
    name = blenderMat.name
blenderMat.use_nodes=True
nodes = blenderMat.node_tree.nodes
for node in nodes:
    nodes.remove(node)
links = blenderMat.node_tree.links
node_output  = nodes.new(type='ShaderNodeOutputMaterial')
node_output.location = 400,0
node_pbsdf    = nodes.new(type='ShaderNodeBsdfPrincipled')
node_pbsdf.location = 0,0
node_pbsdf.inputs['Base Color'].default_value = (1.0, 1.00, 1.0, 1.0)
node_pbsdf.inputs['Alpha'].default_value = $opacity # 1 is opaque, 0 is invisible
node_pbsdf.inputs['Roughness'].default_value = 0.2
node_pbsdf.inputs['Specular'].default_value = 0.5
node_pbsdf.inputs['Metallic'].default_value = 0.10
node_pbsdf.inputs['Transmission'].default_value = 0.75 # 1 is fully transparent
link = links.new(node_pbsdf.outputs['BSDF'], node_output.inputs['Surface'])
blenderMat.blend_method = 'HASHED'
blenderMat.shadow_method = 'HASHED'
blenderMat.use_screen_refraction = True
"""
        input_str = Template(template).substitute(
            opacity=opacity, engine=engine
        )

        if molecule is not None:
            template = """\
bpy.ops.import_mesh.pdb(filepath="$filename", filter_glob="*.pdb", use_center=True, use_camera=False, use_light=False, ball='0', mesh_azimuth=32, mesh_zenith=32, scale_ballradius=0.66, scale_distances=1, atomradius='0', use_sticks=True, use_sticks_type='0', sticks_subdiv_view=2, sticks_subdiv_render=2, sticks_sectors=100, sticks_radius=0.15, sticks_unit_length=0.05, use_sticks_color=True, use_sticks_smooth=True, use_sticks_bonds=False, sticks_dist=1.1, use_sticks_one_object=True, use_sticks_one_object_nr=200, datafile="")
bpy.ops.object.select_all(action='DESELECT')
"""
            input_str += Template(template).substitute(filename=filename_pdb)

        template = """\
bpy.types.World.color=(0.0, 0.0, 0.0)
camera_data = bpy.data.cameras.new(name='Camera')
camera_object = bpy.data.objects.new('Camera', camera_data)
bpy.data.objects['Camera'].location = $camera_location
direction =  bpy.data.objects['Camera'].location * -1.0
rot_quat = direction.to_track_quat('-Z', 'Y')
bpy.data.objects['Camera'].rotation_euler = rot_quat.to_euler()
"""
        input_str += Template(template).substitute(
            camera_location=str(camera_position)
        )

        for i in range(0, number_of_lights):
            if i == 0:
                pos = light_position
            else:
                pos = np.array(light_position)[i]

            light_str = """\
light_data = bpy.data.lights.new(name='Light$nlight', type='SPOT')
light_data.energy = 5000
light_data.shadow_soft_size = 2
light_object$nlight = bpy.data.objects.new(name="Light$nlight", object_data=light_data)
bpy.context.collection.objects.link(light_object$nlight)
bpy.context.view_layer.objects.active = light_object$nlight
light_object$nlight.location = $lightpos
direction = -1* light_object$nlight.location
rot_quat = direction.to_track_quat('-Z', 'Y')
light_object$nlight.rotation_euler = rot_quat.to_euler()
dg = bpy.context.evaluated_depsgraph_get()
dg.update()
"""
            input_str += Template(light_str).substitute(
                nlight=i, lightpos=str(tuple(pos))
            )

        for i in range(0, number_of_surfaces):
            if number_of_surfaces == 0:
                filename_ply = filenames
            else:
                filename_ply = filenames[i]

            colortuple = tuple((color[i][0], color[i][1], color[i][2], 1.0))
            mat = 'mat' + str(i)
            template = """\
bpy.ops.import_mesh.ply(filepath="$filename")
#bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.object.mode_set(mode='OBJECT')
bpy.ops.object.modifier_add(type='SMOOTH')
bpy.ops.object.modifier_apply(modifier='Smooth')
#bpy.ops.object.modifier_add(type='SUBSURF')
#bpy.ops.object.modifier_apply(modifier='Subsurf')
bpy.ops.object.modifier_add(type='REMESH')
bpy.context.object.modifiers['Remesh'].use_smooth_shade = True
bpy.ops.object.modifier_apply(modifier='Remesh')
$mat = blenderMat.copy()
bpy.context.object.data.materials.append($mat)
bpy.context.active_object.material_slots[0].material.node_tree.nodes['Principled BSDF'].inputs['Base Color'].default_value = $colortuple
bpy.ops.object.select_all(action='DESELECT')
"""
            input_str += Template(template).substitute(
                filename=filename_ply, colortuple=colortuple, mat=mat
            )

        render_str = """\
scene = bpy.context.scene
scene.eevee.use_ssr = True
scene.eevee.use_ssr_refraction = True
scene.render.film_transparent = True
scene.render.image_settings.color_mode = 'RGBA'
scene.render.image_settings.file_format='PNG'
scene.render.filepath='$filename'
scene.render.use_border = True
#scene.render.use_crop_to_border = True
scene.render.resolution_x = $res_x
scene.render.resolution_y = $res_y
bpy.ops.render.render(write_still=1)
"""
        input_str += Template(render_str).substitute(
            filename=filename_png, res_x=resolution_x, res_y=resolution_y
        )
        write_file = open(filename_bpy, 'w')
        write_file.write(input_str)
        write_file.close()

        if which('blender') is None or not run_script:
            print(
                f'\nblender-script {filename_bpy} created.',
                'For rendering the molecule, use \n',
                f'  blender --background --python {filename_bpy}\n'
                f'to obtain rendered picture in file {filename_png} ',
            )
        else:
            os.system(
                f'blender --background --python {filename_bpy} 2>&1>/dev/null'
            )
