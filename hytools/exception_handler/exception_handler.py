import functools
import traceback

from hytools.logger import get_logger


def exception_handler(exception_type=Exception):
    """Wrap func and log exceptions should one occur.

    Parameters
    ----------
    exception_type : Exception or subclass of Exception, optional
        The type of exception to be caught. If not provided, defaults to
        Exception.

    Returns
    -------
    wrapper : function
        The decorated function which includes exception handling.

    """
    def decorator(func):
        """Decorate the function with exception handling."""
        @functools.wraps(func)
        def wrapper(self, *args, **kwargs):
            """Wrap the function with exception handling."""
            logger = kwargs.pop('logger', getattr(self, 'logger', get_logger()))
            try:
                return func(self, *args, **kwargs)
            except exception_type as e:
                logger.error('Error in %s: %s\n%s', func.__name__, e,
                             traceback.format_exc())
                return None
        return wrapper
    return decorator


def exception_handler_static(exception_type=Exception):
    """Wrap func and log exceptions should one occur.

    Parameters
    ----------
    exception_type : Exception or subclass of Exception, optional
        The type of exception to be caught. If not provided, defaults to
        Exception.

    Returns
    -------
    wrapper : function
        The decorated function which includes exception handling.

    """
    def decorator(func):
        """Decorate the function with exception handling."""
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            """Wrap the function with exception handling."""
            logger = kwargs.pop('logger', get_logger())
            try:
                return func(*args, **kwargs)
            except exception_type as e:
                logger.error('Error in %s: %s\n%s', func.__name__, e,
                             traceback.format_exc())
                return None
        return wrapper
    return decorator
