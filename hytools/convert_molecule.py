import os
# import shutil
# import subprocess as sp
import sys
from typing import Any, List, Optional, Tuple, Union

import numpy as np
import xyz2mol as xyz2mol_jensen
from qcelemental import PhysicalConstantsContext
from rdkit import Chem

# from .errors import GenericError
# from .molecule import Molecule

constants = PhysicalConstantsContext('CODATA2018')
list_type = Union[List[float], np.ndarray, np.array]


class ConvertMolecule:
    """Convert molecule attributes."""

    def __init__(self, molecule: Any):
        """Convert molecule attributes.

        Parameters
        ----------
        molecule : HylleraasMoleculeLike
            Hylleraas molecule like object

        """
        self.molecule = molecule

    def daltonproject(self):
        """Generate daltonproject.Molecule instance.

        Returns
        -------
        :obj:`daltonproject.Molecule`
            daltonproject Molecule instance

        """
        try:
            import daltonproject as dp
        except Exception:
            raise ImportError('daltonproject python package not available')
        else:
            dp_atoms = ''
            for i, atom in enumerate(self.molecule.atoms):
                dp_atoms += f' {atom} {self.molecule.coordinates[0]} \
                                    {self.molecule.coordinates[1]} \
                                    {self.molecule.coordinates[2]} ;'

            return dp.Molecule(atoms=dp_atoms)

    def xyz2xyz(self):
        """Generate string in xyz format."""
        atoms = self.atoms
        coordinates = self.coordinates
        mol_string = str(len(atoms)) + '\n\n'
        if len(atoms) == 1 and len(coordinates) == 3:
            coordinates = [coordinates]
        for i, atom in enumerate(atoms):
            coord = np.array(coordinates).ravel().reshape(-1, 3)
            mol_string += atom
            mol_string += ' {:.12f}'.format(coord[i, 0])
            mol_string += ' {:.12f}'.format(coord[i, 1])
            mol_string += ' {:.12f}'.format(coord[i, 2])
            mol_string += '\n'
        return mol_string

    def xyz2mol(self) -> str:
        """Generate mol file string.

        Returns
        -------
        str
            mol file string

        """
        charge = self.molecule.properties.get('charge', 0)
        coordinates = self.molecule.coordinates
        atoms = [xyz2mol_jensen.int_atom(atom.lower())
                 for atom in self.molecule.atoms]

        mol = xyz2mol_jensen.xyz2mol(atoms, coordinates, charge)

        # AC, m =  xyz2mol_jensen.xyz2AC(atoms, coordinates, charge, use_huckel=False)
        # BO, _ =   xyz2mol_jensen.AC2BO(AC, atoms, charge, allow_charged_fragments=True, use_graph=True)
        # print(BO)

        return Chem.MolToMolBlock(mol[0])

    def xyz2smiles(self, options: Optional[dict] = None) -> str:
        """Generate SMILeS-string from atoms and coordinates.

        Parameters
        ----------
        options : dict, optional
            dictionary containing "path to xyz2mol.py file

        Returns
        -------
        str
            SMILES string

        """
        options = {} if options is None else options

        xyz2mol_path = options.get('path_to_xyz2mol', os.getcwd())

        sys.path.append(xyz2mol_path)
        try:
            from xyz2mol import xyz2mol
        except Exception:
            raise ImportError(
                f'could not find script <xyz2mol.py> in given path {options["path_to_executable"]}')

        try:
            from rdkit import Chem
        except Exception:
            raise ImportError('could not find package rdkit')

        # xyz2mol_exec = os.path.join(xyz2mol_path, 'xyz2mol.py')
        xyz2mol_charge = int(options.get('charge', '0'))

        # found_exec = shutil.which(xyz2mol_exec)
        # found_script = os.path.isfile(xyz2mol_exec)
        # if not found_exec and not found_script:
        #     raise Exception('location of script xyz2mol.py not found, see https://github.com/jensengroup/xyz2mol')

        # filename='smiles_dummy.xyz'
        # write_xyz(filename, self.molecule.atoms, self.molecule.coordinates)
        # if found_exec:
        #     smiles = sp.getoutput(f'{xyz2mol_exec} {filename} --charge {xyz2mol_charge}')
        # elif found_script:
        #     smiles = sp.getoutput(f'python3 {xyz2mol_exec} {filename} --charge {xyz2mol_charge}')
        # os.remove(filename)
        mol = xyz2mol(self.molecule.atomic_number,
                      self.molecule.coordinates, charge=xyz2mol_charge)[0]
        smiles = Chem.MolToSmiles(mol)
        return smiles

    def xyz2pdb(self, options: Optional[dict] = None) -> str:
        """Generate pdb-string from atoms and coordinates.

        Parameters
        ----------
        options : dict, optional
            dictionary containing "bond_threshold" (defaults to 1.72) and "unit" (defaults to "bohr")

        Returns
        -------
        str
            pdb string

        """
        options = {} if options is None else options

        single = options.get('bond_thresh', 1.72)
        unit = options.get('unit', 'bohr')

        unit_scale = 1. / constants.bohr2angstroms if 'angstrom' in unit.lower() else 1.0

        pdb = '{:>6}'.format('COMPND')
        pdb += '    '
        pdb += 'hylleraas molecule\n'
        pdb += '{:>6}'.format('AUTHOR')
        pdb += '    '
        pdb += 'generated by hylleraas software platform\n'
        for i, atom in enumerate(self.molecule.atoms):
            pdb += '{:>6}'.format('HETATM')
            pdb += '{:5d}'.format(i + 1)
            pdb += '{:>3}'.format(atom)
            pdb += '{:>7}'.format('UNL')
            pdb += '{:6d}'.format(1)
            pdb += '   '
            pdb += '{:8.3f}{:8.3f}{:8.3f}'.format(self.molecule.coordinates[i][0] * unit_scale,
                                                  self.molecule.coordinates[i][1] *
                                                  unit_scale,
                                                  self.molecule.coordinates[i][2] * unit_scale)
            pdb += '{:6.2f}{:6.2f}'.format(1.0, 0.0)
            pdb += '{:>12}'.format(atom)
            pdb += '\n'
        for i, atom1 in enumerate(self.molecule.atoms):
            pdb += '{:>6}'.format('CONECT')
            pdb += '{:5d}'.format(i + 1)
            for j, atom2 in enumerate(self.molecule.atoms):
                if i == j:
                    continue
                dist = self.molecule.distance(i, j)
                if dist < single:
                    pdb += '{:5d}'.format(j + 1)
                # if cheat_bonds:
                #     if dist < double:
                #         pdb += '{:5d}'.format(j + 1)
                #     if dist < triple:
                #         pdb += '{:5d}'.format(j + 1)
            pdb += '\n'
        pdb += '{:>6}'.format('MASTER')
        pdb += '    '
        for i in range(0, 8):
            pdb += '{:5d}'.format(0)
        pdb += '{:5d}{:5d}{:5d}{:5d}'.format(
            len(self.molecule.atoms), 0, len(self.molecule.atoms), 0)
        pdb += '\n'
        pdb += '{:3}'.format('END')
        pdb += '\n'

        # # if filename is not None:
        # #     if not os.path.isabs(filename):
        # #         os.path.join(self.settings.work_dir, filename)

        # #     write_file = open(filename, 'w')
        # #     write_file.write(pdb)
        # #     write_file.close()
        # #     return True
        # #     # print(f'molecule in pdb-format dumped to file {filename}')
        # # else:
        return pdb

    def xyz2zmat(self):
        """Compute zmat from atoms and coordinates.

        Returns
        -------
        :obj:`Sequence[Union[str, int, float]]`
            zmat

        """
        zmat = []
        for i1, atom in enumerate(self.molecule.atoms):
            line = []
            line.append(self.molecule.atoms[i1])
            if i1 > 0:
                i2 = max(0, i1 - 3)
                # dist = __class__.distance_ext(xyz[i], xyz[j])
                dist = self.molecule.distance(i1, i2)
                line.append(i2 + 1)
                line.append(round(dist, 6))

            if i1 > 1:
                i3 = max(1, i1 - 2)
                # angle = __class__.angle_ext(xyz[i], xyz[j], xyz[k])
                angle = self.molecule.angle(i1, i2, i3)
                line.append(i3 + 1)
                line.append(round(angle, 6))

            if i1 > 2:
                i4 = max(2, i1 - 1)
                # dihedral = __class__.dihedral_ext(xyz[i], xyz[j], xyz[k],xyz[l])
                dihedral = self.molecule.dihedral(i1, i2, i3, i4)
                line.append(i4 + 1)
                line.append(round(dihedral, 6))

            zmat.append(line)
        return zmat

    @classmethod
    def zmat2xyz(cls, zmat) -> Tuple[list, list]:
        """Compute xyz coordinates from zmat.

        Paramters
        ---------
            zmat : :obj:`Sequence[Union[str, int, float]]`
                zmat as generated by Molecule.read_zmat()

        Returns
        -------
        tuple
            atoms, coordinates: extracted from zmat

        """
        num_atoms = len(zmat)
        xyz = np.zeros([num_atoms, 3])
        atoms = []
        for i1 in range(0, num_atoms):
            atoms.append(zmat[i1][0].lower().title())
            xyz[0] = np.zeros(3)
            if i1 == 1:
                dist = float(zmat[i1][2])
                xyz[i1] = [dist, 0.0, 0.0]
            if i1 == 2:
                i2 = int(zmat[i1][1]) - 1
                dist = float(zmat[i1][2])
                i3 = int(zmat[i1][3]) - 1
                angle = float(zmat[i1][4]) / 180.0 * np.pi
                t = xyz[i3][0] - xyz[i2][0]
                x = dist * np.cos(angle)
                y = dist * np.sin(angle)
                x = xyz[i2][0] + np.sign(t) * x
                y = xyz[i2][1] + np.sign(t) * y
                xyz[i1] = [x, y, 0.0]
            if i1 > 2:
                i2 = int(zmat[i1][1]) - 1
                dist = float(zmat[i1][2])
                i3 = int(zmat[i1][3]) - 1
                angle = float(zmat[i1][4]) / 180.0 * np.pi
                i4 = int(zmat[i1][5]) - 1
                dihedral = float(zmat[i1][6]) / 180.0 * np.pi
                tx = dist * np.cos(angle)
                ty = dist * np.cos(dihedral) * np.sin(angle)
                tz = dist * np.sin(dihedral) * np.sin(angle)
                v1 = xyz[i3] - xyz[i4]
                v2 = xyz[i2] - xyz[i3]
                n2 = np.linalg.norm(v2)
                if n2 > 1.0e-09:
                    v2 = v2 / n2
                u1 = np.cross(v1, v2)
                n1 = np.linalg.norm(u1)
                if n1 > 1.0e-09:
                    u1 = u1 / n1
                u2 = np.cross(u1, v2)
                d = np.zeros(3)
                d[0] = -tx * v2[0] + ty * u2[0] + tz * u1[0]
                d[1] = -tx * v2[1] + ty * u2[1] + tz * u1[1]
                d[2] = -tx * v2[2] + ty * u2[2] + tz * u1[2]
                xyz[i1] = xyz[i2] + d
        return atoms, np.around(xyz, decimals=6)


#     def xyz2internal(self, constraints:Optional[dict]=None):

#         atoms = self.molecule.atoms
#         coords = self.molecule.coordinates

#         tmat = ConvertMolecule.get_topology_matrix(atoms, coords)
#         constraints = {} if constraints is None else constraints
#         bonds = ConvertMolecule.get_bonds_from_topology(tmat, atoms, coords, constraints.get('bonds',[]))

#         internals = Convert.Molecule.contract([ bonds[i][3] for i in range(n)])
#         if len(bonds) >=  3*len(atoms)-6:
#             print('done')

#         angles = ConvertMolecule.get_angles_from_topology(tmat, atoms, coords, constraints.get('angles',[]))
#         # torsions = ConvertMolecule.get_torsions_from_topology(tmat, atoms, coords, constraints.get('torsions',[]))
#         # n=len(tmat)
#         # bonds = []
#         # n=len(tmat)
#         # thresh=1
#         # for i in range(n):
#         #     if sum(tmat[i,0:n]) <= 1:
#         #         continue

#         #     for j in range(i+1, n):
#         #         if tmat[i,j] < thresh:
#         #             continue
#         #         dist = np.linalg.norm(coord[i]-coord[j])
#         #         for k in constraints:
#         #             if i in k[0:1] and j in k[0:1]:
#         #                 dist = k[2]
#         #         bonds.append([i,j,dist])

#     @classmethod
#     def contract(cls, q):

#     @classmethod
#     def get_torsions_from_topology(cls,tmat, atoms, coord, constraints)-> list:
#         thresh = 0.9
#         angles=[]
#         bonds=[]
#         torsions=[]
#         n=len(tmat)
#         for i in range(n):
#             for j in range(i+1, n):
#                 if tmat[i,j] < thresh:
#                     continue
#                 bonds.append([i,j])

#         for bond1 in bonds:
#             for bond2 in bonds:
#                 found = [ 1  for x in bond1 if x in bond2]
#                 if found == []:
#                     continue
#                 angle = bond1.copy()
#                 for iatom in bond2:
#                     if iatom not in bond1:
#                         angle.append(iatom)
#                         angles.append(sorted(angle))

#         angles=sorted(angles)
#         angles = list(angles for angles, _ in itertools.groupby(angles))

#         for angle1 in angles:
#             for angle2 in angles:
#                 torsion = angle1 + list(set(angle2)-set(angle1))
#                 if len(torsion) == 4:
#                     torsions.append(torsion)
#         torsions=sorted(torsions)
#         torsions = list(torsions for torsions, _ in itertools.groupby(torsions))

#         print('TORSIOnS', torsions)
#      #            torsion= ConvertMolecule.merge_sublists(subunits, doubles[i-1], doubles[i])

#      # first = in_list[idx1]
#      #    second = in_list.pop(idx2)
#      #    combined = first + list(set(second)-set(first))
#      #    in_list[idx1]=sorted(combined)

#     @classmethod
#     def get_angles_from_topology(cls,tmat, atoms, coord, constraints)-> list:
#         thresh=1e-6
#         thresh2 = 0.5
#         n = len(tmat)
#         angles=[]
#         for i in range(n):
#             # exclude terminal atoms
#             tmat[i,i]=0
#             connections = [j for j in range(n) if tmat[i,j] > thresh ]
#             n_connections=len(connections)
#             if n_connections <= 1:
#                 continue
#             # check planarity
#             is_planar = False
#             if n_connections >2 :
#                 planarity=0.0
#                 vecs = []
#                 for j in range(n_connections):
#                     vec = coord[connections[j]]-coord[i]
#                     norm = np.linalg.norm(vec)
#                     vecs.append(vec/norm)
#                 for j in range(2,n_connections):
#                     ivec = vecs[j]
#                     jvec = vecs[j-1]
#                     kvec = vecs[j-2]
#                     planarity += np.abs(ivec@(np.cross(jvec,kvec)))

#                 if planarity<0.5:
#                     is_planar = True
#                     print(f'Fragment {i} is planar, {planarity}')

#             # now we have a fragment

#             for j in range(n_connections):
#                 for k in range(j+1, n_connections):
#                     idx1 = connections[j]
#                     idx2 = connections[k]
#                     ifound=False
#                     for constraint in constraints:
#                         if all([i in constraint[0:3], idx1 in constraint[0:3], idx2 in constraint[0:3]]):
#                             ifound=True
#                             theta = constraint[3]
#                     if ifound:
#                         angles.append([*constraint[0:3], theta])
#                         continue
#                     u = coord[idx1]-coord[i]
#                     norm = np.linalg.norm(u)
#                     if norm <thresh:
#                         print('WARNING,u ')
#                         continue
#                     u = u/norm
#                     v = coord[idx2]-coord[i]
#                     norm=np.linalg.norm(v)
#                     if norm <thresh:
#                         print('WARNING,v ')
#                         continue
#                     v=v/norm

#                     theta = np.arccos(u@v)*180.0/np.pi
#                     angles.append([idx1,i,idx2, theta])

#             # print('planaroty of current fragmetn', is_planar)
#             if is_planar:
#                 print('fragmen is planar, adding all other angels')
#                 not_connected = [ j for j in range(n) if j not in connections]
#                 for j in range(n_connections):
#                     for k in range(len(not_connected)):
#                         idx1 = connections[j]
#                         idx2 = not_connected[k]
#                         ifound=False
#                         for constraint in constraints:
#                             if all([i in constraint[0:3], idx1 in constraint[0:3], idx2 in constraint[0:3]]):
#                                 ifound=True
#                                 theta = constraint[3]
#                         if ifound:
#                             angles.append([*constraint[0:3], theta])
#                             continue
#                         u = coord[idx1]-coord[i]
#                         norm = np.linalg.norm(u)
#                         if norm <thresh:
#                             print('WARNING,u ')
#                             continue
#                         u = u/norm
#                         v = coord[idx2]-coord[i]
#                         norm=np.linalg.norm(v)
#                         if norm <thresh:
#                             print('WARNING,v ')
#                             continue
#                         v=v/norm

#                         theta = np.arccos(u@v)*180.0/np.pi
#                         print('ANGLES ADDED', [idx1,i,idx2, theta])
#                         angles.append([idx1,i,idx2, theta])

#         print('angles=', angles)

#             # apex at i
#             # print('number of connectisons for atom', i, n_connections)
#             # print('number of possible angles', n_connections*(n_connections-1)/2)
#             # for j in range(n_connections)
#             # print('connectsions', connections)
# #             n = sum

# #             for j in range(i+1, n):
# #                 if tmat[i,j] < thresh:
# #                     continue
# #                 dist = np.linalg.norm(coord[i]-coord[j])
# #                 for k in constraints:
# #                     if i in k[0:1] and j in k[0:1]:
# #                         dist = k[2]
# #                 bonds.append([i,j,dist])

# #         print('input bonds', bonds)
# # #         thresh = 0.9
# #         angles=[]
# #         bonds=[]
# #         n=len(tmat)
# #         for i in range(n):
# #             for j in range(i+1, n):
# #                 if tmat[i,j] < thresh:
# #                     continue
# #                 bonds.append([i,j])

# #         for bond1 in bonds:
# #             for bond2 in bonds:
# #                 found = [ 1  for x in bond1 if x in bond2]
# #                 if found == []:
# #                     continue
# #                 angle = bond1.copy()
# #                 for iatom in bond2:
# #                     if iatom not in bond1:
# #                         angle.append(iatom)
# #                         angles.append(sorted(angle))

# #         angles=sorted(angles)
# #         angles = list(angles for angles, _ in itertools.groupby(angles))

# #         constraints=[] if constraints is None else constraints
# #         for iangle, angle in enumerate(angles):
# #             for permut in list(itertools.permutations([0,1,2])):
# #                 i = angle[permut[0]]
# #                 j = angle[permut[1]]
# #                 k = angle[permut[2]]

# #                 v1 = coord[i]
# #                 v2 = coord[j]
# #                 v3 = coord[k]
# #                 u = v1-v2
# #                 u_norm = np.linalg.norm(u)
# #                 if u_norm < 1e-4:
# #                     continue
# #                 u = u/u_norm

# #                 v = v3-v2
# #                 v_norm = np.linalg.norm(v)
# #                 if v_norm <1e-4:
# #                     continue
# #                 v = v/v_norm
# #                 theta = np.arccos(u@v)*180.0/np.pi
# #                 angles[iangle] = [i,j,k,theta]
# # # now overwrite with contraints
# #         for iangle, angle in enumerate(angles):
# #             for constraint in constraints:
# #                 i,j,k = constraint[0:3]
# #                 if any([i==j, i==k, j==k]) :
# #                     raise Exception('faulty angle constrinats {angle}')
# #                 if all([i in angle[0:3], j in angle[0:3], k in angle[0:3]]):
# #                     angles[iangle] = constraint

# #         return angles

#     @classmethod
#     def get_bonds_from_topology(cls,tmat, atoms, coord, constraints)-> list:
#         thresh=0.9
#         bonds=[]
#         n = len(tmat)
#         for i in range(n):
#             # exclude terminal atoms
#             if sum(tmat[i,0:n]) <= 1:
#                 continue
#             for j in range(i+1, n):
#                 if tmat[i,j] < thresh:
#                     continue
#                 dist = np.linalg.norm(coord[i]-coord[j])
#                 for k in constraints:
#                     if i in k[0:1] and j in k[0:1]:
#                         dist = k[2]
#                 bonds.append([i,j,dist])

#         return bonds

#     @classmethod
#     def merge_sublists(cls, in_list: list, idx1:int, idx2:int ) -> list:
#         first = in_list[idx1]
#         second = in_list.pop(idx2)
#         combined = first + list(set(second)-set(first))
#         in_list[idx1]=sorted(combined)
#         return in_list

#     @classmethod
#     def get_subunits_topology(cls, tmat) -> list:
#         subunits = []
#         for i in range(len(tmat)):
#             ifound = 0
#             for unit in subunits:
#                 if i in unit:
#                     ifound +=1
#             if ifound == 0:
#                 subunits.append([i])
#             for j in range(i+1,len(tmat)):
#                 if np.abs(tmat[i][j]) > 1e-16:
#                     for unit in subunits:
#                         if i in unit:
#                             if j not in unit:
#                                 unit.append(j)

#         for i in range(len(tmat)):
#             doubles=[]
#             for j, unit in enumerate(subunits):
#                 if i in unit:
#                     doubles.append(j)
#             if len(doubles) <= 1:
#                 continue
#             for i in range(1,len(doubles)):
#                 subunits = ConvertMolecule.merge_sublists(subunits, doubles[i-1], doubles[i])

#         return subunits

#     @classmethod
#     def get_min_subunit_dist(cls, coord, subunits: list) -> list:
#         subunits_min=[]
#         for i in range(len(subunits)):
#             unit1 = subunits[i]
#             n1 = len(unit1)
#             for j in range(i+1, len(subunits)):
#                 unit2 = subunits[j]
#                 n2 = len(unit2)
#                 mindist = np.linalg.norm(coord[unit1[0]]-coord[unit2[0]])
#                 idx=[unit1[0], unit2[0], mindist]
#                 for atom1 in range(n1):
#                     for atom2 in range(n2):
#                         dist = np.linalg.norm(coord[unit1[atom1]]-coord[unit2[atom2]])
#                         if dist < mindist:
#                             idx= [unit1[atom1],unit2[atom2], dist]
#                 subunits_min.append(idx)

#         return subunits_min

#     @classmethod
#     def get_topology_matrix(cls, atoms, coordinates, unit:Optional[str]='bohr') -> np.array:
#         n = len(atoms)
#         coord =np.array(coordinates)
#         fac1 = 1.05  # taken from doi: 10.1063/1.479510 (turbomole)
#         fac2 = 1.2  # taken from doi: 10.1063/1.479510 (turbomole)
#         unit_min = 6.5 # unit bohr, taken from doi: 10.1063/1.479510 (turbomole)
#         tmat = np.zeros((n,n))

#         for i in range(n):
#             atom1 = atoms[i]
#             cov_rad1= covalentradii.get(atom1, units='bohr')
#             for j in range(i+1, n):
#                 atom2 = atoms[j]
#                 cov_rad2 = covalentradii.get(atom2, units='bohr')
#                 dist = np.linalg.norm(coord[i]-coord[j])
#                 if (dist  < fac1*(cov_rad1+cov_rad2)):
#                     tmat[i,j] = 1
#                     tmat[j,i] = 1

#         n_subunits = n
#         min_dist = unit_min
#         i=0
#         for i in range(n):
#             subunits = ConvertMolecule.get_subunits_topology(tmat)
#             if len(subunits) <= 1:
#                 break
#             subunits_min = ConvertMolecule.get_min_subunit_dist(coord, subunits)

#             for unit in subunits_min:
#                 if unit[2] < min_dist:
#                     tmat[unit[0], unit[1]] = 1
#                     tmat[unit[1], unit[0]] = 1

#             i+=1
#             min_dist *= fac2

#         return tmat

#         # at this point we have a fully-connected topology matrix.

#         # # cleaned = [[ tmat[i,j] if i is not in terminal_atoms for i in range(n)] if j is not in
# terminal_atoms for j in range(n)]
#         # cleaned = [[tmat[i,j] for i in range(n) if i not in terminal_atoms  ] for j in range(n)
# if j not in terminal_atoms]
#         # cleaned= np.array(cleaned)
#         # w, v = np.linalg.eigh(cleaned)
#         # cleaned_list= [i for i in range(n) if i not in terminal_atoms]

#         # print('topology matirx', tmat)
#         # print('terminal atoms', terminal_atoms)
#         # print('terminal atoms revmoec', cleaned)
#         # print('cleand list', cleaned_list)
#         # print('number of vectors', len(v), len(cleaned_list))
#         # subunits =[ [i] for i in range(len(tmat))]

#         # if len(subunits)>1 :
#         #     print(f'THERE ARE {len(subunits)} UNCONNECTED SUBUNITS IN THE MOLECULE')

#         # for i in range(len(subunits)):
#         #     unit1 = subunits[i]
#         #     n1 = len(unit1)
#         #     for j in range(i+1, len(subunits)):
#         #         unit2 = subunits[j]
#         #         n2 = len(unit1)
#         #         mindist = np.linalg.norm(coord[unit1[0]]-coord[unit2[0]])
#         #         idx=[unit1[0], unit2[0], mindist]

#         #         for atom1 in range(n1):
#         #             for atom2 in range(n2):
#         #                 dist = np.linalg.norm(coord[unit1[atom1]]-coord[unit2[atom2]])
#         #                 if dist < mindist:
#         #                     idx= [unit1[atom1],unit2[atom2], dist]

#         print('connection to be adedd to the topology mastrix', idx)
# for i in len(idx):
#     if idx[2] < 6.5bohr*(1.2)**x :
#         add to topology matrix

# increase x until there is only one topplogy matrix

# cleaned_units  = set(map(lambda x: tuple(sorted(x)), subunits))

# print('subunits', cleaned_units, type(cleaned_units))
#     # print('eigenvectors', v[i])
#     visited=0
#     for j in range(len(v[i])):
#         if np.abs(v[i][j]) > 1e-16:
#             visited+=1
#     if visited>1:
#         print('vector connected')
#         # cleaned_list.pop(i)
#         # out = cleaned_list[i]
#     else:
#         super_clean.append(i)
# print('filnal cleaned list', super_clean)
# def zmat2xyz(self
#     #zmat: Sequence[Union[str, int, float]]
# ) -> Tuple[List[str], Union[List[float], np.ndarray]]:
#     """Compute xyz coordinates from zmat.

#     Paramters
#     ---------
#         zmat : :obj:`Sequence[Union[str, int, float]]`
#             zmat as generated by Molecule.read_zmat()

#     Returns
#     -------
#     tuple
#         coordinates) extracted from zmat

#     """
#     return zmat2xyz_ext(self.molecule.zmat)

if __name__ == '__main__':

    import hylleraas as hsp
    mymol = hsp.Molecule('''
C -0.061684 .67379 0
C -0.061684 -0.726210 0
F 1.174443 1.331050 0
H -0.927709 1.173790 0
H -0.927709 -1.226210 0
H 0.804342 -1.226210 0''')
    mymol2 = hsp.Molecule('''
O  -17.3102   -0.3814   -0.0567
C   -5.2228   -1.2332    0.2439
C   -6.1762   -0.2801    0.5498
C   -5.9295    1.0541    0.2854
C   -4.7292    1.4353   -0.2844
C   -3.7754    0.4824   -0.5895
C   -4.0226   -0.8520   -0.3264
C   10.7023   -1.6870   -0.1998
C   10.0865   -0.3159   -0.0910
O   10.6984    0.6529   -0.4723
C    8.7070   -0.1537    0.4936
C    8.3218    1.3271    0.4887
H  -17.5629    0.5520   -0.0555
H  -16.4308   -0.5503   -0.4217
H   -5.4137   -2.2751    0.4546
H   -7.1140   -0.5780    0.9949
H   -6.6747    1.7986    0.5237
H   -4.5367    2.4777   -0.4916
H   -2.8378    0.7802   -1.0353
H   -3.2773   -1.5964   -0.5644
H   11.6945   -1.6055   -0.6435
H   10.7836   -2.1286    0.7934
H   10.0741   -2.3186   -0.8280
H    7.9906   -0.7174   -0.1039
H    8.7000   -0.5275    1.5175
H    8.3288    1.7009   -0.5352
H    7.3241    1.4444    0.9115
H    9.0383    1.8908    1.0862''')

    # # test = ConvertMolecule.topology_matrix(mymol2.atoms, mymol2.coordinates)
    # test = ConvertMolecule(mymol2)
    # test.xyz2internal()
    CM = ConvertMolecule(mymol2)
    CM.xyz2mol()
