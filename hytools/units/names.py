from typing import Optional

energy_like = ['energ', 'virial']
force_like = ['grad', 'force']
hessian_like = ['hessian']
length_like = [
    'length',
    'xyz',
    'coord',
    'geo',
    'box',
    'cell',
    'unit_cell',
    'dist',
    'distance',
]
pressure_like = ['pressure', 'pressures']
stress_like = ['stress', 'stresses']


def get_quantity_name(name: Optional[str]) -> Optional[str]:
    """Get name of quantity."""
    if name is None:
        return None
    type_map = {
        'energy': energy_like,
        'force': force_like,
        'hessian': hessian_like,
        'length': length_like,
        'pressure': pressure_like,
        'stress': stress_like,
    }
    for k, v in type_map.items():
        for i in v:
            if name.lower().startswith(i):
                return k
    return None
