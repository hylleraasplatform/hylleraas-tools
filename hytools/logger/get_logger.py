import logging
import logging.config
from pathlib import Path
from typing import Optional

import yaml

from .logger import Logger, LoggerDummy


def get_logger(**kwargs) -> Logger:
    """Create a new logger.

    Parameters
    ----------
    **kwargs : dict
        Additional keyword arguments. Available keyword arguments are:
        config_file: str, optional
            The path to the logging configuration file. Defaults to
            'logger_config.yml'.
        logger_name : str, optional
            The name of the logger. Defaults to __name__.
        print_level : str, optional
            The desired logging level. Defaults to 'ERROR'.
            Available levels are: 'DEBUG', 'INFO', 'WARNING', 'ERROR',
            'CRITICAL'.
        logging_level : str, optional
            Alias for print_level.
        log_file : str, optional
            The path to the log file.

    Returns
    -------
    Logger
        The generated logger.

    Examples
    --------
    >>> logger = get_logger(logging_level='INFO', log_file='app.log')
    >>> logger.info('This is an info message')
    This is an info message
    >>> logger.warning('This is a warning message')
    This is a warning message
    >>> logger.error('This is an error message')
    This is an error message

    """
    if kwargs.get('dummy', False):
        return LoggerDummy()
    logger_config = kwargs.pop('config_file', None)
    logger_config = logger_config or str(Path(__file__).parent /
                                         'logger_config.yml')
    logger_name = kwargs.pop('logger_name', None)
    logger_name = logger_name or __name__
    logging_level = kwargs.pop('print_level',
                               kwargs.pop('logging_level', None))
    logging_level = logging_level or 'ERROR'
    log_file = kwargs.pop('log_file', None)
    return _gen_logger(logger_config, logger_name, logging_level, log_file)


def _gen_logger(config_file: str,
                logger_name: str,
                logging_level: str,
                log_file: str) -> Logger:
    """Generate a logger.

    Parameters
    ----------
    config_file : str
        The path to the logger configuration file.
    logger_name : str
        The name of the logger.
    logging_level : str
        The desired logging level.
    log_file : str
        The path to the log file.

    Returns
    -------
    Logger
        The generated logger.

    Examples
    --------
    >>> logger = _gen_logger(logging_level='INFO', log_file='app.log')

    """
    if not Path(config_file).exists():
        raise FileNotFoundError(f'{config_file} does not exist')

    with open(config_file, 'r') as f:
        try:
            config = yaml.safe_load(f.read())
        except yaml.YAMLError as e:  # coverage: ignore
            raise ValueError(f'Error yaml-reading {config_file}: {e}')
        else:
            logging.config.dictConfig(config)
    logger = logging.getLogger(_check_logger_name(
        logger_name=logger_name, logging_level=logging_level))

    # logging_level = _get_min_logging_level(logger_name) or logging_level

    try:
        logger.setLevel(logging_level.upper())
    except (AttributeError, ValueError):
        raise ValueError(f'Invalid logging level: {logging_level}' +
                         'available levels: DEBUG, INFO, WARNING, ERROR,' +
                         'CRITICAL')

    return _add_handler('file', logger, config,
                        log_file) if log_file else logger


def _check_logger_name(logger_name: Optional[str] = '',
                       logging_level: Optional[str] = 'error') -> str:
    """Check if logger name already exists and return a new name."""
    loggers = getattr(logging.Logger.manager,
                      'loggerDict',
                      {}) if hasattr(logging.Logger, 'manager') else {}
    if logger_name not in loggers:
        return logger_name
    level_map = {'DEBUG': 10, 'INFO': 20, 'WARNING': 30,
                 'ERROR': 40, 'CRITICAL': 50}
    suffix = 1
    while f'{logger_name}_{suffix}' in loggers:
        _level = getattr(loggers[f'{logger_name}_{suffix}'], 'level', 0)
        if _level >= level_map[logging_level.upper()]:
            return f'{logger_name}_{suffix}'
        suffix += 1
    return f'{logger_name}_{suffix}'


# def _get_min_logging_level(logger_name: str) -> str:import logging
#     """Get the minimum logging level for specified loggers."""
#     try:
#         loggers = logging.Logger.manager.loggerDict
#     except AttributeError:
#         return ''

#     levels = ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL']
#     level_map = {level: getattr(logging, level)
#                  for level in levels}
#     reverse_level_map = {v: k for k, v in level_map.items()}

#     hsp_modules = ['hyset', 'hyif', 'hyobj']
#     hsp_loggers = [loggers.get(key) for key in hsp_modules
#                    if loggers.get(key)]
#     hsp_loggers.append(loggers.get(logger_name))
#     logging_levels = [getattr(logger, 'level', 40)
#                       for logger in hsp_loggers if logger]
#     return reverse_level_map.get(min(logging_levels), '')


def _add_handler(key: str,
                 logger: logging.Logger,
                 config: dict,
                 log_file: Optional[str] = None) -> Logger:
    """Add a file handler to logger.

    Parameters
    ----------
    key:
        The key of the handler in the logger configuration.
    logger : logging.Logger
        The logger to add the file handler to.
    config : dict
        The logger configuration.
    log_file : str, optional
        The path to the log file.

    Returns
    -------
    Logger
        The logger with the added file handler.

    """
    handler_config = _get_handler_config(config, key, log_file)
    formatter_key = handler_config.pop('formatter')

    handler = _get_handler(handler_config)
    formatter = _get_formatter(config, formatter_key)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    return _remove_duplicate_handlers(logger)


def _get_handler_config(config: dict,
                        key: str,
                        log_file: Optional[str] = None) -> dict:
    """Retrieve and modify a handler configuration.

    This function retrieves a handler configuration from a 'handlers' section
    of a given config dictionary using a provided key. If a log_file is
    provided, it replaces the 'filename' in the handler configuration.

    Parameters
    ----------
    config : dict
        The configuration dictionary from which to retrieve the handler
        configuration.
    key : str
        The key used to retrieve the handler configuration from the 'handlers'
        section of the config dictionary.
    log_file : str, optional
        If provided, this replaces the 'filename' in the handler configuration.

    Returns
    -------
    dict
        The handler configuration dictionary.

    Raises
    ------
    KeyError
        If the 'handlers' section does not exist in the config dictionary or
        the provided key does not exist in the 'handlers' section.

    Examples
    --------
    >>> config = {'handlers': {'file': {'filename': 'old_log.txt',
                                        'class': 'FileHandler',
                                        'level': 'INFO'}}}
    >>> get_handler_config(config, 'file', 'new_log.txt')
    {'filename': 'new_log.txt', 'class': 'FileHandler', 'level': 'INFO'}

    """
    handlers_config = config.get('handlers')
    if handlers_config is None or key not in handlers_config:
        raise KeyError('No file handler in logger configuration. ' +
                       'Expected keys: handlers: <key>')

    handler_config = handlers_config[key].copy()

    # If a log_file is provided, use it as the filename
    if isinstance(log_file, str):
        handler_config['filename'] = log_file

    return handler_config


def _get_formatter(config: dict, key: str) -> logging.Formatter:
    """Retrieve and configure a formatter.

    This function retrieves a formatter configuration from a 'formatters'
    section of a given config dictionary using a provided key. It then creates
    a logging.Formatter instance with the retrieved configuration.

    Parameters
    ----------
    config : dict
        The configuration dictionary from which to retrieve the formatter
        configuration.
    key : str
        The key used to retrieve the formatter configuration from the
        'formatters' section of the config dictionary.

    Returns
    -------
    logging.Formatter
        The configured logging.Formatter instance.

    """
    formatter_config = config.get('formatters', {}).get(key, {})
    fmt = formatter_config.pop('format', None)
    return logging.Formatter(fmt=fmt, **formatter_config)


def _get_handler(handler_config: dict) -> logging.Handler:
    """Get a handler based on a configuration dictionary.

    Parameters
    ----------
    handler_config: dict
        The handler configuration dictionary.

    """
    # Extract required fields from the handler configuration
    class_path = handler_config.pop('class')
    level = handler_config.pop('level')

    # Determine the parent module for the handler class
    parent_module = (logging.handlers
                     if 'logging.handlers' in class_path else logging)
    handler_class = getattr(parent_module, class_path.split('.')[-1])
    handler = handler_class(**handler_config)
    handler.setLevel(level)
    return handler


def _remove_duplicate_handlers(logger: logging.Logger) -> Logger:
    """Remove duplicate handlers from logger.

    Parameters
    ----------
    logger : logging.Logger
        The logger to remove duplicate handlers from.

    Returns
    -------
    Logger
        The logger with duplicate handlers removed.

    """
    unique_handlers = []
    seen_handlers = set()

    for handler in logger.handlers:
        handler_str = str(handler)
        if handler_str not in seen_handlers:
            seen_handlers.add(handler_str)
            unique_handlers.append(handler)

    logger.handlers = unique_handlers
    return logger
