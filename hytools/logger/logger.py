import logging
import logging.config

Logger = logging.Logger


class LoggerDummy(Logger):
    """Dummy logger class."""

    def __init__(self, *wargs, **kwargs):
        """Initialize."""
        self.name = 'LoggerDummy'

    def __repr__(self):
        """Return repr(self)."""
        return 'LoggerDummy'

    def info(self, *args, **kwargs):
        """Info."""

    def debug(self, *args, **kwargs):
        """Debug."""

    def warning(self, *args, **kwargs):
        """Warning."""

    def error(self, *args, **kwargs):
        """Error."""

    def critical(self, *args, **kwargs):
        """Critical."""
