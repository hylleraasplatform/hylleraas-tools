{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Logging\n",
    "\n",
    "`hytools.logger.get_logger()` offers preconfigurated logger for developers.\n",
    "Currently, `hytools.logger.Logger` is an instance of [logging.logger](https://docs.python.org/3/library/logging.html), the default name of the logger is 'hytools.logger.get_logger' and the logging level is 'ERROR', e.g. only messages at levels ERROR and CRITICAL are displayed in stdout."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Usage\n",
    "\n",
    "Basic usage is as follows:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### basic\n",
    "\n",
    "`get_logger()`is called with a variable number of keyword arguments for maximal flexibility:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from hytools.logger import get_logger, Logger\n",
    "logger: Logger = get_logger(logger_name='my_module',\n",
    "                            print_level='debug',\n",
    "                            unused_kw='mykw')  # unused_kw will be ignored\n",
    "logger.debug('This is a debug message')\n",
    "logger.info('This is an info message')\n",
    "logger.warning('This is a warning message')\n",
    "logger.error('This is an error message')\n",
    "logger.critical('This is a critical message')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### print level\n",
    "The `print_level` dictates which messages are shown.\n",
    "Available print levels  (`str`, with alias `logging_level`) are 'debug', 'info', 'warning', 'error', and 'critical'.\n",
    "A given level will print all messages at the corresponding level or higher, e.g. 'info' will print all messages except 'debug'.\n",
    "Print level 'warning', will print all messages marked with 'warning', 'error' or 'critical':\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from hytools.logger import get_logger\n",
    "my_kwargs = {'logger_name': 'my_module',\n",
    "             'print_level': 'WARNING',\n",
    "             'log_file': './my_log.log'}\n",
    "logger = get_logger(**my_kwargs)\n",
    "logger.debug('This is a debug message')\n",
    "logger.info('This is an info message')\n",
    "logger.warning('This is a warning message')\n",
    "logger.error('This is an error message')\n",
    "logger.critical('This is a critical message')\n",
    "path_to_logfile = str(logger.handlers[0]).split()[1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# load loadfile into notebook\n",
    "%load $path_to_logfile\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### configuration\n",
    "\n",
    "hytools.logger provides a default configuration file (yaml-format). A custom configuration (for options, see [logging.config](https://docs.python.org/3/library/logging.config.html)) file can be used by"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from hytools.logger import get_logger\n",
    "logger = get_logger(config_file='my_config.yml')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Integration into hyif\n",
    "\n",
    "In the [future](https://gitlab.com/hylleraasplatform/hylleraas-interfaces/-/merge_requests/36) `hytools.logger.get_logger()` will be integrated into the most recent version of `hyif.HylleraasInterfaceBasic.initiate_interface()`:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from hyif import HylleraasInterfaceBase\n",
    "\n",
    "class MyInterface(HylleraasInterfaceBase):\n",
    "    \"\"\"My own hyif interface.\"\"\"\n",
    "\n",
    "    def __init__(self, *args, **kwargs):\n",
    "        \"\"\"Initiate.\"\"\"\n",
    "        self.initiate_interface(*args, logger_name='MyInterface', **kwargs)\n",
    "        self.logger.info('Interface inititated: %s', self)\n",
    "        self.logger.warning('This interface is work in progress.')\n",
    "\n",
    "    def author(self):\n",
    "        pass\n",
    "\n",
    "    def parse(self, *args):\n",
    "        pass\n",
    "\n",
    "    def setup(self, *args, **kwargs):\n",
    "        pass\n",
    "\n",
    "    @property\n",
    "    def units_default(self):\n",
    "        pass\n",
    "\n",
    "    def check_version(self):\n",
    "        pass\n",
    "\n",
    "\n",
    "M = MyInterface(print_level='debug')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# API"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Logger\n",
    "\n",
    "```python\n",
    "get_logger(**kwargs)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Parameters used in the method\n",
    "- `logger_name` (str): Name of the logger. Default is 'hytools.logger.logger.logger'.\n",
    "- `print_level` (str): Level of the logger. Default is 'ERROR'.\n",
    "- `logging_level` (str): Alias for logging_level. Default is 'ERROR'.\n",
    "- `config_file` (str): Path to a custom configuration file. Default is None.\n",
    "- `log_file` (str): Path to a custom log file. Default is None.\n",
    "- `return` (Logger): A logging.logger object.\n",
    "- `raises` (ValueError): If logging_level or print_level are not valid levels.\n",
    "- `raises` (FileNotFoundError): If config_file or log_file are not valid paths."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
