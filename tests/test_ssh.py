# import unittest
# from unittest.mock import MagicMock

# from fabric import Connection as FabricConnection

# from hytools.connection import SSHConnection


# class TestSSHConnection(unittest.TestCase):
#     """Test the SSHConnection class."""

#     def setUp(self):
#         """Set up mock Connection objects for testing."""
#         self.mock_fabric_connection = MagicMock(spec=FabricConnection)
#         self.connection_dict = {
#             'host': 'testhost',
#             'user': 'testuser',
#             'port': 22,
#             'connect_kwargs': {'key_filename': 'testkey'}
#         }

#     # @patch('hytools.connection.SSHConnection.connect_ssh')
#     # def test_init_with_dict(self, mock_connect_ssh):
#     #     """Test initialization with a dictionary."""
#     #     mock_connect_ssh.return_value = self.mock_fabric_connection
#     #     ssh_conn = SSHConnection(self.connection_dict)
#     #     mock_connect_ssh.assert_called_once_with(self.connection_dict)
#     #     self.assertIs(ssh_conn.connection, self.mock_fabric_connection)

# #     @patch('hytools.connection.SSHConnection.connect_ssh')
# #     def test_init_with_fabric_connection(self, mock_connect_ssh):
# #         """Test initialization with a Fabric Connection object."""
# #         mock_connect_ssh.return_value = self.mock_fabric_connection
# #         ssh_conn = SSHConnection(self.mock_fabric_connection)
# #         mock_connect_ssh.assert_called_once_with(self.mock_fabric_connection)
# #         self.assertIs(ssh_conn.connection, self.mock_fabric_connection)

#     def test_open(self):
#         """Test the open method."""
#         ssh_conn = SSHConnection(self.mock_fabric_connection)
#         ssh_conn.connection = self.mock_fabric_connection
#         ssh_conn.open()
#         self.mock_fabric_connection.open.assert_called_once()

#     def test_close(self):
#         """Test the close method."""
#         ssh_conn = SSHConnection(self.mock_fabric_connection)
#         ssh_conn.connection = self.mock_fabric_connection
#         ssh_conn.close()
#         self.mock_fabric_connection.close.assert_called_once()

#     def test_execute(self):
#         """Test the execute method."""
#         ssh_conn = SSHConnection(self.mock_fabric_connection)
#         ssh_conn.connection = self.mock_fabric_connection
#         ssh_conn.execute('ls')
#         self.mock_fabric_connection.run.assert_called_once_with('ls')

#     def test_put(self):
#         """Test the put method."""
#         ssh_conn = SSHConnection(self.mock_fabric_connection)
#         ssh_conn.connection = self.mock_fabric_connection
#         ssh_conn.put('local_path', 'remote_path')
#         self.mock_fabric_connection.put.assert_called_once_with('local_path',
#                                                                 'remote_path')

#     def test_get(self):
#         """Test the get method."""
#         ssh_conn = SSHConnection(self.mock_fabric_connection)
#         ssh_conn.connection = self.mock_fabric_connection
#         ssh_conn.get('remote_path', 'local_path')
#         self.mock_fabric_connection.get.assert_called_once_with('remote_path',
#                                                                 'local_path')

#     def test_enter_exit(self):
#         """Test the context management methods."""
#         ssh_conn = SSHConnection(self.mock_fabric_connection)
#         ssh_conn.connection = self.mock_fabric_connection
#         with ssh_conn as conn:
#             self.assertIs(conn, ssh_conn)
#             self.mock_fabric_connection.open.assert_called_once()
#         self.mock_fabric_connection.close.assert_called_once()

# #     @patch('hytools.connection.SSHConnection.connect_ssh')
# #     def test_connect_ssh_with_dict(self, mock_connect_ssh):
# #         """Test the connect_ssh method with a dictionary."""
# #         mock_connect_ssh.return_value = self.mock_fabric_connection
# #         ssh_conn = SSHConnection()
# #         conn = ssh_conn.connect_ssh(self.connection_dict)
# #         self.assertIs(conn, self.mock_fabric_connection)

# #     @patch('hytools.connection.SSHConnection.connect_ssh')
# #     def test_connect_ssh_with_fabric_connection(self, mock_connect_ssh):
# #         """Test the connect_ssh method with a Fabric Connection object."""
# #         mock_connect_ssh.return_value = self.mock_fabric_connection
# #         ssh_conn = SSHConnection()
# #         conn = ssh_conn.connect_ssh(self.mock_fabric_connection)
# #         self.assertIs(conn, self.mock_fabric_connection)

#     # def test_connect_ssh_with_invalid_type(self):
#     #     """Test the connect_ssh method with an invalid type."""
#     #     ssh_conn = SSHConnection()
#     # #     assertIsNone(ssh_conn.connect_ssh(None))
#     #     with self.assertRaises(ValueError):
#     #         ssh_conn.connect_ssh(None)

# #     def test_connect_ssh_with_unsupported_type(self):
# #         """Test the connect_ssh method with an unsupported type."""
# #         ssh_conn = SSHConnection()
# #         with self.assertRaises(NotImplementedError):
# #             ssh_conn.connect_ssh(123)


# if __name__ == '__main__':
#     unittest.main()
