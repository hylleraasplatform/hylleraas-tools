import unittest
from datetime import datetime, timedelta

from hytools.time import get_time, get_timedelta


class TestGetTimedelta(unittest.TestCase):
    """Test the get_timedelta function."""

    def test_get_timedelta_timedelta(self):
        """Test get_timedelta with timedelta input."""
        time = timedelta(days=1, hours=2, minutes=3, seconds=4)
        result = get_timedelta(time)
        self.assertEqual(result, time)

    def test_get_timedelta_int(self):
        """Test get_timedelta with int input."""
        time = 3600  # 1 hour in seconds
        result = get_timedelta(time)
        self.assertEqual(result, timedelta(seconds=time))

    def test_get_timedelta_float(self):
        """Test get_timedelta with float input."""
        time = 3600.5  # 1 hour and 0.5 seconds
        result = get_timedelta(time)
        self.assertEqual(result, timedelta(seconds=time))

    def test_get_timedelta_str(self):
        """Test get_timedelta with str input."""
        time = '3600.5'  # 1 hour and 0.5 seconds
        result = get_timedelta(time)
        self.assertEqual(result, timedelta(seconds=float(time)))

    def test_get_timedelta_dict(self):
        """Test get_timedelta with dict input."""
        time = {'days': 1, 'hours': 2, 'minutes': 3, 'seconds': 4}
        result = get_timedelta(time)
        self.assertEqual(result, timedelta(**time))

    def test_get_timedelta_bool(self):
        """Test get_timedelta with bool input."""
        time = True
        result = get_timedelta(time)
        self.assertEqual(result, timedelta(seconds=0))

    def test_get_timedelta_invalid(self):
        """Test get_timedelta with invalid input."""
        with self.assertRaises(TypeError):
            get_timedelta([1, 2, 3])


class TestGetTime(unittest.TestCase):
    """Test the get_time function."""

    def test_get_time_datetime(self):
        """Test get_time with datetime input."""
        time = datetime(2023, 1, 1, 12, 0, 0)
        result = get_time(time)
        self.assertEqual(result, time.isoformat())

    def test_get_time_str(self):
        """Test get_time with str input."""
        time_str = '2023-01-01T12:00:00'
        result = get_time(time_str)
        self.assertEqual(result, time_str)

    def test_get_time_invalid_str(self):
        """Test get_time with invalid str input."""
        time_str = 'invalid-time-string'
        with self.assertRaises(ValueError):
            get_time(time_str)

    def test_get_time_int(self):
        """Test get_time with int input."""
        time_int = 1672531200  # Corresponds to 2023-01-01T12:00:00
        result = get_time(time_int)
        self.assertEqual(result, datetime.fromtimestamp(time_int).isoformat())

    def test_get_time_float(self):
        """Test get_time with float input."""
        time_float = 1672531200.0  # Corresponds to 2023-01-01T12:00:00
        result = get_time(time_float)
        self.assertEqual(result, datetime.fromtimestamp(time_float).isoformat())

    def test_get_time_dict(self):
        """Test get_time with dict input."""
        time_dict = {'year': 2023, 'month': 1, 'day': 1, 'hour': 12, 'minute': 0, 'second': 0}
        result = get_time(time_dict)
        self.assertEqual(result, datetime(**time_dict).isoformat())

    def test_get_time_none(self):
        """Test get_time with None input."""
        result = get_time(None)
        self.assertEqual(result, '')

    def test_get_time_invalid_type(self):
        """Test get_time with invalid type input."""
        with self.assertRaises(TypeError):
            get_time([1, 2, 3])


if __name__ == '__main__':
    unittest.main()
