import unittest
from unittest.mock import MagicMock

from hytools.connection import SSH2FA
from hytools.connection.ssh_multiplex import SSHMultiplexed


class TestSSH2FA(unittest.TestCase):
    """Test the SSH2FA class."""

    def setUp(self):
        """Set up mock Connection objects for testing."""
        self.connection_kwargs = {
            'host': 'testhost',
            'user': 'testuser',
            'socket_path': '/tmp/test_socket',
            'logger': MagicMock(),
            'reopen': True,
            'connect_kwargs': {'key_filename': 'testkey'}
        }
        self.ssh_conn = SSH2FA(**self.connection_kwargs)

    def test_inheritance(self):
        """Test that SSH2FA inherits from SSHMultiplexed."""
        self.assertIsInstance(self.ssh_conn, SSHMultiplexed)
