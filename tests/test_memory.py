import unittest

from hytools.memory import get_memory


class TestGetMemory(unittest.TestCase):
    """Test the get_memory function."""

    def test_get_memory_int(self):
        """Test get_memory with int input."""
        memory = 1024  # 1 KB
        result = get_memory(memory)
        self.assertEqual(result, '1.00 KiB')

    def test_get_memory_float(self):
        """Test get_memory with float input."""
        memory = 1048576.0  # 1 MB
        result = get_memory(memory)
        self.assertEqual(result, '1.00 MiB')

    def test_get_memory_str(self):
        """Test get_memory with str input."""
        memory = '1.5G'  # 1.5 GB
        result = get_memory(memory)
        self.assertEqual(result, '1.50 GB')

    def test_get_memory_str_with_spaces(self):
        """Test get_memory with str input containing spaces."""
        memory = '1.5 G'  # 1.5 GB
        result = get_memory(memory)
        self.assertEqual(result, '1.50 GB')

    def test_get_memory_str_invalid(self):
        """Test get_memory with invalid str input."""
        memory = 'invalid'
        with self.assertRaises(ValueError):
            get_memory(memory)

    def test_get_memory_none(self):
        """Test get_memory with None input."""
        result = get_memory(None)
        self.assertEqual(result, '0 B')

    def test_get_memory_dict(self):
        """Test get_memory with dict input."""
        memory = {'value': 1024, 'unit': 'KB'}
        with self.assertRaises(TypeError):
            get_memory(memory)


if __name__ == '__main__':
    unittest.main()
