import unittest
from unittest.mock import MagicMock

from hytools.exception_handler import (exception_handler,
                                       exception_handler_static)


class TestExceptionHandler(unittest.TestCase):
    """Test the exception_handler decorator."""

    def test_exception_handler(self):
        """Test the exception_handler decorator."""
        class DummyClass:
            """A dummy class to test the exception_handler decorator."""

            def __init__(self):
                """Initialize the DummyClass."""
                self.logger = MagicMock()

            @exception_handler(ValueError)
            def dummy_method(self):
                """Raise a ValueError exception."""
                raise ValueError('Test exception')

        dummy = DummyClass()
        dummy.dummy_method()

        # Check if logger.error was called with the correct arguments
        dummy.logger.error.assert_called_once()

    def test_exception_handler_static(self):
        """Test the exception_handler_static decorator."""
        logger = MagicMock()

        @exception_handler_static()
        def dummy_function():
            raise ValueError('Test exception')

        dummy_function(logger=logger)

        # Check if logger.error was called with the correct arguments
        logger.error.assert_called_once()


if __name__ == '__main__':
    unittest.main()
