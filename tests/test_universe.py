import unittest

from hytools.units import Units
from hytools.universe import Universe


class TestUniverse(unittest.TestCase):
    """Test the Universe class."""

    def test_initialization_default_units(self):
        """Test initialization with default units."""
        universe = Universe()
        self.assertIsInstance(universe.units, Units)

    def test_initialization_custom_units(self):
        """Test initialization with custom units."""
        custom_units = Units()
        universe = Universe(units=custom_units)
        self.assertEqual(universe.units, custom_units)

    def test_units_property(self):
        """Test the units property."""
        universe = Universe()
        self.assertIsInstance(universe.units, Units)

    def test_units_setter(self):
        """Test the units setter."""
        universe = Universe()
        new_units = Units()
        universe.units = new_units
        self.assertEqual(universe.units, new_units)


if __name__ == '__main__':
    unittest.main()
