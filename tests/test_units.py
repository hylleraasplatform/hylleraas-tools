import numpy as np
import pytest

from hytools.constants import Constants
from hytools.units import Quantity, UnitConverter, Units, convert_units


def test_constants():
    """Test constants."""
    np.testing.assert_almost_equal(Constants.bohr2angstroms, 0.52917721092)
    np.testing.assert_almost_equal(Constants.hartree2kcalmol, 627.5094740631)
    np.testing.assert_almost_equal(Constants.hartree2kJmol, 2625.4996394798254)


def test_quantity():
    """Test quantity."""
    input_dict = {'unit': 'test', 'value': 1.0, 'symbol': 't'}
    q = Quantity(**input_dict)
    assert q.unit == 'test'
    assert q.value == 1.0
    assert q.symbol == 't'
    assert q[0] == 'test'
    assert q[1] == 1.0
    assert q[2] == 't'
    assert q['unit'] == 'test'
    assert q['value'] == 1.0
    assert q['symbol'] == 't'
    assert q[3] is None
    #
    input_dict['value'] = 2.0
    input_tuple = tuple(input_dict.values())
    q1 = Quantity.generate(input_dict)
    q2 = Quantity.from_dict(input_dict)
    assert q1 == q2
    q2 = Quantity.generate(input_tuple)
    assert q1 == q2
    q2 = Quantity.from_tuple(input_tuple)
    assert q1 == q2
    q2 = Quantity.generate(q1)
    assert q1 == q2
    assert q1 != q
    q1.value = 1.0
    assert q1 == q
    q3 = Quantity(unit='test', value=1.0)
    assert q3.symbol == 'test'
    q4 = Quantity(str(q3))
    assert q4 == q3
    with pytest.raises(TypeError):
        q[4.0]
    q4 = Quantity(q3)
    assert q4 == q3
    with pytest.raises(TypeError):
        Quantity.generate(1)
    with pytest.raises(IndexError):
        Quantity.generate(('test',))
    with pytest.raises(KeyError):
        Quantity.generate({'unit': 'test'})


def test_units_init():
    """Test units init."""
    units = Units()
    assert units.energy == Quantity('hartree', 1.0, 'Eh')
    assert units.length == Quantity('bohr', 1.0, 'a0')
    assert units.mass == Quantity('electron_mass', 1.0, 'me')
    assert units.time == Quantity('femtoseconds', 1.0, 'fs')
    assert units.force == Quantity('hartree/bohr', 1.0, 'Eh/a0')
    assert units.hessian == Quantity('hartree/bohr**2', 1.0, 'Eh/a0**2')
    units = Units('real')
    units2 = Units(units)
    assert units.energy == units2.energy


def test_units_init_exceptions():
    """Test units init exceptions."""
    with pytest.raises(TypeError):
        Units(1)
    with pytest.raises(IndexError):
        Units('test', 1)


def test_units_init_custom():
    """Test units init custom."""
    u = Units(
        energy={'unit': 'hartree', 'value': 1.0, 'symbol': 'hartree'},
        dipole=('Debye', 0.3934303, 'D'),
    )
    assert u.energy == Quantity('hartree', 1.0, 'hartree')
    assert u.dipole == Quantity('Debye', 0.3934303, 'D')

    u = Units('atomic', dipole=('Debye', 0.3934303, 'D'))
    assert u.energy == Quantity('hartree', 1.0, 'Eh')
    assert u.dipole == Quantity('Debye', 0.3934303, 'D')
    assert u.length == Quantity('bohr', 1.0, 'a0')

    u = Units('atomic', length=('angstrom', 0.52917721092, 'AA'))
    assert u.energy == Quantity('hartree', 1.0, 'Eh')
    assert u.length == Quantity('angstrom', 0.52917721092, 'AA')

    u = Units(
        {
            'energy': {'unit': 'hartree', 'value': 1.0, 'symbol': 'hartree'},
            'dipole': ('Debye', 0.3934303, 'D'),
        }
    )
    assert u.energy == Quantity('hartree', 1.0, 'hartree')
    assert u.dipole == Quantity('Debye', 0.3934303, 'D')
    u = Units.from_dict(
        {
            'energy': {'unit': 'hartree', 'value': 1.0, 'symbol': 'hartree'},
            'dipole': ('Debye', 0.3934303, 'D'),
        }
    )
    assert u.energy == Quantity('hartree', 1.0, 'hartree')
    assert u.dipole == Quantity('Debye', 0.3934303, 'D')

    u1 = Units('atomic')
    u2 = Units(dipole=('Debye', 0.3934303, 'D'))
    u3 = u1 + u2
    assert u3.energy == Quantity('hartree', 1.0, 'Eh')
    assert u3.dipole == Quantity('Debye', 0.3934303, 'D')


def test_units_replace():
    """Test units replace."""
    u = Units()
    u1 = u.replace(energy={'unit': 'kcal/mol', 'value': 627.509})
    assert u1.energy[0] == 'kcal/mol'


def test_units_update():
    """Test units update."""
    u = Units()
    u.update(energy={'unit': 'kcal/mol', 'value': 627.509})
    assert u.energy[0] == 'kcal/mol'


def test_get_quantity_name():
    """Test get_quantity_name."""
    assert Units.get_quantity_name('energy') == 'energy'
    assert Units.get_quantity_name('energ') == 'energy'
    assert Units.get_quantity_name('virial') == 'energy'
    assert Units.get_quantity_name('force') == 'force'
    assert Units.get_quantity_name('grad') == 'force'
    assert Units.get_quantity_name('hessian') == 'hessian'
    assert Units.get_quantity_name('length') == 'length'
    assert Units.get_quantity_name('xyz') == 'length'
    assert Units.get_quantity_name('coord') == 'length'
    assert Units.get_quantity_name('geo') == 'length'
    assert Units.get_quantity_name('box') == 'length'
    assert Units.get_quantity_name('cell') == 'length'
    assert Units.get_quantity_name('unit_cell') == 'length'
    assert Units.get_quantity_name('test') is None


def test_get_quantity():
    """Test get_quantity."""
    units = Units()
    assert Units.get_quantity('energy', units) == units.energy
    assert Units.get_quantity('force', units) == units.force
    assert Units.get_quantity('hessian', units) == units.hessian
    assert Units.get_quantity('length', units) == units.length
    assert Units.get_quantity('test', units) is None


def test_conversion_dict():
    """Test conversion of dict."""
    units_old = Units('atomic')
    units_new = Units('real')
    name_mapping = {'energy_per_length': 'gradient'}
    en_a = {
        'energy': 1.0,
        'energy_per_length': 2.0,
        'hessian': 3.0,
        'errors': 'string',
    }
    en_b = convert_units(
        en_a,
        units_to=units_new,
        units_from=units_old,
        name_mapping=name_mapping,
        only=['energy', 'energy_per_length'],
        record_success=True,
    )
    assert en_b['energy'] == en_a['energy'] * Constants.hartree2kcalmol
    assert all(x in en_b['converted'] for x in ['energy', 'energy_per_length'])
    fac = Constants.hartree2kcalmol / Constants.bohr2angstroms
    np.testing.assert_almost_equal(
        en_b['energy_per_length'], en_a['energy_per_length'] * fac
    )
    fac = Constants.hartree2kcalmol / Constants.bohr2angstroms
    np.testing.assert_almost_equal(
        en_b['energy_per_length'], en_a['energy_per_length'] * fac
    )
    np.testing.assert_almost_equal(en_b['hessian'], en_a['hessian'])
    assert en_b['errors'] == en_a['errors']


def test_conversion_list():
    """Test conversion of list."""
    units_old = Units('atomic')
    units_new = Units('real')
    en_a = [1.0, 2.0]
    en_b = convert_units(
        en_a,
        units_to=units_new,
        units_from=units_old,
        name='energy',
        overwrite=False,
        conversion_function=lambda x, y, z: y + z,
        debug=False,
    )
    assert en_b[0] == en_a[0] + Constants.hartree2kcalmol


def test_conversion_tuple():
    """Test conversion of tuple."""
    units_old = Units('atomic')
    units_new = Units('real')
    en_a = (1.0, 2.0)
    en_b = convert_units(
        en_a,
        units_to=units_new,
        units_from=units_old,
        name='energy',
        overwrite=False,
        conversion_function=lambda x, y, z: y + z,
        debug=False,
    )
    assert en_b[0] == en_a[0] + Constants.hartree2kcalmol


def test_conversion_np_ndarray():
    """Test convert_units with np.ndarray."""
    obj = np.array([1, 2, 3])
    units_to = {'length': dict(unit='m', value=2.0)}
    units_from = {'length': dict(unit='bohr', value=1.0)}
    obj = convert_units(obj, units_to, units_from=units_from, name='length')
    assert np.allclose(obj, np.array([2, 4, 6]))


def test_conversion_pd_dataframe():
    """Test conversion of pandas dataframe."""
    import pandas as pd

    df = pd.DataFrame({'energy': [1, 2, 3], 'length': [1, 2, 3]})
    df2 = convert_units(df, Units('real'), units_from=Units('atomic'))
    assert df2['energy'].tolist() == [
        1 * Constants.hartree2kcalmol,
        2 * Constants.hartree2kcalmol,
        3 * Constants.hartree2kcalmol,
    ]
    assert df2['length'].tolist() == [
        1 * Constants.bohr2angstroms,
        2 * Constants.bohr2angstroms,
        3 * Constants.bohr2angstroms,
    ]


def test_conversion_stress():
    """Test conversion of stress."""
    units_old = Units('atomic')
    units_new = Units('si')
    stress = np.array([[1]])
    stress = convert_units(
        stress, units_to=units_new, units_from=units_old, name='stress'
    )
    assert np.allclose(stress, np.array([[2.942101569710e13]]), rtol=1e-3)

    # test that pressure and stress are converted the same way
    pressure = np.array([1])
    pressure = convert_units(
        pressure, units_to=units_new, units_from=units_old, name='pressure'
    )
    assert np.allclose(pressure, np.array([2.942101569710e13]), rtol=1e-3)

    # check that metal units are in bar
    units_new = Units('metal')
    stress = np.array([[1]])
    stress = convert_units(
        stress, units_to=units_new, units_from=units_old, name='stress'
    )
    assert np.allclose(stress, np.array([[2.942101569710e13 / 1e5]]),
                       rtol=1e-3)

    # Check the metal-nequip which has negative stress
    units_old = Units('metal-nequip')
    units_new = Units('si')
    stress = np.array([[1]])
    stress = convert_units(
        stress, units_to=units_new, units_from=units_old, name='stress'
    )
    assert np.allclose(
        np.array(stress), np.array([[1.602176634e-19 / 1e-30]]),
        rtol=1e-3
    )


def test_conversion_pd_series():
    """Test conversion of pandas series."""
    import pandas as pd

    s = pd.Series([1, 2, 3])
    s2 = convert_units(
        s, Units('real'), units_from=Units('atomic'), name='energy'
    )
    assert s2.tolist() == [
        1 * Constants.hartree2kcalmol,
        2 * Constants.hartree2kcalmol,
        3 * Constants.hartree2kcalmol,
    ]


def test_conversion_class_instance():
    """Test conversion of a class instance."""

    class Myclass:
        def __init__(self, a, b):
            self.energy = a
            self.length = b

    myclass = Myclass(1, 2)
    myclass.units = Units('atomic')
    myclass = convert_units(myclass, 'metal')
    np.testing.assert_almost_equal(myclass.energy, 27.211386245988)
    np.testing.assert_almost_equal(myclass.length, 1.058354421806)


def test_conversion_class_method():
    """Test conversion of a class method."""
    class Myclass(UnitConverter):
        def __init__(self, a, b):
            self.energy = a
            self.length = b

    myclass1 = Myclass(1, 2)
    myclass1.units = Units('atomic')
    myclass2 = myclass1.convert_units('metal', overwrite=False)
    np.testing.assert_almost_equal(myclass2.energy, 27.211386245988)
    np.testing.assert_almost_equal(myclass2.length, 1.058354421806)
    myclass3 = myclass2.in_units('metal')
    np.testing.assert_almost_equal(myclass2.length, myclass3.length)
    myclass3.change_units('atomic')
    np.testing.assert_almost_equal(myclass1.length, myclass3.length)


def test_unit_to_str():
    """Test unit to str."""
    u_ref = Units('metal')
    # print(u_ref)
    # reference string
    ref_s = '''Units({'mass': Quantity(unit='grams/mol', value=0.9999999996544214, symbol='g/mol'), 'energy': Quantity(unit='eV', value=27.211386245988, symbol='eV'), 'length': Quantity(unit='angstrom', value=0.529177210903, symbol='angstrom'), 'time': Quantity(unit='ps', value=0.01, symbol='ps'), 'stress': Quantity(unit='bar', value=294210156.965221, symbol='bar'), 'pressure': Quantity(unit='bar', value=294210156.965221, symbol='bar'), 'force': Quantity(unit='eV/angstrom', value=51.422067476325886, symbol='eV/angstrom'), 'hessian': Quantity(unit='eV/angstrom**2', value=97.17362429228217, symbol='eV/angstrom**2')})'''  # noqa: E501
    # print(str(ref_s))

    s = str(u_ref)
    # print(s)
    assert s == ref_s
    u = Units.from_str(s)
    assert u == u_ref
    u_ref = Units(
        energy={'unit': 'hamster/wheel', 'value': 1.0, 'symbol': 'h/w'}
    )
    s = str(u_ref)
    assert (
        s
        == '''Units({'energy': Quantity(unit='hamster/wheel', value=1.0, symbol='h/w')})'''  # noqa: E501
    )  # noqa: E501
    u = Units(s)
    assert u.energy.unit == 'hamster/wheel'
