import unittest

from hytools.connection import ConnectionDummy


class TestConnectionDummy(unittest.TestCase):
    """Test the ConnectionDummy class."""

    def setUp(self):
        """Create a new ConnectionDummy instance."""
        self.connection = ConnectionDummy()

    def test_teardown(self):
        """Test the teardown method."""
        self.connection.open()
        d = self.connection.teardown()
        assert d['connection_type'] == 'dummy'

    def test_initialization(self):
        """Test the initialization of the ConnectionDummy instance."""
        self.assertFalse(self.connection.is_open)

    def test_open_connection(self):
        """Test opening the connection."""
        self.connection.open()
        self.assertTrue(self.connection.is_open)

    def test_close_connection(self):
        """Test closing the connection."""
        self.connection.open()
        self.connection.close()
        self.assertFalse(self.connection.is_open)

    def test_execute_statement(self):
        """Test executing a statement."""
        result = self.connection.execute('SELECT 1')
        self.assertIsNone(result)

    def test_rollback(self):
        """Test rolling back the connection."""
        try:
            self.connection.rollback()
        except Exception as e:
            self.fail(f'Rollback raised an exception: {e}')

    def test_is_open_property(self):
        """Test the is_open property."""
        self.assertFalse(self.connection.is_open)
        self.connection.open()
        self.assertTrue(self.connection.is_open)
        self.connection.close()
        self.assertFalse(self.connection.is_open)

    def test_enter_method(self):
        """Test the __enter__ method."""
        with self.connection as conn:
            self.assertIs(conn, self.connection)
            self.assertTrue(self.connection.is_open)

    def test_exit_method(self):
        """Test the __exit__ method."""
        with self.connection:
            pass
        self.assertFalse(self.connection.is_open)


if __name__ == '__main__':
    unittest.main()
