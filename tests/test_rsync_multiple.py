
import sys
import unittest

from hytools.connection.rsync import receive_multiple, send_multiple

host = 'host'
user = 'user'
port = 22


class TestSendMultiple(unittest.TestCase):
    """Test the send_multiple function."""

    def test_sssd(self):
        """Test the single source singe dest function."""
        src = '/home/test0.txt'
        dest = '/cluster/'

        if 'darwin' in sys.platform:
            expected = "rsync -avuzRP -e 'ssh -p 22' /home/./test0.txt 'user@host:/cluster'"
        else:
            expected = "rsync -avuzRP -e 'ssh -p 22' /home/./test0.txt user@host:/cluster"

        cmd = send_multiple(src=src, dest=dest, user=user, host=host, port=port)
        self.assertEqual(cmd, expected)

        if 'darwin' in sys.platform:
            expected = "rsync -avuzRP -e 'ssh -p 22' /home/test0.txt 'user@host:/cluster'"
        else:
            expected = "rsync -avuzRP -e 'ssh -p 22' /home/test0.txt user@host:/cluster"

        cmd = send_multiple(src=src, dest=dest, user=user, host=host, port=port, failback=True)
        self.assertEqual(cmd, expected)

    def test_msmd(self):
        """Test the multiple source single dest function."""
        src = ['/home/test0.txt',
               '/home/work1/scratch/test1.txt']
        dest = ['/cluster/',
                '/cluster/work1/scratch/']

        if 'darwin' in sys.platform:
            expected = "rsync -avuzRP -e 'ssh -p 22' /home/./test0.txt /home/./work1/scratch/test1.txt 'user@host:/cluster'"
        else:
            expected = "rsync -avuzRP -e 'ssh -p 22' /home/./test0.txt /home/./work1/scratch/test1.txt user@host:/cluster"

        cmd = send_multiple(src=src, dest=dest, user=user, host=host, port=port)
        self.assertEqual(cmd, expected)

        if 'darwin' in sys.platform:
            expected = "rsync -avuzRP -e 'ssh -p 22' /home/test0.txt 'user@host:/cluster'\n"
            expected += "rsync -avuzRP -e 'ssh -p 22' /home/work1/scratch/test1.txt 'user@host:/cluster/work1/scratch'"

        else:
            expected = "rsync -avuzRP -e 'ssh -p 22' /home/test0.txt user@host:/cluster\n"
            expected += "rsync -avuzRP -e 'ssh -p 22' /home/work1/scratch/test1.txt user@host:/cluster/work1/scratch"

        cmd = send_multiple(src=src, dest=dest, user=user, host=host,
                            port=port, failback=True)
        self.assertEqual(cmd, expected)

    def test_mssd(self):
        """Test the multple source single dest function."""
        src = ['/home/test0.txt',
               '/home/work1/scratch/test1.txt']
        dest = ['/cluster/',
                '/cluster/']

        if 'darwin' in sys.platform:
            expected = "rsync -avuzRP -e 'ssh -p 22' /home/./test0.txt /home/work1/scratch/./test1.txt 'user@host:/cluster'"
        else:
            expected = "rsync -avuzRP -e 'ssh -p 22' /home/./test0.txt /home/work1/scratch/./test1.txt user@host:/cluster"

        cmd = send_multiple(src=src, dest=dest, user=user, host=host, port=port)
        self.assertEqual(cmd, expected)

        if 'darwin' in sys.platform:
            expected = "rsync -avuzRP -e 'ssh -p 22' /home/test0.txt 'user@host:/cluster'\n"
            expected += "rsync -avuzRP -e 'ssh -p 22' /home/work1/scratch/test1.txt 'user@host:/cluster'"

        else:
            expected = "rsync -avuzRP -e 'ssh -p 22' /home/test0.txt user@host:/cluster\n"
            expected += "rsync -avuzRP -e 'ssh -p 22' /home/work1/scratch/test1.txt user@host:/cluster"

        cmd = send_multiple(src=src, dest=dest, user=user, host=host, port=port, failback=True)
        self.assertEqual(cmd, expected)


class TestReceiveMultiple(unittest.TestCase):
    """Test the receive_multiple function."""

    def test_sssd(self):
        """Test the single source single dest function."""
        src = '/cluster/test0.txt'
        dest = '/home/'

        if 'darwin' in sys.platform:
            expected = "rsync -avuzRP -e 'ssh -p 22' 'user@host:/cluster/./test0.txt' /home"
        else:
            expected = "rsync -avuzRP -e 'ssh -p 22' user@host:/cluster/./test0.txt /home"

        cmd = receive_multiple(src=src, dest=dest, user=user, host=host, port=port)
        self.assertEqual(cmd, expected)

        if 'darwin' in sys.platform:
            expected = "rsync -avuzRP -e 'ssh -p 22' 'user@host:/cluster/test0.txt' /home"
        else:
            expected = "rsync -avuzRP -e 'ssh -p 22' user@host:/cluster/test0.txt /home"

        cmd = receive_multiple(src=src, dest=dest, user=user, host=host, port=port, failback=True)
        self.assertEqual(cmd, expected)

    def test_msmd(self):
        """Test the multiple source single dest function."""
        src = ['/cluster/test0.txt',
               '/cluster/work1/scratch/*']
        dest = ['/home/',
                '/home/work1/scratch/']

        if 'darwin' in sys.platform:
            expected = "rsync -avuzRP -e 'ssh -p 22' 'user@host:/cluster/./test0.txt /cluster/./work1/scratch/*' /home"
        else:
            expected = "rsync -avuzRP -e 'ssh -p 22' user@host:/cluster/./test0.txt /cluster/./work1/scratch/* /home"

        cmd = receive_multiple(src=src, dest=dest, user=user, host=host, port=port)
        self.assertEqual(cmd, expected)

        if 'darwin' in sys.platform:
            expected = "rsync -avuzRP -e 'ssh -p 22' 'user@host:/cluster/test0.txt' /home\n"
            expected += "rsync -avuzRP -e 'ssh -p 22' 'user@host:/cluster/work1/scratch/*' /home/work1/scratch"
        else:
            expected = "rsync -avuzRP -e 'ssh -p 22' user@host:/cluster/test0.txt /home\n"
            expected += "rsync -avuzRP -e 'ssh -p 22' user@host:/cluster/work1/scratch/* /home/work1/scratch"

        cmd = receive_multiple(src=src, dest=dest, user=user, host=host, port=port, failback=True)
        self.assertEqual(cmd, expected)

    def test_mssd(self):
        """Test the multple source single dest function."""
        src = ['/cluster/test0.txt',
               '/cluster/work1/scratch/test1.txt']
        dest = ['/home/',
                '/home/']

        if 'darwin' in sys.platform:
            expected = "rsync -avuzRP -e 'ssh -p 22' 'user@host:/cluster/./test0.txt /cluster/work1/scratch/./test1.txt' /home"
        else:
            expected = "rsync -avuzRP -e 'ssh -p 22' user@host:/cluster/./test0.txt /cluster/work1/scratch/./test1.txt /home"

        cmd = receive_multiple(src=src, dest=dest, user=user, host=host, port=port)
        self.assertEqual(cmd, expected)

        if 'darwin' in sys.platform:
            expected = "rsync -avuzRP -e 'ssh -p 22' 'user@host:/cluster/test0.txt' /home\n"
            expected += "rsync -avuzRP -e 'ssh -p 22' 'user@host:/cluster/work1/scratch/test1.txt' /home"

        else:
            expected = "rsync -avuzRP -e 'ssh -p 22' user@host:/cluster/test0.txt /home\n"
            expected += "rsync -avuzRP -e 'ssh -p 22' user@host:/cluster/work1/scratch/test1.txt /home"

        cmd = receive_multiple(src=src, dest=dest, user=user, host=host, port=port, failback=True)
        self.assertEqual(cmd, expected)


if __name__ == '__main__':
    unittest.main()
