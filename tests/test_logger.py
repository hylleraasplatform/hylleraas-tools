

import logging
import logging.handlers

import pytest

from hytools.logger.get_logger import get_logger


@pytest.fixture(scope='module', params=[
    ('DEBUG', 'debug.log'),
    ('INFO', 'info.log'),
    ('WARNING', 'warning.log'),
    ('ERROR', 'error.log'),
    ('CRITICAL', 'critical.log')
], ids=['debug-level', 'info-level', 'warning-level', 'error-level',
        'critical-level'])
def param_logger(request):
    """Fixture for the logger."""
    logging_level, log_file = request.param
    return get_logger(logging_level=logging_level, log_file=log_file)


# @pytest.mark.parametrize('invalid_level',
#                          ['INVALID', 123],
#                          ids=['string-invalid-level', 'numeric-level'])
# def test_logger_creation_invalid_level(invalid_level):
#     """Test that an invalid logging level raises a ValueError."""
#     with pytest.raises(ValueError):
#         get_logger(logging_level=invalid_level, log_file='test.log')


def test_logger_creation(param_logger):
    """Test that the logger is created."""
    assert isinstance(param_logger, logging.Logger)


def test_logger_invalied_config_file():
    """Test that an invalid logging configuration file."""
    with pytest.raises(FileNotFoundError):
        get_logger(config_file='invalid_config.yml')


def test_dummy_logger():
    """Test dummy logger."""
    logger = get_logger(dummy=True)
    assert isinstance(logger, logging.Logger)
    assert hasattr(logger, 'info')
