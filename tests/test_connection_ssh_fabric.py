import unittest
from unittest.mock import MagicMock, PropertyMock, patch

from fabric import Connection as FabricConnection

from hytools.connection import SSHFabric


class TestSSHFabric(unittest.TestCase):
    """Test the SSHFabric class."""

    def setUp(self):
        """Set up mock Connection objects for testing."""
        self.connection_kwargs = {
            'host': 'testhost',
            'user': 'testuser',
            'port': 22,
            'logger': MagicMock(),
            'connect_kwargs': {'key_filename': 'testkey'}
        }
        self.ssh_conn = SSHFabric(**self.connection_kwargs)

    def test_initialization(self):
        """Test the initialization of SSHFabric."""
        self.assertEqual(self.ssh_conn.host, 'testhost')
        self.assertEqual(self.ssh_conn.user, 'testuser')
        self.assertEqual(self.ssh_conn.port, 22)
        self.assertIsInstance(self.ssh_conn.logger, MagicMock)
        self.assertEqual(self.ssh_conn.connect_kwargs,
                         {'key_filename': 'testkey'})

    @patch('fabric.Connection.open')
    def test_open(self, mock_open):
        """Test the open method."""
        self.ssh_conn.open()
        mock_open.assert_called_once()

    @patch('fabric.Connection.close')
    @patch.object(SSHFabric, 'is_open', new_callable=PropertyMock)
    def test_close(self, mock_is_open, mock_close):
        """Test the close method."""
        mock_is_open.return_value = True
        self.ssh_conn.close()
        mock_close.assert_called_once()

    @patch('fabric.Connection.is_connected')
    @patch('fabric.Connection.run')
    def test_execute(self, mock_run, mock_is_connected):
        """Test the execute method."""
        mock_is_connected.return_value = True
        mock_run.return_value = MagicMock(stdout='output', exited=0)
        result = self.ssh_conn.execute('ls')
        mock_run.assert_called_once_with('ls', hide='stdout')
        self.assertEqual(result.stdout, 'output')
        self.assertEqual(result.returncode, 0)

    @patch('fabric.Connection.put')
    def test_put(self, mock_put):
        """Test the put method."""
        self.ssh_conn.send('local_path', 'remote_path')
        mock_put.assert_called_once_with('local_path', 'remote_path')

    @patch('fabric.Connection.get')
    def test_get(self, mock_get):
        """Test the get method."""
        self.ssh_conn.receive('remote_path', 'local_path')
        mock_get.assert_called_once_with('remote_path', 'local_path')

    @patch.object(FabricConnection, 'is_connected', new_callable=PropertyMock)
    def test_is_open_true(self, mock_is_connected):
        """Test the is_open property when the connection is open."""
        mock_is_connected.return_value = True
        self.assertTrue(self.ssh_conn.is_open)

    @patch.object(FabricConnection, 'is_connected', new_callable=PropertyMock)
    def test_is_open_false(self, mock_is_connected):
        """Test the is_open property when the connection is closed."""
        mock_is_connected.return_value = False
        self.assertFalse(self.ssh_conn.is_open)

    def test_context_manager(self):
        """Test the context manager methods."""
        with patch.object(self.ssh_conn, 'open',
                          return_value=None) as mock_open, \
             patch.object(self.ssh_conn, 'close',
                          return_value=None) as mock_close:
            with self.ssh_conn as conn:
                self.assertIs(conn, self.ssh_conn)
                mock_open.assert_called_once()
            mock_close.assert_called_once()


if __name__ == '__main__':
    unittest.main()
