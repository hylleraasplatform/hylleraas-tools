import unittest
from unittest.mock import MagicMock, patch

from hytools.connection import SSHMultiplexed


class TestSSHMultiplexed(unittest.TestCase):
    """Test the SSHMultiplexed class."""

    def setUp(self):
        """Set up mock Connection objects for testing."""
        self.connection_kwargs = {
            'host': 'testhost',
            'user': 'testuser',
            'timeout': 28800,
            'ssh_socket_path': '/tmp/test_socket',
            'logger': MagicMock()
        }
        self.ssh_conn = SSHMultiplexed(**self.connection_kwargs)

    @patch('subprocess.run')
    @patch('pathlib.Path.exists')
    def test_open(self, mock_exists, mock_run):
        """Test the open method."""
        mock_exists.return_value = True
        mock_run.return_value = MagicMock(stderr=b'')
        self.ssh_conn.open()
        mock_run.called_once_with(['ssh', '-CX', '-fN', '-o',
                                   'ControlMaster=auto', '-o',
                                   f'ControlPath={self.ssh_conn.ssh_socket_path}',
                                   '-o', 'ControlPersist=yes',
                                   '-o', 'ControlPersist=480m',
                                   '-o', 'ServerAliveInterval=30',
                                   f'{self.ssh_conn.user}@{self.ssh_conn.host}'])
        self.assertTrue(self.ssh_conn.is_open)

    @patch('subprocess.run')
    @patch('pathlib.Path.exists')
    def test_teardown(self, mock_exists, mock_run):
        """Test the open method."""
        mock_exists.return_value = True
        mock_run.return_value = MagicMock(stderr=b'')
        self.ssh_conn.open()
        mock_run.called_once_with(['ssh', '-CX', '-fN', '-o',
                                   'ControlMaster=auto', '-o',
                                   f'ControlPath={self.ssh_conn.ssh_socket_path}',
                                   '-o', 'ControlPersist=480m',
                                   '-o', 'ServerAliveInterval=30',
                                   f'{self.ssh_conn.user}@{self.ssh_conn.host}'])
        d = self.ssh_conn.teardown()
        assert d['host'] == 'testhost'
        assert d['user'] == 'testuser'
        assert d['ssh_socket_path'] == '/tmp/test_socket'
        assert d['connection_type'] == 'ssh_multiplexed'

    @patch('subprocess.run')
    @patch('pathlib.Path.exists')
    def test_close(self, mock_exists, mock_run):
        """Test the close method."""
        mock_exists.side_effect = [True, True,
                                   False, False]
        mock_run.side_effect = [MagicMock(stderr=b''),
                                MagicMock(stderr=b'', stdout=b'output')]
        self.ssh_conn.open()
        self.ssh_conn.close()
        mock_run.called_with(['ssh', '-O', 'exit', '-o',
                              f'ControlPath={self.ssh_conn.ssh_socket_path}',
                              f'{self.ssh_conn.user}@{self.ssh_conn.host}'
                              ])
        self.ssh_conn.timeout = False
        self.assertFalse(self.ssh_conn.is_open)

    @patch('subprocess.run')
    @patch('pathlib.Path.exists')
    def test_execute(self, mock_exists, mock_run):
        """Test the execute method."""
        mock_exists.side_effect = [False, True, True]
        mock_run.side_effect = [MagicMock(stderr=''),
                                MagicMock(stderr='', stdout='')]
        self.ssh_conn.open()
        result = self.ssh_conn.execute('ls')
        mock_run.called_with(['ssh',
                              '-o',
                              f'ControlPath={self.ssh_conn.ssh_socket_path}',
                              f'{self.ssh_conn.user}@{self.ssh_conn.host}',
                              'ls'])

        self.assertEqual(result.stdout, '')

    @patch('subprocess.run')
    @patch('pathlib.Path.exists')
    def test_execute_not_is_open(self, mock_exists, mock_run):
        """Test the execute method when connection is not open."""
        mock_exists.return_value = False
        result = self.ssh_conn.execute('ls')
        mock_run.assert_not_called()
        self.assertIsNone(result)

    @patch('subprocess.run')
    @patch('pathlib.Path.exists')
    def test_enter_exit(self, mock_exists, mock_run):
        """Test the context management methods."""
        mock_exists.side_effect = [True, True, True, False, False]
        mock_run.side_effect = [MagicMock(stderr=b'')]
        with self.ssh_conn as conn:
            self.assertIs(conn, self.ssh_conn)
            self.assertTrue(self.ssh_conn.is_open)
        self.assertFalse(self.ssh_conn.is_open)


class TestSSHMultiplexedConvertConnectKwargs(unittest.TestCase):
    """Test the _convert_connect_kwargs method of the SSHMultiplexed class."""

    def test_convert_connect_kwargs_none(self):
        """Test _convert_connect_kwargs with None."""
        ssh_conn = SSHMultiplexed()
        result = ssh_conn._convert_connect_kwargs(None)
        self.assertEqual(result, '')

    def test_convert_connect_kwargs_str(self):
        """Test _convert_connect_kwargs with a string."""
        ssh_conn = SSHMultiplexed()
        result = ssh_conn._convert_connect_kwargs('key=value')
        self.assertEqual(result, 'key=value')

    def test_convert_connect_kwargs_dict(self):
        """Test _convert_connect_kwargs with a dictionary."""
        ssh_conn = SSHMultiplexed()
        result = ssh_conn._convert_connect_kwargs(
            {'key1': 'value1', 'key2': 'value2'})
        self.assertEqual(result, 'key1=value1 key2=value2')

    def test_convert_connect_kwargs_dict_with_none(self):
        """Test _convert_connect_kwargs with a dict containing None values."""
        ssh_conn = SSHMultiplexed()
        result = ssh_conn._convert_connect_kwargs({'key1': 'value1',
                                                   'key2': None})
        self.assertEqual(result, 'key1=value1')


class TestSSHMultiplexedHashEq(unittest.TestCase):
    """Test the __hash__ and __eq__ methods of the SSHMultiplexed class."""

    def setUp(self):
        """Set up mock Connection objects for testing."""
        self.connection_kwargs1 = {
            'host': 'testhost1',
            'user': 'testuser1',
            'ssh_socket_path': '/tmp/test_socket1',
            'logger': MagicMock(),
            'connect_kwargs': {'key_filename': 'testkey1'}
        }
        self.connection_kwargs2 = {
            'host': 'testhost2',
            'user': 'testuser2',
            'ssh_socket_path': '/tmp/test_socket2',
            'logger': MagicMock(),
            'connect_kwargs': {'key_filename': 'testkey2'}
        }
        self.ssh_conn1 = SSHMultiplexed(**self.connection_kwargs1)
        self.ssh_conn2 = SSHMultiplexed(**self.connection_kwargs2)
        self.ssh_conn1_duplicate = SSHMultiplexed(**self.connection_kwargs1)

    def test_hash(self):
        """Test the __hash__ method."""
        self.assertEqual(hash(self.ssh_conn1), hash(self.ssh_conn1_duplicate))
        self.assertNotEqual(hash(self.ssh_conn1), hash(self.ssh_conn2))

    def test_eq(self):
        """Test the __eq__ method."""
        self.assertTrue(self.ssh_conn1 == self.ssh_conn1_duplicate)
        self.assertFalse(self.ssh_conn1 == self.ssh_conn2)


if __name__ == '__main__':
    unittest.main()
