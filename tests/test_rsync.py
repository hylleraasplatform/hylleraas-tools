import unittest
from unittest.mock import MagicMock

from hytools.connection import rsync, rsync_get, rsync_put


class TestRsync(unittest.TestCase):
    """Test the rsync functions."""

    def setUp(self):
        """Set up a mock Connection object for testing."""
        self.mock_connection = MagicMock()
        self.mock_connection.user = 'testuser'
        self.mock_connection.host = 'testhost'
        self.mock_connection.connect_kwargs = {'key_filename': 'testkey'}
        self.mock_logger = MagicMock()

    # @patch('hytools.connection.rsync.rsync_put')
    # def test_rsync_wrapper_put(self, mock_rsync_put):
    #     """Test the rsync function."""
    #     rsync(connection=self.mock_connection,
    #           source='source',
    #           target='target',
    #           download=False)
    #     mock_rsync_put.assert_called_once_with(connection=self.mock_connection,
    #                                            source='source',
    #                                            target='target')

    # @patch('hytools.connection.rsync_get')
    # def test_rsync_wrapper_get(self, mock_rsync_get):
    #     """Test the rsync function."""
    #     rsync(connection=self.mock_connection,
    #           source='source',
    #           target='target',
    #           download=True)
    #     mock_rsync_get.assert_called_once_with(connection=self.mock_connection,
    #                                            source='source',
    #                                            target='target')

    def test_rsync_get_wrapper(self):
        """Test the rsync_get function wrapper."""
        cmd = rsync(connection=self.mock_connection,
                    download=True,
                    dry_run=True,
                    source='source',
                    target='target')
        self.assertIn('rsync', cmd)
        self.assertIn('testuser@testhost:source', cmd)
        self.assertIn('target', cmd)

    def test_rsync_put_wrapper(self):
        """Test the rsync_put function wrapper."""
        cmd = rsync(connection=self.mock_connection,
                    source='source',
                    dry_run=True,
                    target='target',
                    execute=False)
        self.assertIn('rsync', cmd)
        self.assertIn('source', cmd)
        self.assertIn('testuser@testhost:target', cmd)

    def test_rsync_get(self):
        """Test the rsync_get function."""
        cmd = rsync_get(connection=self.mock_connection,
                        source='source',
                        target='target')
        self.assertIn('rsync', cmd)
        self.assertIn('testuser@testhost:source', cmd)
        self.assertIn('target', cmd)

    def test_rsync_put(self):
        """Test the rsync_put function."""
        cmd = rsync_put(connection=self.mock_connection,
                        source='source',
                        target='target')
        self.assertIn('rsync', cmd)
        self.assertIn('source', cmd)
        self.assertIn('testuser@testhost:target', cmd)


if __name__ == '__main__':
    unittest.main()
