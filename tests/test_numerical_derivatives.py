import numpy as np
import pytest

from hytools.numerical_derivatives import NumDiff

# central : note the order
# points = [0,1,-1,2,-2, 3,-3, 4, -4]
# # forward:
# #points = [0,1,2,3,4,5,6,7,8]
# # backwards:
# # take result from forward and revert all sings of coefficien
#  of odd orders of DERIVATIVES (m=1,3,5,...)
# test = NumDiff.gen_coeff(4, points)


def gen_coeff(numdiff):
    """Generate an array of coefficients for finite differences."""
    return np.array([numdiff[1][k] for k in sorted(numdiff[1].keys())])


# test2 = NumDiff.gen_coeff(4, points)
def test_numdiff():
    """Test generation of coefficients for finite differences."""
    assert np.allclose(gen_coeff(NumDiff.find_coeff(1, 2)),
                       np.array([-1 / 2, 1 / 2]))
    assert np.allclose(gen_coeff(NumDiff.find_coeff(1, 4, 'central')),
                       [1 / 12, -2 / 3, 2 / 3, -1 / 12])
    # assert np.allclose(gen_coeff(NumDiff.find_coeff(1, 4, 'forwards')),
    #                    [-11 / 6, 3, -3 / 2, 1 / 3])
    # assert np.allclose(gen_coeff(NumDiff.find_coeff(2, 4, 'forwards')),
    #                    [2, -5, 4, -1])
    assert np.allclose(gen_coeff(NumDiff.find_coeff(2, 5, 'central')),
                       [-1 / 12, 4 / 3, -2.5, 4 / 3, -1 / 12])
    assert np.allclose(gen_coeff(NumDiff.find_coeff(4, 9, 'central')),
                       [7 / 240, -2 / 5, 169 / 60, -122 / 15, 91 / 8,
                        -122 / 15, 169 / 60, -2 / 5, 7 / 240])
    with pytest.raises(Exception):
        NumDiff.find_coeff(2, 1)
