import shutil
import tempfile
import unittest
from hashlib import sha256
from pathlib import Path
from unittest.mock import MagicMock, patch

from hytools.file import File, FileManager, get_file


class TestFile(unittest.TestCase):
    """Test the File class."""

    def test_initialization(self):
        """Test the initialization of the File class."""
        file = File(name='test.txt', content='Hello, World!', folder='/tmp')
        self.assertEqual(file.name, 'test.txt')
        self.assertEqual(file.content, 'Hello, World!')
        self.assertEqual(file.folder, '/tmp')
        # self.assertEqual(file.host, 'localhost')
        self.assertEqual(file.path, '/tmp/test.txt')

    def test_initialization_defaults(self):
        """Test the initialization with default values."""
        file = File(name='test.txt')
        self.assertEqual(file.name, 'test.txt')
        self.assertEqual(file.content, None)
        self.assertEqual(file.folder, None)
        self.assertEqual(file.host, None)

    # def test_missing_name(self):
    #     """Test initialization with missing name."""
    #     with self.assertRaises(ValueError):
    #         File()

    def test_str_method(self):
        """Test the __str__ method."""
        file = File(name='test.txt', content='Hello, World!')
        expected_str = ("File(name='test.txt', content='Hello, World!', "
                        'path=None, '
                        'host=None, '
                        'folder=None)')
        self.assertEqual(str(file), expected_str)

    def test_hash_method(self):
        """Test the hash method."""
        file = File(name='test.txt', content='Hello, World!')
        expected_hash = sha256(str(file).encode()).hexdigest()
        self.assertEqual(file.hash(), expected_hash)


class TestFileManager(unittest.TestCase):
    """Test the FileManager class."""

    @patch('hytools.file.filemanager.get_file')
    @patch('pathlib.Path.read_text')
    @patch('pathlib.Path.exists')
    def test_read_file_local(self, mock_exists, mock_read_text, mock_get_file):
        """Test the read_file_local method."""
        mock_get_file.return_value = MagicMock(path='/tmp/test.txt')
        mock_exists.return_value = True
        mock_read_text.return_value = 'Hello, World!'

        result = FileManager.read_file_local('test.txt')
        mock_get_file.assert_called_once_with('test.txt')
        mock_exists.assert_called_once()
        mock_read_text.assert_called_once()
        self.assertEqual(result, 'Hello, World!')

    @patch('hytools.file.filemanager.get_file')
    @patch('pathlib.Path.write_text')
    @patch('pathlib.Path.exists')
    @patch('pathlib.Path.mkdir')
    def test_write_file_local(self, mock_mkdir, mock_exists, mock_write_text,
                              mock_get_file):
        """Test the write_file_local method."""
        mock_get_file.return_value = MagicMock(path='/tmp/test.txt',
                                               content='Hello, World!')
        mock_exists.return_value = False

        result = FileManager.write_file_local('test.txt',
                                              variables={'name': 'World'})
        mock_get_file.assert_called_once_with('test.txt')
        mock_exists.assert_called_once()
        mock_mkdir.assert_called_once_with(parents=True, exist_ok=True)
        mock_write_text.assert_called_once_with('Hello, World!')
        self.assertEqual(result, '/tmp/test.txt')

    @patch('hytools.file.filemanager.get_file')
    @patch('pathlib.Path.exists')
    def test_write_file_local_no_overwrite(self, mock_exists, mock_get_file):
        """Test the write_file_local method with no overwrite."""
        mock_get_file.return_value = MagicMock(path='/tmp/test.txt')
        mock_exists.return_value = True

        result = FileManager.write_file_local('test.txt', overwrite=False)
        mock_get_file.assert_called_once_with('test.txt')
        mock_exists.assert_called_once()
        self.assertEqual(result, '/tmp/test.txt')


class TestGetFile(unittest.TestCase):
    """Test the get_file function."""

    def setUp(self):
        """Set up a temporary directory for testing."""
        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        """Remove the temporary directory after testing."""
        shutil.rmtree(self.test_dir)

    def test_get_file_str(self):
        """Test get_file with a string argument."""
        test_file_path = Path(self.test_dir) / 'test.txt'
        test_file_path.write_text('Hello, World!')

        result = get_file(str(test_file_path), include_content=True)
        self.assertEqual(result.name, 'test.txt')
        self.assertEqual(result.content, 'Hello, World!')
        self.assertEqual(result.path, str(test_file_path))
        self.assertEqual(result.folder, str(test_file_path.parent))

    def test_get_file_path(self):
        """Test get_file with a Path argument."""
        test_file_path = Path(self.test_dir) / 'test.txt'
        test_file_path.write_text('Hello, World!')

        result = get_file(test_file_path, include_content=True)
        self.assertEqual(result.name, 'test.txt')
        self.assertEqual(result.content, 'Hello, World!')
        self.assertEqual(result.path, str(test_file_path))
        self.assertEqual(result.folder, str(test_file_path.parent))

    def test_get_file_dict(self):
        """Test get_file with a dictionary argument."""
        file_dict = {
            'name': 'test.txt',
            'content': 'Hello, World!',
            'path': '/tmp/test.txt',
            'folder': '/tmp'
        }
        result = get_file(file_dict)
        self.assertEqual(result.name, 'test.txt')
        self.assertEqual(result.content, 'Hello, World!')
        self.assertEqual(result.path, '/tmp/test.txt')
        self.assertEqual(result.folder, '/tmp')

    def test_get_file_invalid(self):
        """Test get_file with an invalid argument."""
        with self.assertRaises(TypeError):
            get_file(123)


if __name__ == '__main__':
    unittest.main()
